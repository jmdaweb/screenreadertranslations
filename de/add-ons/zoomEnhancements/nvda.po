# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the zoomEnhancements package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: zoomEnhancements 1.1.1\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-08-17 09:00+0000\n"
"PO-Revision-Date: 2021-12-12 08:39+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0.1\n"

#. Translators: Title for the settings dialog
msgid "Zoom Enhancements settings"
msgstr "Einstellungen für Zoom-Verbesserungen"

#. Translators: the lable of the combobox in the settings dialog
msgid "Alerts reporting mode"
msgstr "Benachrichtigungsmodus"

#. Translators: the text of the grouping in the settings dialog
msgid ""
"Choose which alerts should be reported (effective only when custom mode is "
"selected)"
msgstr ""
"Wählen Sie aus, welche Warnungen gemeldet werden sollen (nur wirksam, wenn "
"der benutzerdefinierte Modus ausgewählt ist)"

#. Translators: a label of a checkbox in the settings dialog
msgid "Participant Has Joined/Left Meeting"
msgstr "Teilnehmer ist dem Meeting beigetreten/hat das Meeting verlassen"

#. Translators: a label of a checkbox in the settings dialog
msgid "Participant Has Joined/Left Waiting Room"
msgstr "Teilnehmer hat den Wartebereich betreten/verlassen"

#. Translators: a label of a checkbox in the settings dialog
msgid "Audio Muted by Host"
msgstr "Audio vom Gastgeber stummgeschaltet"

#. Translators: a label of a checkbox in the settings dialog
msgid "Video Stopped by Host"
msgstr "Video vom Gastgeber gestoppt"

#. Translators: a label of a checkbox in the settings dialog
msgid "Screen Sharing Started/Stopped by a Participant"
msgstr "Bildschirmfreigabe wurde von einem Teilnehmer gestartet/gestoppt"

#. Translators: a label of a checkbox in the settings dialog
msgid "Recording Permission Granted/Revoked"
msgstr "Aufnahmeberechtigung erteilt/entzogen"

#. Translators: a label of a checkbox in the settings dialog
msgid "Public In-meeting Chat Received"
msgstr "Öffentlicher In-Meeting-Chat erhalten"

#. Translators: a label of a checkbox in the settings dialog
msgid "Private In-meeting Chat Received"
msgstr "Privater In-Meeting-Chat erhalten"

#. Translators: a label of a checkbox in the settings dialog
msgid "In-meeting File Upload Completed"
msgstr "Datei-Upload im Meeting abgeschlossen"

#. Translators: a label of a checkbox in the settings dialog
msgid "Host Privilege Granted/Revoked"
msgstr "Gastgeberberechtigung erteilt/entzogen"

#. Translators: a label of a checkbox in the settings dialog
msgid "Participant Has Raised/Lowered Hand (Host Only)"
msgstr "Teilnehmer hat die Hand gehoben/gesenkt (nur für Gastgeber)"

#. Translators: a label of a checkbox in the settings dialog
msgid "Remote Control Permission Granted/Revoked"
msgstr "Fernsteuerungsberechtigung erteilt/entzogen"

#. Translators: a label of a checkbox in the settings dialog
msgid "IM Chat Received"
msgstr "In-Meeting-Chat erhalten"

#. A script category for the add -on
#. Translators: the name of the add-on category in input gestures
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on to be shown on installation and add-on information.
msgid "Zoom Enhancements"
msgstr "Zoom-Verbesserungen"

#. Translators: a lable for alerts reporting mode
msgid "Report all alerts"
msgstr "Alle Benachrichtigungen"

#. Translators: a lable for alerts reporting mode
msgid "Beep for alerts"
msgstr "Signalton bei Warnungen"

#. Translators: a lable for alerts reporting mode
msgid "Silence all alerts"
msgstr "Alle Benachrichtigungen stummschalten"

#. Translators: a lable for alerts reporting mode
msgid "Custom"
msgstr "Benutzerdefiniert"

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting joined / left meeting alerts on / off"
msgstr ""
"Schaltet das Melden von Benachrichtigungen zu beigetretenen / verlassenen "
"Besprechungen ein / aus"

#. Translators: a label for on / off states for reporting a spicific alert
msgid "on"
msgstr "ein"

msgid "off"
msgstr "aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Participant Has Joined/Left Meeting is %s"
msgstr ""
"Benachrichtigung für Teilnehmer ist dem Meeting beigetreten/hat as Meeting "
"verlassen ist %s"

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting joined / left Waiting Room alerts on / off"
msgstr ""
"Schaltet die Benachrichtigungen für Betreten/Verlassen im Wartebereich ein/"
"aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Participant Has Joined/Left Waiting Room is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting audio muted by host alerts on / off"
msgstr ""
"Schaltet die Benachrichtigungen für Audio, das durch Gastgeber-Warnungen "
"stummgeschaltet wurde, ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Audio Muted By Host is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting video stopped by host alerts on / off"
msgstr ""
"Schaltet die Benachrichtigungen für Video, das durch Gastgeber-Warnungen "
"gestoppt wurde, ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Video Stopped By Host is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid ""
"Toggles reporting screen sharing started / stopped by participant alerts "
"on / off"
msgstr ""
"Schaltet die Benachrichtigung der Bildschirmfreigabe ein/aus, die von "
"Teilnehmerbenachrichtigungen gestartet/gestoppt wurde"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Screen Sharing Started/Stopped By Participant is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid ""
"Toggles reporting recording permission granted / revoked alerts on / off"
msgstr ""
"Aktiviert/deaktiviert die Benachrichtigungen für die Berechtigung zum "
"Aufzeichnen, die erteilt/entzogen wurde"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Recording Permission Granted/Revoked is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting public in-meeting chat received alerts on / off"
msgstr ""
"Schaltet die Benachrichtigungen im öffentlichen In-Meeting-Chat ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Public In-meeting Chat Received is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting private in-meeting chat received alerts on / off"
msgstr "Schaltet die Benachrichtigungen im privaten In-Meeting-Chat ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Private In-meeting Chat Received is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting in-meeting file upload completed alerts on / off"
msgstr ""
"Schaltet die Benachrichtigungen über abgeschlossene Datei-Uploads im "
"Meeting ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting In-meeting File Upload Completed is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting host privilege granted / revoked alerts on / off"
msgstr ""
"Schaltet die Berechtigungen der Gastgeber für erteilte/entzogene Warnungen "
"ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Host Privilege Granted/Revoked is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting participant raised / lowered hand alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Participant Has Raised/Lowered Hand is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid ""
"Toggles reporting remote control permission granted /revoked alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Remote Control Permission Granted/Revoked is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting IM chat received alerts on / off"
msgstr "Schaltet die Benachrichtigungen für In-Meeting-Chats ein/aus"

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting IM Chat Received is %s"
msgstr ""

#. Translators: a description for a command to cycle between alerts reporting modes
msgid ""
"Toggles between reporting alerts as usual, beeping for the alert, silencing "
"alerts completely, or custom mode, where you can choose which alerts are "
"reported and which aren't."
msgstr ""

#. Translators: a description for a command to move the focus in / out of the remote controlled screen
msgid "Moves focus in / out of the remote controlled screen"
msgstr ""
"Verschiebt den Fokus in bzw. aus dem Bereich für den ferngesteuerten "
"Bildschirm"

#. Translators: a description for a command to show the add-on settings dialog
msgid "Shows Zoom enhancements settings dialog"
msgstr "Zeigt die Einstellungen für Zoom-Verbesserungen an"

#. Translators: a description for a command to show chat history dialog
msgid "Shows chat history dialog"
msgstr "Zeigt den Chat-Verlauf an"

#. Translaters: the title of the chat history dialog
msgid "Chat history"
msgstr "Chat-Verlauf"

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid ""
"An add-on which aims to enhance the experience while using Zoom and NVDA"
msgstr "Mit dieser Erweiterung wird das Erlebnis mit Zoom und NVDA verbessert"
