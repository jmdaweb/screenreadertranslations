# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: resourceMonitor 2.1\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-07-09 12:42-0700\n"
"PO-Revision-Date: 2021-07-09 12:42-0700\n"
"Last-Translator: Çağrı Doğan <cagrid@hotmail.com>\n"
"Language-Team: \n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.9\n"

#. Translators: Message reported when there is no battery on the system,
#. mostly laptops with battery pack removed and running on AC power.
#: addon/globalPlugins/resourceMonitor/__init__.py:121
msgid "This computer does not have a battery connected."
msgstr "Bu bilgisayara pil takılı değil."

#. Translators: message presented when AC is connected and battery is charging,
#. also show current battery percentage.
#: addon/globalPlugins/resourceMonitor/__init__.py:127
#, python-brace-format
msgid "{percent}%, battery charging."
msgstr "%{percent}, pil şarj oluyor."

#. Translators: message presented when computer is running on battery power,
#. showing percentage remaining yet battery time is unknown.
#: addon/globalPlugins/resourceMonitor/__init__.py:133
#, python-brace-format
msgid "{percent}% battery remaining, battery time unknown."
msgstr "%{percent} kalan pil, pil süresi bilinmiyor."

#. Translators: battery and system uptime in hours.
#. Translators: system uptime in hours.
#: addon/globalPlugins/resourceMonitor/__init__.py:145
#: addon/globalPlugins/resourceMonitor/__init__.py:486
msgid "1 hour"
msgstr "1 saat"

#. Translators: battery and system uptime in hours.
#. Translators: system uptime in hours.
#: addon/globalPlugins/resourceMonitor/__init__.py:147
#: addon/globalPlugins/resourceMonitor/__init__.py:488
#, python-brace-format
msgid "{0} hours"
msgstr "{0} saat"

#. Translators: battery and system uptime in minutes.
#. Translators: system uptime in minutes.
#: addon/globalPlugins/resourceMonitor/__init__.py:152
#: addon/globalPlugins/resourceMonitor/__init__.py:492
msgid "1 minute"
msgstr "1 dakika"

#. Translators: battery and system uptime in minutes.
#. Translators: system uptime in minutes.
#: addon/globalPlugins/resourceMonitor/__init__.py:154
#: addon/globalPlugins/resourceMonitor/__init__.py:494
#, python-brace-format
msgid "{0} minutes"
msgstr "{0} dakika"

#. Translators: message presented when computer is running on battery power,
#. showing percentage remaining and estimated remaining time.
#: addon/globalPlugins/resourceMonitor/__init__.py:162
#, python-brace-format
msgid "{percent}% battery remaining, about {time}."
msgstr "%{percent} kalan pil, yaklaşık {time}."

#. Translators: Message reported when battery level is low.
#: addon/globalPlugins/resourceMonitor/__init__.py:166
msgid " Warning: low battery."
msgstr " Uyarı: düşük pil."

#. Translators: Message reported when battery level is critical.
#: addon/globalPlugins/resourceMonitor/__init__.py:169
msgid " Warning: critically low battery."
msgstr " Uyarı: pil kritik seviyede."

#. Translators: Presents Windows version
#. (example output: "Windows 8.1 (32-bit)").
#: addon/globalPlugins/resourceMonitor/__init__.py:285
#: addon/globalPlugins/resourceMonitor/__init__.py:341
#, python-brace-format
msgid "{winVersion} ({cpuBit})"
msgstr "{winVersion} ({cpuBit})"

#. Translators: Presents Windows version and service pack level
#. (example output: "Windows 7 service pack 1 (64-bit)").
#: addon/globalPlugins/resourceMonitor/__init__.py:291
#: addon/globalPlugins/resourceMonitor/__init__.py:347
#, python-brace-format
msgid "{winVersion} {servicePackLevel} ({cpuBit})"
msgstr "{winVersion} {servicePackLevel} ({cpuBit})"

#. Translators: The gestures category for this add-on in input gestures dialog (2013.3 or later).
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on
#. to be shown on installation and add-on information.
#: addon/globalPlugins/resourceMonitor/__init__.py:358 buildVars.py:23
msgid "Resource Monitor"
msgstr "Kaynak İzleme"

#. Translators: Input help message about battery info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:363
msgid ""
"Presents battery percentage, charging status, remaining time (if not "
"charging), and a warning if the battery is low or critical."
msgstr ""
"pil yüzdesini, şarj durumunu, kalan zamanı  (şarja takılı değilse ), ve "
"düşük ya da kritik pil seviyesini söyler."

#. Translators: Input help message about drive info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:377
msgid ""
"Presents the used and total space of the static and removable drives on this "
"computer."
msgstr ""
"Bilgisayara takılı sabit ve çıkarılabilir disklerle ilgili kullanım "
"bilgisini verir."

#. Translators: Shows drive letter, type of drive (fixed or removable),
#. used capacity and total capacity of a drive
#. (example: C drive, ntfs; 40 GB of 100 GB used (40%).
#: addon/globalPlugins/resourceMonitor/__init__.py:394
#, python-brace-format
msgid ""
"{driveName} ({driveType} drive): {usedSpace} of {totalSpace} used {percent}%."
msgstr ""
"{driveName} ({driveType} sürücü): Kullanılan, {usedSpace}, Toplam, "
"{totalSpace}, %{percent}."

#. Translators: Input help mode message about processor info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:409
msgid "Presents the average processor load and the load of each core."
msgstr "Ortalama işlemci yükü ve çekirdek kullanımı bilgisini verir."

#. Translators: Shows average load of CPU cores (example: core 1, 50%).
#: addon/globalPlugins/resourceMonitor/__init__.py:418
#, python-brace-format
msgid "Core {coreNumber}: {corePercent}%"
msgstr "Çekirdek {coreNumber}: yüzde {corePercent}"

#. Translators: Shows average load of the processor and the load for each core.
#: addon/globalPlugins/resourceMonitor/__init__.py:423
#, python-brace-format
msgid "Average CPU load {avgLoad}%, {cores}."
msgstr "Ortalama CPU yükü %{avgLoad}, {cores}."

#. Translators: Input help mode message about memory info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:433
msgid "Presents the used and total space for both physical and virtual ram."
msgstr "Kullanılan fiziksel ve sanal ram bilgisini verir."

#. Translators: Shows RAM (physical memory) usage.
#: addon/globalPlugins/resourceMonitor/__init__.py:439
#, python-brace-format
msgid "Physical: {physicalUsed} of {physicalTotal} used ({physicalPercent}%). "
msgstr ""
"Fiziksel: {physicalUsed} bölü {physicalTotal} kullanılıyor "
"(%{physicalPercent})."

#. Translators: Shows virtual memory usage.
#: addon/globalPlugins/resourceMonitor/__init__.py:446
#, python-brace-format
msgid "Virtual: {virtualUsed} of {virtualTotal} used ({virtualPercent}%)."
msgstr ""
"Sanal: {virtualUsed} bölü {virtualTotal} kullanılıyor (%{virtualPercent})."

#. Translators: Input help mode message about Windows version command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:458
msgid "Announces the version of Windows you are using."
msgstr "Kullandığınız Windows sürümünü söyler."

#. Translators: system uptime in days.
#: addon/globalPlugins/resourceMonitor/__init__.py:479
msgid "1 day"
msgstr "1 gün"

#. Translators: system uptime in days.
#: addon/globalPlugins/resourceMonitor/__init__.py:481
#, python-brace-format
msgid "{0} days"
msgstr "{0} gün"

#. Translators: system uptime in seconds.
#: addon/globalPlugins/resourceMonitor/__init__.py:499
msgid "1 second"
msgstr "1 saniye"

#. Translators: system uptime in seconds.
#: addon/globalPlugins/resourceMonitor/__init__.py:501
#, python-brace-format
msgid "{0} seconds"
msgstr "{0} saniye"

#. Translators: Input help mode message about obtaining the system's uptime
#: addon/globalPlugins/resourceMonitor/__init__.py:507
msgid "Announces the system's uptime."
msgstr "Sistemin çalışma süresini seslendirir."

#. Translators: Obtaining uptime failed
#: addon/globalPlugins/resourceMonitor/__init__.py:519
msgid "Failed to get the system's uptime."
msgstr "Sistemin açık olduğu zaman bilgisi alınamadı."

#. Translators: Input help mode message about overall system resource info command in Resource Monitor
#: addon/globalPlugins/resourceMonitor/__init__.py:523
msgid ""
"Presents used ram, average processor load, and battery info if available."
msgstr "Kullanılan ram, CPU yükü, ve mevcutsa pil bilgisini verir ."

#: addon/globalPlugins/resourceMonitor/__init__.py:530
#, python-brace-format
msgid "{ramPercent}% RAM used, CPU at {cpuPercent}%."
msgstr "yüzde {ramPercent} RAM kullanılan, CPU:  %{cpuPercent}."

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
#: buildVars.py:26
msgid ""
"A handy resource monitor to report CPU load, memory usage, battery, disk "
"usage status and more."
msgstr ""
"CPU yükü, hafıza kullanımı, pil ve disk kullanım durumunu takip etmek ve "
"daha fazlası için pratik bir araç."
