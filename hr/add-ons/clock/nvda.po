# Hrvatska lokalizacija za NVDA.
# Copyright (C) 2006-2019 NVDA contributors <https://www.nvda-project.org>
# This file is distributed under the same license as the virtualRevision package.
# Zvonimir Stanečić <zvonimirek222@yandex.com>, 2019.
# Milo Ivir <mail@milotype.de>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Clock 18.12\n"
"Report-Msgid-Bugs-To: 'nvda-translations@groups.io'\n"
"POT-Creation-Date: 2018-12-12 07:57+0100\n"
"PO-Revision-Date: 2021-10-17 20:03-0700\n"
"Last-Translator: Milo Ivir <mail@milotype.de>\n"
"Language-Team: HR <hrvojekatic@gmail.com>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.9\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"

#. Translators: the label of a message box dialog.
msgid ""
"The date and time format you were using are not compatible with this version "
"of the Clock add-on, this will be fixed during installation. Click OK to "
"confirm these corrections"
msgstr ""
"Oblik datuma i vremena koji ste koristili nisu kompatibilni s ovom verzijom "
"dodatka Sat, a to će se popraviti tijekom instalacije. Kliknite U redu da "
"biste potvrdili ove ispravke"

#. Translators: the title of a message box dialog.
msgid "Time and date format corrections"
msgstr "Ispravci oblika vremena i datuma"

#, python-brace-format
msgid "{hours} hours, "
msgstr "{hours} sati, "

#, python-brace-format
msgid "{minutes} minutes, "
msgstr "{minutes} minuta, "

#, python-brace-format
msgid "{seconds} seconds"
msgstr "{seconds} sekundi"

msgid "0 seconds"
msgstr "0 sekundi"

#. Translators: Script category for Clock addon commands in input gestures dialog.
#. Translators: This is the label for the clock settings panel.
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on
#. to be shown on installation and add-on information found in Add-ons Manager.
msgid "Clock"
msgstr "Sat"

#. Translators: The name of the alarm item in NVDA Tools menu.
msgid "Schedule a&larms..."
msgstr "Zakaži a&larme..."

#. Translators: The tooltyp text for the alarm item in NVDA Tools menu.
msgid "Allows you to schedule an alarm"
msgstr "Omogućuje postavljanje alarma"

#. Translators: Message presented in input help mode.
msgid ""
"Speaks current time. If pressed twice quickly, speaks current date. If "
"pressed three times quickly, reports the current day, the week number, the "
"current year and the days remaining before the end of the year."
msgstr ""
"Izgovara trenutno vrijeme. Ako se pritisne dva puta brzo, izgovara trenutni "
"datum. Ako se pritisne tri puta brzo, biti 'e iygovoren broj dana koji "
"traje, broju tjedna, tekućoj godini i danima koji su preostali prije kraja "
"godine."

#, python-brace-format
msgid "Day {day}, week {week} of {year}, remaining days {remain}."
msgstr "{day}. dan, {week}. tjedan {year}., preostalih dana: {remain}."

#. Translators: Message presented in input help mode.
msgid ""
"Clock and calendar layer commands. After pressing this keystroke, press H "
"for additional help."
msgstr ""
"Višeslojne naredbe za sat i kalendar. Nakon pritiskanja ove tipkovničke "
"kombinacije, pritisni „H” za dodatnu pomoć."

#. Translators: Message presented in input help mode.
msgid "Starts, resets or stops the stopwatch."
msgstr "Pokreće, zaustavlja ili resetira štopericu."

msgid "Reset. Running."
msgstr "Resetiraj. Pokretanje."

msgid "Running."
msgstr "Pokretanje."

#, python-brace-format
msgid "{0} stopped."
msgstr "{0} zaustavljen."

#. Translators: Message presented in input help mode.
msgid "Speaks current stopwatch or count-down timer."
msgstr "Izgovara trenutačno stanje štoperice ili odbrojavanja vremena."

#. Translators: Message presented in input help mode.
msgid "Gives the remaining and elapsed time before the next alarm."
msgstr "Izdaje proteklo vrijeme i preostalo vrijeme prije sljedećeg alarma."

#, python-brace-format
msgid "Elapsed time {elapsed}, remaining time {remaining}."
msgstr "Proteklo vrijeme {elapsed}, preostalo vrijeme {remaining}."

msgid "No alarm"
msgstr "Nema alarma"

#. Translators: Message presented in input help mode.
msgid "Cancel the next alarm."
msgstr "Otkaži sljedeći alarm."

msgid "Alarm cancelled"
msgstr "Alarm je otkazan"

#. Translators: Message presented in input help mode.
msgid "Resets stopwatch to 0 without restarting it."
msgstr "Resetira štopericu na nulu bez ponovnog pokretanja."

msgid ""
"The stopwatch is already reset to 0. Use the clock layer command followed by "
"s to start it."
msgstr ""
"Štoperica je već resetirana na nulu. Za pokretanje štoperice koristi "
"povezane naredbe sata, a zatim tipku „s”."

msgid "Stopwatch reset."
msgstr "Resetiranje štoperice."

#. Translators: Message presented in input help mode.
msgid "Lists available commands in clock command layer."
msgstr "Ispisuje dostupne naredbe za sat."

#. Translators: Message presented in input help mode.
msgid "Allows to check the next alarm. If pressed twice, cancels it."
msgstr ""
"Omogućuje provjeravanje sljedećeg alarm. Ako se pritisne dvaput, otkazuje "
"alarm."

#. Translators: Message presented in input help mode.
msgid "If an alarm is too long, allows to stop it."
msgstr "Omogućuje zaustavljanje alarma, ako je predug."

msgid "No sound is launched."
msgstr "Nijedan zvuk nije pokrenut."

msgid "Sound stopped"
msgstr "Zvuk zaustavljen"

#. Translators: error message when attempting to open more than one alarm settings dialogs.
msgid "Schedule alarms dialog is already open."
msgstr "Dijaloški okvir Zakazivanje alarma već je otvoren."

#. Translators: Message presented in input help mode.
msgid "Display the clock settings dialog box."
msgstr "Prikazuje dijaloški okvir za postavke sata."

#. Translators: Message presented in input help mode.
msgid "Display schedule alarms dialog box."
msgstr "Prikazuje dijaloški okvir zakazivanje alarma."

#. Translators: This is the label for a combo box in the Clock settings dialog.
msgid "&Time display format:"
msgstr "Format za prikaz &vremena:"

#. Translators: This is the label for a combo box in the Clock settings dialog.
msgid "&Date display format:"
msgstr "Format za prikaz &datuma:"

#. Translators: This is a choice of the auto announce choices combo box.
msgid "off"
msgstr "isključeno"

#. Translators: This is a choice of the auto announce choices combo box.
msgid "every 10 minutes"
msgstr "svakih 10 minuta"

#. Translators: This is a choice of the auto announce choices combo box.
msgid "every 15 minutes"
msgstr "svakih 15 minuta"

#. Translators: This is a choice of the auto announce choices combo box.
msgid "every 30 minutes"
msgstr "svakih pola sata"

#. Translators: This is a choice of the auto announce choices combo box.
msgid "every hour"
msgstr "svakih sat vremena"

#. Translators: This is the label for a combo box in the Clock settings dialog.
msgid "&Interval:"
msgstr "&interval:"

#. Translators: This is a choice of the time report choices combo box.
msgid "message and sound"
msgstr "poruka i zvuk"

#. Translators: This is a choice of the time report choices combo box.
msgid "message only"
msgstr "samo poruka"

#. Translators: This is a choice of the time report choices combo box.
msgid "sound only"
msgstr "samo zvuk"

#. Translators: This is the label for a combo box in the Clock settings dialog.
msgid "Time &announcement:"
msgstr "N&ajava vremena:"

#. Translators: This is the label for a combo box in the Clock settings dialog.
msgid "Clock chime &sound:"
msgstr "Zvuk zvona &sata:"

#. Translators: This is the label for a checkbox in the Clock settings dialog.
msgid "&Quiet hours"
msgstr "Sati &mirovanja"

#. Translators: This is a choice of the quiet hours time format choices.
msgid "12-hour format"
msgstr "12-satni format"

#. Translators: This is a choice of the quiet hours time format choices.
msgid "24-hour format"
msgstr "24-satni format"

#. Translators: This is the label for a combo box in the Clock settings dialog.
msgid "Quiet hours time &format:"
msgstr "Tihi sati i oblikovanje &vremena:"

#. Translators: This is the label for an group in the Clock settings dialog.
msgid "Quiet hours start time"
msgstr "Početak tihog sata"

#. Translators: This is the label for an group in the Clock settings dialog.
msgid "Quiet hours end time"
msgstr "Kraj tihog sata"

#. Translators: the hour label in quiet hours group.
msgid "Hour:"
msgstr "Sat:"

#. Translators: the minute label in quiet hours group.
msgid "Minute:"
msgstr "Minuta:"

#. Translators: This is the label for the alarm settings panel.
msgid "Schedule alarms"
msgstr "Zakazivanje alarma"

#. Translators: This is an item of the alarm duration choices.
msgid "hours"
msgstr "sati"

#. Translators: This is an item of the alarm duration choices.
msgid "minutes"
msgstr "minuta"

#. Translators: This is an item of the alarm duration choices.
msgid "seconds"
msgstr "sekundi"

#. Translators: This is the label for a combo box in the Alarm settings dialog.
msgid "&Alarm duration in:"
msgstr "&Trajanje alarma u:"

#. Translators: This is the label for an edit field in the Alarm settings dialog.
msgid "&Duration:"
msgstr "&Trajanje:"

#. Translators: This is the label for a combo box in the Alarm settings dialog.
msgid "A&larm sound:"
msgstr "Zvuk a&larma:"

#. Translators: This is the label for a button in the Alarm settings dialog.
msgid "&Stop"
msgstr "&Zaustavi"

#. Translators: This is the label for a button in the Alarm settings dialog.
msgid "&Pause"
msgstr "&Pauza"

#. Translators: The message displayed after a countdown for an alarm has been chosen.
#, python-brace-format
msgid "You've chosen an alarm to be triggered in {tm} {unit}"
msgstr "Izabran je alarm koji će se aktivirati za {tm} {unit}"

#. Translators: The title of the dialog which appears when the user has chosen to trigger an alarm.
msgid "Confirmation"
msgstr "Potvrda"

#. Translators: A time formating.
#, python-brace-format
msgid "It's {hours} o'clock and {minutes} minutes"
msgstr "Sada je {hours} i {minutes}"

#. Translators: A time formating.
#, python-brace-format
msgid "It's {hours} o'clock, {minutes} minutes and {seconds} seconds"
msgstr "Sada je {hours} i {minutes} i {seconds} s"

#. Translators: A time formating.
#, python-brace-format
msgid "{hours} o'clock, {minutes} minutes"
msgstr "{hours} i {minutes}"

#. Translators: A time formating.
#, python-brace-format
msgid "{hours} o'clock, {minutes} minutes, {seconds} seconds"
msgstr "{hours} i {minutes} i {seconds} s"

#. Translators: A time formating.
#, python-brace-format
msgid "It's {minutes} past {hours}"
msgstr "Sada je {hours} i {minutes}"

#. Translators: A time formating.
#, python-brace-format
msgid "{hours} h {minutes} min"
msgstr "{hours} h {minutes} min"

#. Translators: A time formating.
#, python-brace-format
msgid "{hours} h, {minutes} min, {seconds} sec"
msgstr "{hours} h, {minutes} min, {seconds} s"

#. Translators: A time formating.
#, python-brace-format
msgid "It's {hours}:{minutes}"
msgstr "Sada je {hours}:{minutes}"

#. Translators: A time formating.
#, python-brace-format
msgid "It's {hours}:{minutes}:{seconds}"
msgstr "Sada je {hours}:{minutes}:{seconds}"

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid ""
"An advanced clock and calendar for NVDA.\n"
"NVDA+F12, get current time.\n"
"NVDA+F12 pressed twice quickly, get current date.\n"
"NVDA+F12 pressed three times quickly, reports the current day, the week "
"number, the current year and the remaining days before the end of the year.\n"
"For other instructions, press Add-on help button in add-ons manager."
msgstr ""
"Napredni sat i kalendar za NVDA.\n"
"NVDA+F12, izgovara trenutno vrijeme.\n"
"NVDA+F12 pritisnut dva puta brzo, izgovara trenutni datum.\n"
"NVDA+F12 pritisnut  tri puta brzo, izgovara trenutni dan, broj tjedna, "
"trenutnu godinu i preostale dane do kraja godine.\n"
"Za ostale upute pritisnite gumb pomoć dodatka u upravitelju dodataka."
