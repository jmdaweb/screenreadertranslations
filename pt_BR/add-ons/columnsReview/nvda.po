# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the columnsReview package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: columnsReview 3.0\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-09-16 00:36+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: menu item in preferences
msgid "Columns Review Settings..."
msgstr ""

msgid "(DO NOT EDIT!)"
msgstr ""

#. Translators: message when digit pressed exceed the columns number
msgid "No more columns available"
msgstr ""

#. Translators: Announced when the column at the requested index is not visible.
msgid "No more visible columns available"
msgstr ""

#. Translators: Presented for an empty column.
msgid "Empty column"
msgstr ""

#. Translators: documentation of script to read columns
msgid ""
"Returns the header and the content of the list column at the index "
"corresponding to the number pressed"
msgstr ""

#. Translators: message when you change interval in a list with more ten columns
#, python-brace-format
msgid "From {start} to {end}"
msgstr ""

#. Translators: documentation for script to change interval
msgid "Cycles between a variable number of intervals of ten columns"
msgstr ""

msgid "No information available"
msgstr ""

#. Translators: documentation for script to announce list item info
msgid "Announces list item position information"
msgstr ""

#. Translators: documentation for script to manage headers
msgid "Provides a dialog for interactions with list column headers"
msgstr ""

#. translators: message presented when get selected item count and names
msgid "selected items"
msgstr ""

#. Translators: documentation for script to know current selected items
msgid "Reports current selected list items"
msgstr ""

#. Translators: documentation for script to find in list
msgid "Provides a dialog for searching in item list"
msgstr ""

#. Translators: Message presented when search is in progress.
msgid "Searching..."
msgstr ""

#. Translators: documentation for script to manage headers
msgid "Goes to next result of current search"
msgstr ""

#. Translators: documentation for script to manage headers
msgid "Goes to previous result of current search"
msgstr ""

#. Translators: documentation for script to read all list items starting from the focused one.
msgid "Starts reading all list items beginning at the item with focus"
msgstr ""

msgid "Current selection info not available"
msgstr ""

msgid "Use multiple selection"
msgstr ""

#. Translators: title of settings dialog
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on
#. to be shown on installation and add-on information found in Add-ons Manager.
msgid "Columns Review"
msgstr ""

#. Translators: title of settings dialog
msgid "Columns Review Settings"
msgstr ""

#. Translators: Help message for group of comboboxes allowing to assign action to a keypress.
msgid "When pressing combination to read column:"
msgstr ""

#. Translators: Help message for sub-sizer of keys choices
msgid "Choose the keys you want to use with numbers:"
msgstr ""

#. Translators: label for numpad keys checkbox in settings
msgid "Use numpad keys to navigate through the columns"
msgstr ""

#. Translators: label for edit field in settings, visible if previous checkbox is disabled
msgid ""
"Insert the char after \"0\" in your keyboard layout, or another char as you "
"like:"
msgstr ""

#. Translators: label for announce-empty-list checkbox in settings
msgid "Announce empty list (not working in Win8/10 folders)"
msgstr ""

#. Translators: Description of the action which simply ignores the pressed key
msgid "Do nothing"
msgstr ""

#. Translators: Name of the action which reads given column.
msgid "Read column"
msgstr ""

#. Translators: Name of the action which copies given column.
msgid "Copy column"
msgstr ""

#. Translators: Name of the action which spells given column.
msgid "Spell column"
msgstr ""

#. Translators: Name of the action which displays content of the given column in the browseable message.
msgid "Show column content in browse mode"
msgstr ""

#. Translators: Label of a combobox in which action can be assigned to a first press of the shortcut.
msgid "On first press:"
msgstr ""

#. Translators: Label of a combobox in which action can be assigned to a second press of the shortcut.
msgid "On second press:"
msgstr ""

#. Translators: Label of a combobox in which action can be assigned to a third press of the shortcut.
msgid "On third press:"
msgstr ""

#. Translators: Label of a combobox in which action can be assigned to a fourth press of the shortcut.
msgid "On fourth press:"
msgstr ""

#. Translators: label for read-header checkbox in settings
msgid "Read the column header"
msgstr ""

#. Translators: label for copy-header checkbox in settings
msgid "Copy the column header"
msgstr ""

msgid "Headers manager"
msgstr ""

msgid "Unnamed header"
msgstr ""

msgid "Headers:"
msgstr ""

msgid "Left click"
msgstr ""

msgid "Right click"
msgstr ""

#, python-format
msgid "%s header clicked"
msgstr ""

#. Translators: the label of a message box dialog.
msgid ""
"Previous add-on settings will be lost. Please configure it again, from NVDA "
"preferences."
msgstr ""

#. Translators: the title of a message box dialog.
msgid "Add-on settings reset"
msgstr ""

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid "A better experience with lists"
msgstr ""
