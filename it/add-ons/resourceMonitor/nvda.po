# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: resourceMonitor 2.1\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-07-09 12:48-0700\n"
"PO-Revision-Date: 2021-07-09 12:49-0700\n"
"Last-Translator: Fabrizio Marini <marini.carlo@fastwebnet.it>\n"
"Language-Team: Simone Dal Maso <simone.dalmaso@juvox.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.9\n"

#. Translators: Message reported when there is no battery on the system,
#. mostly laptops with battery pack removed and running on AC power.
#: addon/globalPlugins/resourceMonitor/__init__.py:121
msgid "This computer does not have a battery connected."
msgstr "Questo computer non ha una batteria collegata."

#. Translators: message presented when AC is connected and battery is charging,
#. also show current battery percentage.
#: addon/globalPlugins/resourceMonitor/__init__.py:127
#, python-brace-format
msgid "{percent}%, battery charging."
msgstr "{percent}%, batteria in carica."

#. Translators: message presented when computer is running on battery power,
#. showing percentage remaining yet battery time is unknown.
#: addon/globalPlugins/resourceMonitor/__init__.py:133
#, python-brace-format
msgid "{percent}% battery remaining, battery time unknown."
msgstr "{percent}% di batteria rimanente; tempo rimanente sconosciuto."

#. Translators: battery and system uptime in hours.
#. Translators: system uptime in hours.
#: addon/globalPlugins/resourceMonitor/__init__.py:145
#: addon/globalPlugins/resourceMonitor/__init__.py:486
msgid "1 hour"
msgstr "1 ora,"

#. Translators: battery and system uptime in hours.
#. Translators: system uptime in hours.
#: addon/globalPlugins/resourceMonitor/__init__.py:147
#: addon/globalPlugins/resourceMonitor/__init__.py:488
#, python-brace-format
msgid "{0} hours"
msgstr "{0} ore,"

#. Translators: battery and system uptime in minutes.
#. Translators: system uptime in minutes.
#: addon/globalPlugins/resourceMonitor/__init__.py:152
#: addon/globalPlugins/resourceMonitor/__init__.py:492
msgid "1 minute"
msgstr "1 minuto"

#. Translators: battery and system uptime in minutes.
#. Translators: system uptime in minutes.
#: addon/globalPlugins/resourceMonitor/__init__.py:154
#: addon/globalPlugins/resourceMonitor/__init__.py:494
#, python-brace-format
msgid "{0} minutes"
msgstr "{0} minuti"

#. Translators: message presented when computer is running on battery power,
#. showing percentage remaining and estimated remaining time.
#: addon/globalPlugins/resourceMonitor/__init__.py:162
#, python-brace-format
msgid "{percent}% battery remaining, about {time}."
msgstr "{percent}% di batteria rimanente, circa {time}."

#. Translators: Message reported when battery level is low.
#: addon/globalPlugins/resourceMonitor/__init__.py:166
msgid " Warning: low battery."
msgstr " Attenzione: livello batteria basso."

#. Translators: Message reported when battery level is critical.
#: addon/globalPlugins/resourceMonitor/__init__.py:169
msgid " Warning: critically low battery."
msgstr " Attenzione: batteria quasi scarica."

#. Translators: Presents Windows version
#. (example output: "Windows 8.1 (32-bit)").
#: addon/globalPlugins/resourceMonitor/__init__.py:285
#: addon/globalPlugins/resourceMonitor/__init__.py:341
#, python-brace-format
msgid "{winVersion} ({cpuBit})"
msgstr "{winVersion} ({cpuBit})"

#. Translators: Presents Windows version and service pack level
#. (example output: "Windows 7 service pack 1 (64-bit)").
#: addon/globalPlugins/resourceMonitor/__init__.py:291
#: addon/globalPlugins/resourceMonitor/__init__.py:347
#, python-brace-format
msgid "{winVersion} {servicePackLevel} ({cpuBit})"
msgstr "{winVersion} {servicePackLevel} ({cpuBit})"

#. Translators: The gestures category for this add-on in input gestures dialog (2013.3 or later).
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on
#. to be shown on installation and add-on information.
#: addon/globalPlugins/resourceMonitor/__init__.py:358 buildVars.py:23
msgid "Resource Monitor"
msgstr "Resource Monitor"

#. Translators: Input help message about battery info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:363
msgid ""
"Presents battery percentage, charging status, remaining time (if not "
"charging), and a warning if the battery is low or critical."
msgstr ""
"Annuncia la percentuale della batteria, lo stato di carica, il tempo "
"rimanente (se non in carica), e un avviso se la batteria è scarica."

#. Translators: Input help message about drive info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:377
msgid ""
"Presents the used and total space of the static and removable drives on this "
"computer."
msgstr ""
"Annuncia lo spazio utilizzato e totale dei drive fissi e rimovibili presenti "
"su questo computer."

#. Translators: Shows drive letter, type of drive (fixed or removable),
#. used capacity and total capacity of a drive
#. (example: C drive, ntfs; 40 GB of 100 GB used (40%).
#: addon/globalPlugins/resourceMonitor/__init__.py:394
#, python-brace-format
msgid ""
"{driveName} ({driveType} drive): {usedSpace} of {totalSpace} used {percent}%."
msgstr ""
"drive {driveName} ({driveType}): {usedSpace} su {totalSpace} utilizzati "
"{percent}%."

#. Translators: Input help mode message about processor info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:409
msgid "Presents the average processor load and the load of each core."
msgstr "Annuncia il carico medio del processore e il carico di ciascun core."

#. Translators: Shows average load of CPU cores (example: core 1, 50%).
#: addon/globalPlugins/resourceMonitor/__init__.py:418
#, python-brace-format
msgid "Core {coreNumber}: {corePercent}%"
msgstr "Core {coreNumber}: {corePercent}%"

#. Translators: Shows average load of the processor and the load for each core.
#: addon/globalPlugins/resourceMonitor/__init__.py:423
#, python-brace-format
msgid "Average CPU load {avgLoad}%, {cores}."
msgstr "Carico medio della CPU  {avgLoad}%, {cores}."

#. Translators: Input help mode message about memory info command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:433
msgid "Presents the used and total space for both physical and virtual ram."
msgstr ""
"Annuncia lo spazio utilizzato e totale per la ram sia fisica che virtuale."

#. Translators: Shows RAM (physical memory) usage.
#: addon/globalPlugins/resourceMonitor/__init__.py:439
#, python-brace-format
msgid "Physical: {physicalUsed} of {physicalTotal} used ({physicalPercent}%). "
msgstr "Fisica: {physicalUsed} su {physicalTotal} usata ({physicalPercent}%). "

#. Translators: Shows virtual memory usage.
#: addon/globalPlugins/resourceMonitor/__init__.py:446
#, python-brace-format
msgid "Virtual: {virtualUsed} of {virtualTotal} used ({virtualPercent}%)."
msgstr ""
"Virtuale: {virtualUsed} su {virtualTotal} utilizzati ({virtualPercent}%)."

#. Translators: Input help mode message about Windows version command in Resource Monitor.
#: addon/globalPlugins/resourceMonitor/__init__.py:458
msgid "Announces the version of Windows you are using."
msgstr "Annuncia la versione di Windows in uso."

#. Translators: system uptime in days.
#: addon/globalPlugins/resourceMonitor/__init__.py:479
msgid "1 day"
msgstr "1 giorno"

#. Translators: system uptime in days.
#: addon/globalPlugins/resourceMonitor/__init__.py:481
#, python-brace-format
msgid "{0} days"
msgstr "{0} giorni"

#. Translators: system uptime in seconds.
#: addon/globalPlugins/resourceMonitor/__init__.py:499
msgid "1 second"
msgstr "1 secondo"

#. Translators: system uptime in seconds.
#: addon/globalPlugins/resourceMonitor/__init__.py:501
#, python-brace-format
msgid "{0} seconds"
msgstr "{0} secondi"

#. Translators: Input help mode message about obtaining the system's uptime
#: addon/globalPlugins/resourceMonitor/__init__.py:507
msgid "Announces the system's uptime."
msgstr "Legge il tempo di attività del sistema."

#. Translators: Obtaining uptime failed
#: addon/globalPlugins/resourceMonitor/__init__.py:519
msgid "Failed to get the system's uptime."
msgstr "Errore nello stabilire il tempo di attività del sistema."

#. Translators: Input help mode message about overall system resource info command in Resource Monitor
#: addon/globalPlugins/resourceMonitor/__init__.py:523
msgid ""
"Presents used ram, average processor load, and battery info if available."
msgstr ""
"Annuncia la quantità di ram usata, il carico medio del processore e "
"informazioni sulla batteria, se disponibili."

#: addon/globalPlugins/resourceMonitor/__init__.py:530
#, python-brace-format
msgid "{ramPercent}% RAM used, CPU at {cpuPercent}%."
msgstr "{ramPercent}% di RAM usata, CPU al {cpuPercent}%."

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
#: buildVars.py:26
msgid ""
"A handy resource monitor to report CPU load, memory usage, battery, disk "
"usage status and more."
msgstr ""
"Un'utile analizzatore di risorse per segnalare il carico della CPU, "
"l'utilizzo della memoria, lo stato della batteria,  la quantità di spazio "
"utilizzato sul disco ed altro."
