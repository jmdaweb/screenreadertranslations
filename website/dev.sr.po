# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-10-04 18:13+0000\n"
"PO-Revision-Date: 2021-07-23 16:35+0100\n"
"Last-Translator: Nikola Jović <wwenikola123@gmail.com>\n"
"Language-Team: \n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Add-ons under development\"]]\n"
msgstr "[[!meta title=\"Dodaci u razvoju\"]]\n"

#. type: Plain text
msgid ""
"The following add-ons are currently under development by members of the "
"community."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline  pages=\"tagged(dev) and currentlang()\" archive=\"yes\"]]\n"
msgstr "[[!inline  pages=\"tagged(dev) and currentlang()\" archive=\"yes\"]]\n"

#~ msgid ""
#~ "The following add-ons are currently under development by members of the "
#~ "community.  If you wish to give us feedback or want to report unexpected "
#~ "behaviour you can do so on the [add-ons mailing list][2].  The source "
#~ "code repositories of these add-ons can be listed [here][1], patches "
#~ "welcome."
#~ msgstr ""
#~ "Sledeći dodaci su trenutno u fazi razvoja od strane zajednice.  Ako "
#~ "želite da date informacije ili  prijavite neočekivano ponašanje to  "
#~ "možete uraditi na [mejling listi dodataka ][2].  Izvorni kod ovih "
#~ "dodataka se može pronaći [ovde][1], saradnja je dobrodošla."

#~ msgid ""
#~ "If you would like to submit an add-on for inclusion or update information "
#~ "for an existing add-on, please ensure your add-on conforms with our "
#~ "[guidelines][3], and email the [NVDA Add-ons email list][2].  Include the "
#~ "name of the add-on, a short description, its author, the current version "
#~ "number, a web site (if any) and a URL from which it can be downloaded.  "
#~ "Each community add-on goes through our standardized process described "
#~ "[here][4].  Add-ons will be added at the discretion of the community "
#~ "reviewers, considering factors such as quality, usefulness and interest "
#~ "from the wider community.  Add-ons no longer actively maintained or "
#~ "incompatible with future NVDA releases will be listed under legacy "
#~ "section.  Please be aware that this process may take some time."
#~ msgstr ""
#~ "Ako želite da pošaljete neki dodatak ili ažurirate informacije za "
#~ "postojeći, molimo uverite se da vaš dodatak poštuje naše [smernice][3], i "
#~ "pošaljite Email na [mejling listu NVDA dodataka ][2].  Uključite ime "
#~ "dodatka, kratak opis, njegovog autora, broj trenutne verzije, sajt (ako "
#~ "postoji ) i adresu sa koje se može preuzeti.  Svaki dodatak zajednice "
#~ "prolazi standardan proces opisan [ovde ][4].  Dodaci će biti dodati nakon "
#~ "pregleda zajednice, koja će uzeti u obzir faktore kao što su kvalitet, "
#~ "korisnost i zainteresovanost šire zajednice.  Dodaci koji se više ne "
#~ "održavaju ili koji nisu kompatibilni sa budućim NVDA verzijama biće "
#~ "prebačeni u sekciju za stare dodatke.  Molimo imajte na umu da ovaj "
#~ "proces može potrajati neko vreme."

#~ msgid "[1]: https://github.com/nvdaaddons/nvdaaddons.github.io/wiki/repos"
#~ msgstr "[1]: https://github.com/nvdaaddons/nvdaaddons.github.io/wiki/repos"

#~ msgid "[2]: https://nvda-addons.groups.io/g/nvda-addons"
#~ msgstr "[2]: https://nvda-addons.groups.io/g/nvda-addons"

#~ msgid "[3]: https://addons.nvda-project.org/files/get.php?file=gl"
#~ msgstr "[3]: https://addons.nvda-project.org/files/get.php?file=gl"

#~ msgid "[4]: https://addons.nvda-project.org/processes"
#~ msgstr "[4]: https://addons.nvda-project.org/processes"
