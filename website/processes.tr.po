# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2020-10-24 07:26+1000\n"
"PO-Revision-Date: 2021-07-08 08:34+0300\n"
"Last-Translator: Çağrı Doğan <cagrid@hotmail.com>\n"
"Language-Team: \n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"NVDA add-on development and review processes\"]]\n"
msgstr "[[!meta title=\"NVDA eklenti geliştirme ve inceleme süreci\"]]\n"

#. type: Plain text
msgid ""
"Thank you for your interest in developing and submitting your NVDA add-on "
"for community distribution.  Since 2013, the NVDA community add-ons website "
"is home to many add-on packages from authors like you."
msgstr ""
"Toplu dağıtım için NVDA eklentinizi geliştirmeye ve göndermeye gösterdiğiniz "
"ilgi için teşekkür ederiz. 2013 YILINDAN bu zamana kadar, NVDA topluluğu "
"eklentiler web sitesi sizin gibi yazarlardan gelen birçok eklenti paketine "
"ev sahipliği YAPIYOR."

#. type: Plain text
msgid ""
"The goal of this document is to serve as a one-stop guide on how the "
"community review works, expectations, and to provide tips on effective add-"
"on development and reviews.  For more information on add-on development, "
"please read NVDA Add-on Development Guide."
msgstr ""
"Bu belgenin amacı, topluluk inceleme sürecinin nasıl işlediği, Beklentilerin "
"neler olduğu ve etkili eklenti geliştirme hakkında ipuçları gibi konularda "
"tek noktada  bir klavuz olmaktır. Eklenti geliştirme hakkında daha fazla "
"bilgi için lütfen NVDA Eklenti Geliştirme Kılavuzunu okuyun."

#. type: Plain text
msgid ""
"IMPORTANT: by submitting your add-on for community distribution, you "
"acknowledge that you will abide by NVDA community code of conduct."
msgstr ""
"ÖNEMLİ: Eklentinizi toplu dağıtıma göndererek NVDA topluluk kurallarına "
"uyacağınızı kabul etmiş olursunuz."

#. type: Plain text
msgid "The overall add-on development and review processes is as follows:"
msgstr "Genel eklenti geliştirme ve inceleme süreçleri aşağıdaki gibidir:"

#. type: Title ##
#, no-wrap
msgid "Prepare"
msgstr "Hazırlıklar"

#. type: Plain text
msgid ""
"To develop an NVDA add-on, you will need a copy of NVDA.  Although stable "
"versions will work, we recommend using beta releases, or if you are really "
"adventurous, alpha versions."
msgstr ""
"Bir NVDA eklentisi geliştirmek için NVDA'nın bir kopyasına ihtiyacınız var. "
"Kararlı sürümler kullanılabilse de, beta sürümlerini veya gerçekten "
"maceracıysanız alfa sürümlerini kullanmanızı öneririz."

#. type: Plain text
msgid ""
"You will also need to install Python 3.7 or later for testing your add-on "
"idea, and if using NVDA community add-on template, to package your add-on.  "
"Also, you will need access to the app you are writing an app module for, and "
"software and hardware necessary for developing synthesizers or braille "
"display driver add-ons."
msgstr ""
"Eğer Eklenti fikrinizi test etmek, NVDA topluluk eklenti şablonunu kullanmak "
"ve eklentinizi paketlemek isterseniz, Python 3.7 veya sonraki bir sürümünü "
"yüklemeniz gerekir. Ayrıca, modül yazdığınız uygulamaya, sentezleyiciye, "
"braille ekran sürücü eklentileri geliştirmek için gerekli yazılıma ve "
"donanıma erişiminiz olması gerekir."

#. type: Plain text
msgid ""
"Lastly, subscribe to NVDA add-ons mailing list to become familiar with the "
"add-ons community.  The forum is designed to help you develop your add-on by "
"letting users give you feedback.  It is also used to facilitate add-on "
"reviews and new version announcements."
msgstr ""
"Son olarak, eklentiler topluluğunu daha iyi tanımak için NVDA eklentiler "
"posta listesine abone olun. Forum kullanıcıların size geri bildirimde "
"bulunmasına izin vererek eklentinizi geliştirmenize yardımcı olmak için "
"tasarlanmıştır. Ayrıca eklenti incelemelerini ve yeni sürüm duyurularını "
"kolaylaştırmak için de kullanılır."

#. type: Plain text
msgid "For other guidance, see [guidelines][2]."
msgstr "Diğer rehberlik için [yönergeler][2]'e bakın."

#. type: Title ##
#, no-wrap
msgid "Getting an add-on reviewed"
msgstr "Bir eklentiyi incelemeye göndermek"

#. type: Plain text
msgid ""
"If you would like to release your add-on to the NVDA community through "
"community add-ons website, it must go through at least one review.  First, "
"have a public link to the development version of your add-on handy, such as "
"the link to the installer, a version control repository such as GitHub, and "
"so on.  This is so that the prospective reviewer can download and test your "
"add-on."
msgstr ""
"Eklentinizi topluluk eklentileri web sitesi üzerinden NVDA topluluğuna "
"yayınlamak istiyorsanız, en az bir incelemeden geçmesi gerekir. İlk olarak, "
"eklentinizin geliştirilme sürümünün GitHub gibi bir sürüm kontrol deposundan "
"kolay indirilebilir bir bağlantısının olması gerekir. Bu, potansiyel gözden "
"geçirenin eklentinizi indirip test edebilmesi içindir."

#. type: Plain text
msgid ""
"Once you have the download link handy, send a message to NVDA add-ons list "
"requesting an add-on review.  You must include the name of your add-on and a "
"brief description as to what it does.  Don't forget to include the public "
"link to your add-on."
msgstr ""
"İndirme bağlantısına sahipseniz, NVDA eklenti listesine eklenti incelemesi "
"talep eden bir mesaj gönderin. Eklentinizin adını ve ne işe yaradığına dair "
"kısa bir açıklama ekleyin. Eklenti bağlantısını eklemeyi unutmayın. "

#. type: Plain text
msgid ""
"Once a review request is received, a prospective reviewer (not the author of "
"the add-on in question) will review your add-on on the following criteria:"
msgstr ""
"İnceleme talebi alındığında, potansiyel bir gözden geçiren (incelenecek "
"eklentinin yazarı değil) eklentinizi aşağıdaki kriterlere göre inceler:"

#. type: Bullet: '* '
msgid ""
"License and copyright: your add-on must be licensed in a way that is "
"compatible with GNU General public license (GPL) 2 or later. This is a must "
"because NVDA is licensed under GNU GPL version 2. To ensure this, at the top "
"of add-on files, include a short copyright header, and if you are unsure, do "
"ask the community for advice."
msgstr ""
"Lisans ve telif hakkı: eklentiniz,GNU General public lisansı (GPL) 2 veya "
"sonraki sürümleriyle uyumlu bir şekilde lisanslanmalıdır. Bu bir "
"zorunluluktur çünkü NVDA, GNU GPL sürüm 2 ile lisanslanmıştır. Bunu sağlamak "
"için eklenti dosyalarının en üstüne kısa bir telif hakkı başlığı ekleyin ve "
"eğer emin değilseniz topluluktan tavsiye isteyin."

#. type: Bullet: '* '
msgid ""
"User experience: your add-on will be used by many people, so it will be "
"subject to user experience tests such as installation, NVDA compatibility, "
"GUI, and so on."
msgstr ""
"Kullanıcı deneyimi: Eklentiniz birçok kişi tarafından kullanılacağı için "
"kurulum, NVDA uyumluluğu, GUI vb. kullanıcı deneyimi testlerine tabi "
"tutulacaktır."

#. type: Bullet: '* '
msgid ""
"Documentation: users will learn a lot about your add-on with add-on "
"documentation. Please make the documentation understandable to users."
msgstr ""
"Belgeler (yardım dosyaları): Kullanıcılar, sağladığınız yardım dosyalarından "
"eklentiniz hakkında çok şey öğrenecekler. Lütfen belgeleri kullanıcılar için "
"anlaşılır bir şekilde oluşturun."

#. type: Bullet: '* '
msgid ""
"Security: because your add-on becomes part of NVDA when activated, it can "
"perform operations such as adding and removing files, accessing the "
"internet, and read and write Windows Registry keys. The NVDA add-ons "
"community takes security of add-ons seriously."
msgstr ""
"Güvenlik: Eklentiniz etkinleştirildiğinde artık NVDA'nın bir parçası "
"olduğundan, dosya ekleme/kaldırma, internete erişme ve Windows Kayıt Defteri "
"anahtarlarını okuma ve yazma gibi işlemleri gerçekleştirebilir. NVDA eklenti "
"topluluğu, eklentilerin güvenliğini ciddiye alır."

#. type: Plain text
msgid ""
"In the course of a basic review, a reviewer can approve the add-on for "
"distribution or suggest changes.  Often reviews are accompanied by comments "
"dealing with user experience and other matters, so take a moment to review "
"these comments.  Once the add-on is approved, it will be queued for "
"distribution on the community add-ons website."
msgstr ""
"Temel bir inceleme sırasında, gözden geçiren kişi eklentiyi dağıtım için "
"onaylayabilir veya değişiklik önerebilir. İncelemelere genellikle kullanıcı "
"deneyimi ve diğer konularla ilgili yorumlar eşlik eder, bu sebepten dolayı "
"yapılan yorumları gözden geçirmek için bir dakikanızı ayırın. Eklenti "
"onaylandıktan sonra topluluk eklentileri web sitesinde dağıtılmak üzere "
"sıraya alınır."

#. type: Plain text
msgid ""
"IMPORTANT: even though the add-on passes other review criteria, if the "
"license is not compatible with GPL 2, the add-on will not be approved."
msgstr ""
"ÖNEMLİ: Eklenti diğer inceleme kriterlerinden geçse bile, lisans GPL 2 ile "
"uyumlu değilse eklenti onaylanmayacaktır."

#. type: Title ###
#, no-wrap
msgid "In-depth reviews"
msgstr "Derinlemesine inceleme"

#. type: Plain text
msgid ""
"In addition to the basic review (license and copyright, user experience, "
"documentation, security), you can ask the add-ons community for in-depth "
"reviews.  In-depth reviews can include advanced audits such as memory leaks, "
"compatibility with NVDA development snapshots, GUI suggestions, coding "
"style, runtime conflicts with other add-ons, and flagging compatibility "
"issues for different versions of Python.  The add-on must first pass the "
"basic review before in-depth reviews can be requested."
msgstr ""
"Temel incelemeye (lisans ve telif hakkı, kullanıcı deneyimi, belgeler, "
"güvenlik) ek olarak, eklentiler topluluğundan derinlemesine incelemeler "
"isteyebilirsiniz. Ayrıntılı incelemeler, bellek sızıntıları, NVDA geliştirme "
"anlık görüntüleri ile uyumluluk, GUI önerileri, kodlama stili, diğer "
"eklentilerle çalışma zamanı çakışmaları ve farklı Python sürümleri için "
"uyumluluk sorunlarının işaretlenmesi gibi gelişmiş denetimleri içerebilir. "
"Eklenti, derinlemesine incelemeler istenmeden önce temel incelemeyi "
"geçmelidir."

#. type: Title ###
#, no-wrap
msgid "Combining with other add-ons"
msgstr "Diğer eklentilerle birleştirme"

#. type: Plain text
msgid ""
"If it is determined that the add-on under review is similar to another add-"
"on, the community may ask the author(s) to combine add-ons.  In order to do "
"so, community members must point out similarities and differences between "
"candidate add-ons.  If add-on author(s) agree that add-ons can be combined, "
"this fact must be recorded in a subsequent add-on release for the combined "
"add-on."
msgstr ""
"İncelenmekte olan eklentinin başka bir eklentiye benzer olduğu belirlenirse, "
"topluluk yazar(lar)dan eklentileri birleştirmesini isteyebilir. Bunu yapmak "
"için topluluk üyeleri, aday eklentiler arasındaki benzerlikleri ve "
"farklılıkları belirtmelidir. Eklenti yazar(lar)ı, eklentilerin "
"birleştirilebileceğini kabul ederse, bu durum, birleşik eklenti için sonraki "
"bir eklenti yayınında kaydedilmelidir."

#. type: Title ##
#, no-wrap
msgid "Maintenance"
msgstr "Geliştirme"

#. type: Plain text
msgid ""
"Now that your add-on was reviewed and approved, it is time to maintain the "
"add-on.  As you maintain the add-on, you will get feedback from users and "
"other add-on authors.  Do comment on their feedback and keep the "
"communication going."
msgstr ""
"Eklentiniz incelenip onaylandığına göre, artık onu geliştirmek sizin "
"elinizde. Eklentinizde çalışmaya devam ettiğiniz sürece kullanıcılardan ve "
"yazarlardan geribildirim alacaksınız. Yapılan geribildirime cevap vererek "
"iletişimi sürdürün."

#. type: Plain text
msgid ""
"If you are using version control, you may wish to work with multiple "
"branches to work on features or to synchronize with translations workflow.  "
"The \"master\" branch should be considered a development branch, suitable "
"for testing add-ons before release.  If using translations workflow, \"stable"
"\" branch is used to exchange localization content and to release the add-on."
msgstr ""
"Sürüm kontrolü kullanıyorsanız, özellikler üzerinde çalışmak veya çeviri iş "
"akışıyla senkronize etmek için birden çok dal (branch) ile çalışmak "
"isteyebilirsiniz. \"Ana\" dal(master branch), eklentileri yayınlanmadan önce "
"test etmeye uygun bir geliştirme dalı olarak düşünülebilir. Çeviri iş akışı "
"kullanılıyorsa, yerelleştirme içeriğini değiştirmek ve eklentiyi yayınlamak "
"için \"kararlı\" (stable) dal kullanabilirsiniz."

#. type: Plain text
msgid ""
"In case you are no longer able to maintain the add-on, send a message to "
"NVDA add-ons list asking for a new maintainer.  Any member of the community "
"can then volunteer to maintain the add-on.  The new maintainer becomes the "
"author of the add-on, subject to rules set in this document."
msgstr ""
"Eğer artık eklentiyi geliştiremiyorsanız, NVDA eklenti listesine yeni bir "
"geliştirici bulmak için mesaj gönderin. Topluluğun herhangi bir üyesi daha "
"sonra eklentiyi geliştirmek için gönüllü olabilir. Yeni geliştirici, bu "
"belgede belirlenen kurallara tabi olarak eklentinin yeni yazarı olur."

#. type: Title ##
#, no-wrap
msgid "Releasing a new version of an add-on"
msgstr "Bir eklentinin yeni bir sürümünü yayınlama"

#. type: Plain text
msgid ""
"From time to time you will release updated add-on versions to add new "
"features or fix bugs.  In some cases you will release an add-on to make it "
"compatible with newer NVDA releases."
msgstr ""
"Zaman zaman yeni özellikler eklemek veya hataları düzeltmek veya Bazı "
"durumlarda, eklentinizi daha yeni NVDA sürümleriyle uyumlu hale getirmek "
"için yeni bir eklenti sürümü yayınlayacaksınız."

#. type: Plain text
msgid ""
"When you are ready to release a new version or shortly after doing so, send "
"a message to NVDA add-ons list, informing people of new releases.  Provide "
"the new version and a description of what was changed since the last release."
msgstr ""
"Yeni bir sürüm yayınlamaya hazır olduğunuzda, NVDA eklenti listesine bir "
"mesaj göndererek topluluğu yeni sürüm hakkında bilgilendirin. Yeni sürümü ve "
"son sürümden bu yana nelerin değiştiğinin bir açıklamasını yazın."

#. type: Plain text
msgid ""
"If you feel you would like a review from community reviewers, do let people "
"know.  This is applicable if you need to make changes regarding license."
msgstr ""
"Topluluk inceleyicilerinden bir inceleme gerektiğini düşünüyorsanız, "
"topluluğa bildirin. Bu, lisansla ilgili değişiklik yapmanız gerektiğinde "
"geçerlidir."

#. type: Title ##
#, no-wrap
msgid "Add-on removal"
msgstr "Eklentinin kaldırılması"

#. type: Plain text
msgid ""
"If harmful code is discovered while an add-on is listed on community add-ons "
"website, it will be subject to removal.  Harmful code can include "
"downloading files without permission, adding, renaming, or removing files "
"outside of add-on scope without permission, executing external programs "
"outside of add-on scope, and compromising NVDA's functionality with "
"malicious intent.  A special case is inclusion of code that is not "
"compatible with GPL or parts of proprietary licensed code without permission "
"from the code author."
msgstr ""
"Topluluk eklentiler web sitesinde bir eklenti dağıtılırken zararlı bir kod "
"bulunursa, bahsi geçen eklenti kaldırılmaya tabi olacaktır. Zararlı kod, "
"izinsiz dosya indirmeyi, izinsiz eklenti kapsamı dışındaki dosyaları "
"eklemeyi, dosyaları yeniden adlandırmayı veya kaldırmayı, eklenti kapsamı "
"dışında harici programları çalıştırmayı ve kötü niyetli olarak NVDA'nın "
"işlevselliğini bozmayı içerebilir. Özel bir durum, kod yazarının izni "
"olmadan GPL ile uyumlu olmayan kodun veya tescilli lisanslı kodun "
"bölümlerinin dahil edilmesidir."

#. type: Plain text
msgid ""
"If harmful code is suspected or actually found, a member of the community "
"must inform NVDA add-ons list of this issue.  The reporter must provide the "
"add-on name, version, author, and specific harmful code.  If the community "
"investigation determines that harmful code was indeed found, the author of "
"the add-on must be contacted.  The author must respond to community findings "
"and take action such as replacing the harmful code, re-licensing it (subject "
"to basic review again), or asking the community to remove the add-on from "
"add-ons website.  If the author does not take follow-up action, the add-on "
"in question will be removed from community add-ons website."
msgstr ""
"Zararlı koddan şüpheleniliyorsa veya bulunduysa, topluluğun bir üyesi bu "
"sorunu NVDA eklentileri listesine bildirmelidir. Raporlayan kişi, eklentinin "
"adını, sürümünü, yazarını ve zararlı olan kodu sağlamalıdır. Topluluk "
"araştırması eklentide gerçekten zararlı kodun bulunduğunu tespit ederse, "
"eklentinin yazarıyla iletişime geçilmelidir. Yazar, topluluğa yanıt vermeli "
"ve zararlı kodu değiştirmek, yeniden lisanslamak (tekrar temel incelemeye "
"tabidir) veya topluluktan eklentiyi eklentiler  web sitesinden kaldırmasını "
"istemek gibi eylemlerde bulunmalıdır. Yazar cevap vermezse, söz konusu "
"eklenti topluluk eklentiler web sitesinden kaldırılacaktır."

#. type: Title ##
#, no-wrap
msgid "Legacy status"
msgstr "Eski eklentilerin durumu"

#. type: Plain text
msgid ""
"Sometimes an add-on can be flagged as a legacy add-on.  This can happen if "
"the add-on becomes incompatible with most recent backwards incompatible "
"version of NVDA (for example, 2019.3).  This can also happen if features "
"from the add-on becomes part of NVDA (Screen Curtain, for example) or is "
"transferred to another add-on."
msgstr ""
"Bazen bir eklenti, eski bir eklenti olarak işaretlenebilir. Bu durum "
"eklentinin, NVDA'nın en son geriye dönük uyumsuz sürümüyle (örneğin, "
"2019.3)ile uyumsuz hale gelirse, eklentideki özellikler NVDA'nın (örneğin "
"Ekran Perdesi) parçası haline gelirse veya başka bir eklentiye aktarılırsa "
"da gerçekleşebilir."

#. type: Plain text
msgid ""
"If an add-on should be declared as a legacy add-on, the add-on author or a "
"community member must inform the NVDA add-ons list, preferably by the "
"author.  The reporter must provide add-on name, version, and reasons for "
"flagging it as a legacy add-on such as features incorporated into NVDA.  "
"With an a greement from the add-on author and the community, the add-on in "
"question will become a legacy add-on."
msgstr ""
"Bir eklentinin eski bir eklenti olarak işaretlenmesi gerekiyorsa, eklenti "
"yazarı veya bir topluluk üyesi, (söz konusu eklenti yazarı tercih edilir) "
"NVDA eklentiler listesini bilgilendirmesi gerekir. Bilgilendiren kişi, "
"eklentinin adını, sürümünü ve NVDA'ya dahil edilen özellikler gibi eski bir "
"eklenti olarak işaretlenme gerekçelerini sağlamalıdır. Eklenti yazarı ve "
"topluluğun anlaşması sonucu söz konusu eklenti eski bir eklenti olarak "
"işaretlenir."

#. type: Plain text
msgid ""
"The opposite can happen where a legacy add-on is no longer flagged as such.  "
"The same procedure as flagging an add-on as a legacy add-on should be "
"followed, except telling the community reasons for removing the add-on from "
"legacy status.  The add-on will be removed from legacy status if the author "
"and the community determines that the add-on is no longer a legacy add-on.  "
"Maintenance priority goes to the author of the newly changed add-on, or a "
"member of the community can volunteer to maintain the add-on."
msgstr ""
"Eski bir eklentinin artık eski olarak kabul edilmediği durumlarda yukarıdaki "
"durumun tersi olabilir. Topluluğa eklentinin eski durumunun kaldırılmasının "
"nedenlerini açıklamak dışında, bu durumda da bir eklentiyi eski bir eklenti "
"olarak işaretlemekle aynı prosedür izlenmelidir. Yazar ve topluluk, "
"eklentinin artık eski bir eklenti olmadığına karar verirse, eklenti eski "
"durumundan kaldırılacaktır. Geliştirici önceliği, yeni değiştirilen "
"eklentinin yazarına aittir veya topluluğun bir üyesi, eklentiyi geliştirmek "
"için gönüllü olabilir."

#. type: Plain text
msgid "[1]: https://nvda-addons.groups.io/g/nvda-addons"
msgstr "[1]: https://nvda-addons.groups.io/g/nvda-addons"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=gl"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=gl"

#~ msgid "Acceptance"
#~ msgstr "Kabul"

#~ msgid "A developer writes an add-on."
#~ msgstr "Geliştirici eklentiyi yazar."

#~ msgid "Developer submits the add-on to the community."
#~ msgstr "Geliştirici eklentiyi topluluğa gönderir."

#~ msgid ""
#~ "Community considers the possibility of accepting the add-on through basic "
#~ "review (licensing, copyright considerations, security implications, etc.)."
#~ msgstr ""
#~ "Camia eklentinin temel inceleme sürecinde (Lisans, telif hakları "
#~ "konuları, güvenlik sorunları, vb.) açısından kabul edilip edilmeyeceğini "
#~ "değerlendirir."

#~ msgid "Release and Maintenance"
#~ msgstr "Yeni sürüm ve bakım"

#~ msgid ""
#~ "Author releases the add-on and incorporates comments from reviewers and "
#~ "users."
#~ msgstr ""
#~ "Yazar eklentiyi yayınlar ve yorumcular ve kullanıcılardan gelen geri "
#~ "bildirimleri paylaşır. "

#~ msgid ""
#~ "Authors and reviewers should remove add-ons that contain harmful code, or "
#~ "if they are incompatible, if NVDA implements their functionality for "
#~ "instance, notify this into the add-ons mailing list."
#~ msgstr ""
#~ "Yazarlar ya da gözden geçirenler zararlı olabilecek ya da NVDA artık "
#~ "eklentinin sağladığı işlevleri sunduğu için gereksiz hale gelmiş "
#~ "eklentileri eklenti mail listesine duyurarak siteden kaldırmalıdırlar."

#~ msgid ""
#~ "If add-ons no longer in active development, or reported as incompatible "
#~ "with recent NVDA releases will be listed as \"legacy\"."
#~ msgstr ""
#~ "Eklentiler artık aktif olarak geliştirilmiyorsa veya son NVDA "
#~ "sürümleriyle uyumlu olmadığı bildirilirse \"eski\" olarak listelenecektir."

#~ msgid ""
#~ "Add-ons can be reinstated if harmful code has been removed, or add-on is "
#~ "compatible with recent NVDA releases again."
#~ msgstr ""
#~ "Zararlı kod kaldırılırsa veya eklenti en son NVDA sürümleriyle tekrar "
#~ "uyumlu hale getirilirse eklenti statüsü güncellenebilir."

#~ msgid "Notes"
#~ msgstr "Notlar"

#~ msgid ""
#~ "If an add-on author is also a community reviewer s/he can not be the "
#~ "community reviewer for his/her add-ons at the same time."
#~ msgstr ""
#~ "Eklenti yazarı aynı zamanda eklenti değerlendirme topluluğu üyesiyse, "
#~ "kendi eklentisini değerlendiremez."

#~ msgid ""
#~ "Users can be considered reviewers when they provide significant feedback, "
#~ "such as reporting bugs, suggestions, etc."
#~ msgstr ""
#~ "Önerilerde bulunmak, sorun bildirmek gibi anlamlı geri bildirimde bulunan "
#~ "kullanıcılar da eklenti değerlendiren topluluğun üyesi sayılırlar."

#~ msgid ""
#~ "Code will be considered to be harmful if it changes, deletes, or copies "
#~ "system files, or any external files without a clear and useful purpose, "
#~ "produces internet connections without a justified reason, etc."
#~ msgstr ""
#~ "Yalnızca bu koşullarla sınırlı olmamakla birlikte, açık ve yararlı bir "
#~ "amaç olmaksızın sistem veya harici dosyaları değiştirme, silme ya da "
#~ "kopyalama, haklı bir sebep olmadan İnternet bağlantısı üretme vb "
#~ "durumlarda kod zararlı olarak kabul edilecektir."

#~ msgid ""
#~ "Authors should discuss about circumstances of add-ons if their add-on is "
#~ "not clear for copyrights, and licensing (such as synthesizer's licensing)."
#~ msgstr ""
#~ "Yazarlar telif hakkı ve lisans meselelerinin gri alana girdiğine "
#~ "inanırlarsa eklentileri bu açıdan tartışmalıdır. Örneğin, sentezleyici "
#~ "lisansları..."

#~ msgid ""
#~ "For add-ons, master branch is equivalent to NVDA alpha, i.e. considered "
#~ "for testing purposes. Stable branch is considered to have had some "
#~ "testing and should be relatively error free/ok to run on your system."
#~ msgstr ""
#~ "Eklentiler için, master branşı NVDA Next (beta) branşına eş değerdir. Ana "
#~ "sürümlerse belli testlerden geçmiş ve sistemde kurulmasının sorun "
#~ "yaratmayacağı değerlendirilmiş sürümlerdir."

#~ msgid ""
#~ "Stable branch receives translation updates if the add-on is translatable."
#~ msgstr "Kararlı sürüm destek mevcutsa çeviri güncellemeleri alabilir."

#~ msgid ""
#~ "Authors and reviewers are strongly encouraged to follow our [guidelines]"
#~ "[2], to ensure the quality of community add-ons."
#~ msgstr ""
#~ "Eklenti kalitesinin korunması açısından, yazar ve değerlendirenlerin "
#~ "eklenti geliştirme süreciyle ilgili [yönergeyi][2] takip etmesi önemlidir."

#~ msgid ""
#~ "If any add-on author can not continue their work since first request that "
#~ "author can be replaced by another maintainer in three months."
#~ msgstr ""
#~ "Yazar ya da yazarlar eklenti için gerekli bir çalışmayı yapamayacak "
#~ "durumdalarsa, çalışma talebinden üç ay sonra eklenti yazımı ve bakımından "
#~ "sorumlu olan kişi değiştirilebilir."

#~ msgid "[[!meta title=\"Processes\"]]\n"
#~ msgstr "[[!meta title=\"Süreç\"]]\n"

#~ msgid "If requested by the author, perform more rigorous review."
#~ msgstr ""
#~ "Yazar tarafından talep edilmesi halinde, daha sıkı bir inceleme yapın."

#~ msgid "Check that the add-on contains interesting and/or useful features."
#~ msgstr ""
#~ "Eklentinin ilginç / yararlı özellikler ekleyip eklemediğini  kontrol edin."

#~ msgid ""
#~ "Perhaps, it can be merged with an existing add-on, or included into the "
#~ "NVDA core?"
#~ msgstr ""
#~ "Belki mevcut bir eklentiyle ya da NVDA'nın kendisiyle birleştirilebilir?"

#~ msgid ""
#~ "If community considers the add-on to be useful, if accepted the add-on is "
#~ "now in development status."
#~ msgstr ""
#~ "Eğer topluluk eklentiyi faydalı olarak değerlendirirse, kabul edilir. "
#~ "Eklenti statüsü artık Dev yani geliştirilmekte olandır."

#~ msgid "Author requests reviews."
#~ msgstr "Yazar yorumları ister. "

#~ msgid ""
#~ "For major releases: Before major version is released to check for license "
#~ "changes and other basic review."
#~ msgstr ""
#~ "ana sürümler için: ana sürüm yayınlanmadan önce lisans değişiklikleri ve "
#~ "diğer temel inceleme kontrolü için."

#~ msgid "For continuous releases: At least once a year."
#~ msgstr "Sürekli sürümler için, yılda en az bir kez."

#~ msgid ""
#~ "Reviewers and testers check the add-on that doesn't contain harmful code, "
#~ "and authorize to post it on the add-ons website."
#~ msgstr ""
#~ "Gözden geçirenler ve deneyenler eklentinin zararlı kod içerip "
#~ "içermediğini kontrol eder ve sitede yer verilmesini onaylar."

#~ msgid ""
#~ "Authors or reviewers (preferably authors) can release maintainance "
#~ "versions of the add-on to update translations, or fix critical issues, "
#~ "notifying add-on changes on [add-ons mailing list][1]."
#~ msgstr ""
#~ "Yazarlar ya da incelemeciler (tercihen yazarlar) çevirileri güncellemek "
#~ "veya kritik sorunları gidermek amacıyla, değişiklikleri [eklenti mail "
#~ "grubunda][1] duyurarak eklentinin bakım sürümlerini yayınlayabilir."

#~ msgid "Deletion"
#~ msgstr "Silinir"

#~ msgid ""
#~ "Author continues the add-on development, according with improvements in "
#~ "NVDA's core, reviewers and users feedback, provided patches and other "
#~ "community add-ons."
#~ msgstr ""
#~ "Yazar NVDA'daki geliştirmeler, gözden geçirenler ve kullanıcıların "
#~ "geribildirimleri,  sağlanan yamalar ve diğer topluluk eklentilerine göre "
#~ "eklentiyi geliştirmeye devam eder."

#~ msgid ""
#~ "Author requests add-on revision at least two or four weeks before "
#~ "releasing a new major version."
#~ msgstr ""
#~ "Yazar, yeni bir sürüm çıkarmadan önce en az 2-4 hafta eklentinin gözden "
#~ "geçirilip denenmesini ister."

#~ msgid "NV Access, as a trust author, could bypass this process if needed."
#~ msgstr ""
#~ "NVAccess güvenilir bir geliştirici olarak gerekirse bu süreci atlayabilir."
