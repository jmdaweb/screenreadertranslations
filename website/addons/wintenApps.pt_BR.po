# Brazilian Portuguese translation of wintenApps.pt_BR.po
# Copyright (C) 2018-2022 NVDA Contributors.
# This file is distributed under the same license as the NVDA package.
# Ângelo Abrantes <ampa4374@gmail.com>, 2018.
# Tiago Melo Casal <tcasal@intervox.nce.ufrj.br>, 2018, 2020-2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Windows App Essentials webpage\n"
"POT-Creation-Date: 2022-01-21 05:10+0000\n"
"PO-Revision-Date: 2022-01-14 21:58-0300\n"
"Last-Translator: Tiago Melo Casal <tcasal@intervox.nce.ufrj.br>\n"
"Language-Team: NVDA Brazilian Portuguese translation team (Equipe de "
"tradução do NVDA para Português Brasileiro) <clever97@gmail.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"
"X-Poedit-SourceCharset: UTF-8\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Windows App Essentials\"]]\n"
msgstr "[[!meta title=\"Windows App Essentials\"]]\n"

#. type: Bullet: '* '
msgid "Authors: Joseph Lee, Derek Riemer and others"
msgstr "Autores: Joseph Lee, Derek Riemer e outros"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Baixe a [versão estável][1]"

#. type: Bullet: '* '
msgid "Download [development version][2]"
msgstr "Baixe a [versão em desenvolvimento][2]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2021.2 and later"
msgstr "Compatibilidade com NVDA: 2021.2 e posteriores"

#. type: Plain text
msgid ""
"Note: Originally called Windows 10 App Essentials, it was renamed to Windows "
"App Essentials in 2021 to support Windows 10 and future releases such as "
"Windows 11. Parts of this add-on will still refer to the original add-on "
"name."
msgstr ""
"Nota: Originalmente chamado de Windows 10 App Essentials, foi renomeado para "
"Windows App Essentials em 2021 para oferecer suporte ao Windows 10 e versões "
"futuras, como Windows 11. Partes deste complemento ainda farão referência ao "
"nome do complemento original."

#. type: Plain text
msgid ""
"This add-on is a collection of app modules for various modern Windows apps, "
"as well as enhancements and fixes for certain controls found in Windows 10 "
"and later."
msgstr ""
"Este complemento é uma coleção de módulos de aplicativos (app modules) para "
"vários aplicativos modernos do Windows, bem como aprimoramentos e correções "
"para determinados controles encontrados no Windows 10 e posteriores."

#. type: Plain text
msgid ""
"The following app modules or support modules for some apps are included (see "
"each app section for details on what is included):"
msgstr ""
"Os seguintes módulos de aplicativos ou módulos de suporte para alguns "
"aplicativos estão incluídos (consulte a seção de cada aplicativo para obter "
"detalhes sobre o que está incluso):"

#. type: Title ##
#, no-wrap
msgid "Calculator"
msgstr "Calculadora"

#. type: Title ##
#, no-wrap
msgid "Cortana"
msgstr "Cortana"

#. type: Title ##
#, no-wrap
msgid "Mail"
msgstr "Email"

#. type: Title ##
#, no-wrap
msgid "Maps"
msgstr "Mapas"

#. type: Title ##
#, no-wrap
msgid "Microsoft Solitaire Collection"
msgstr "Microsoft Solitaire Collection (Coleção de Paciência)"

#. type: Bullet: '* '
msgid ""
"Modern keyboard (emoji panel/dictation/voice typing/hardware input "
"suggestions/clipboard history/modern input method editors)"
msgstr ""
"Teclado moderno (painel de emoji/ditado/digitação por voz/sugestões de "
"entrada de hardware/histórico da área de transferência/editores de métodos "
"de entrada modernos)"

#. type: Bullet: '* '
msgid "Notepad (Windows 11)"
msgstr "Bloco de Notas (Windows 11)"

#. type: Title ##
#, no-wrap
msgid "People"
msgstr "Pessoas"

#. type: Bullet: '* '
msgid "Settings (system settings, Windows+I)"
msgstr "Configurações (configurações do sistema, Windows+I)"

#. type: Title ##
#, no-wrap
msgid "Weather"
msgstr "Clima"

#. type: Bullet: '* '
msgid "Miscellaneous modules for controls such as Start Menu tiles"
msgstr "Módulos diversos para controles, como blocos do Menu Iniciar"

#. type: Plain text
msgid "Notes:"
msgstr "Notas:"

#. type: Bullet: '* '
msgid ""
"This add-on requires Windows 10 21H1 (build 19043) or later and is "
"compatible with Windows 11."
msgstr ""
"Este complemento requer o Windows 10 21H1 (compilação 19043) ou posteriores "
"e é compatível com o Windows 11."

#. type: Bullet: '* '
msgid ""
"Although installation is possible, this add-on does not support Windows "
"Enterprise LTSC (Long-Term Servicing Channel) and Windows Server releases."
msgstr ""
"Embora a instalação seja possível, este complemento não oferece suporte para "
"Windows Enterprise LTSC (Long-Term Servicing Channel — Canal de Manutenção "
"de Longo Prazo) e versões do Windows Server."

#. type: Bullet: '* '
msgid "Some add-on features are or will be part of NVDA screen reader."
msgstr ""
"Alguns recursos do complemento são ou farão parte do leitor de tela NVDA."

#. type: Bullet: '* '
msgid ""
"For entries not listed below, you can assume that features are part of NVDA, "
"no longer applicable as the add-on does not support unsupported Windows "
"releases such as old Windows 10 versions, or changes were made to Windows "
"and apps that makes entries no longer applicable."
msgstr ""
"* Para entradas não listadas abaixo, você pode presumir que os recursos "
"fazem parte do NVDA, não são mais aplicáveis, pois o complemento não oferece "
"suporte a versões não suportadas do Windows, como versões antigas do Windows "
"10, ou alterações foram feitas no Windows e aplicativos que tornam as "
"entradas não mais aplicáveis."

#. type: Bullet: '* '
msgid ""
"Some apps support compact overlay mode (always on top in Calculator, for "
"example), and this mode will not work properly with portable version of NVDA."
msgstr ""
"Alguns aplicativos suportam o modo de sobreposição compacta (sempre no topo "
"da Calculadora, por exemplo), e este modo não funcionará corretamente com a "
"versão portátil do NVDA."

#. type: Plain text
msgid ""
"For a list of changes made between each add-on releases, refer to "
"[changelogs for add-on releases][3] document."
msgstr ""
"Para obter uma lista de alterações feitas entre cada lançamento do "
"complemento, consulte o documento [changelogs for add-on releases][3]."

#. type: Title ##
#, no-wrap
msgid "General"
msgstr "Geral"

#. type: Bullet: '* '
msgid ""
"NVDA can announce suggestion count when performing a search in majority of "
"cases, including when suggestion count changes as search progresses. This is "
"now part of NVDA 2021.3."
msgstr ""
"O NVDA pode anunciar a contagem de sugestões ao realizar uma pesquisa na "
"maioria dos casos, incluindo quando a contagem de sugestões muda à medida "
"que a pesquisa avança. Isso agora faz parte do NVDA 2021.3."

#. type: Bullet: '* '
msgid ""
"In addition to UIA event handlers provided by NVDA, the following UIA events "
"are recognized: drag complete, drop target dropped, layout invalidated. With "
"NVDA's log level set to debug, these events will be tracked, and for UIA "
"notification event, a debug tone will be heard if notifications come from "
"somewhere other than the currently active app. Events built into NVDA such "
"as name change and controller for events are tracked from an add-on called "
"Event Tracker."
msgstr ""
"Além dos manipuladores de eventos UIA fornecidos pelo NVDA, os seguintes "
"eventos UIA são reconhecidos: drag complete, drop target dropped, layout "
"invalidated. Com o nível de log do NVDA definido para depuração, esses "
"eventos serão rastreados e, para eventos de notificação UIA, um tom de "
"depuração será ouvido se as notificações vierem de outro lugar que não o "
"aplicativo atualmente ativo. Os eventos integrados ao NVDA, como mudança de "
"nome e controlador de eventos, são rastreados a partir de um complemento "
"chamado Event Tracker."

#. type: Bullet: '* '
msgid ""
"When opening, closing, reordering (Windows 11), or switching between virtual "
"desktops, NVDA will announce active virtual desktop name (desktop 2, for "
"example)."
msgstr ""
"Ao abrir, fechar, reordenar (Windows 11) ou alternar entre áreas de trabalho "
"virtuais, o NVDA anunciará o nome do desktop virtual ativo (área de trabalho "
"2, por exemplo)."

#. type: Bullet: '* '
msgid ""
"NVDA will no longer announce Start menu size text when changing screen "
"resolutions or orientation."
msgstr ""
"O NVDA não anunciará mais o texto do tamanho do menu Iniciar ao alterar as "
"resoluções ou orientação da tela."

#. type: Bullet: '* '
msgid ""
"When arranging Start menu tiles or Action Center quick actions with Alt+Shift"
"+arrow keys, NVDA will announce information on dragged items or new position "
"of the dragged item."
msgstr ""
"Ao organizar os blocos do menu Iniciar ou as ações rápidas da Central de "
"Ações com as teclas Alt+Shift+setas, o NVDA anuncia informações sobre itens "
"arrastados ou nova posição do item arrastado."

#. type: Bullet: '* '
msgid ""
"Announcements such as volume/brightness changes in File Explorer and app "
"update notifications from Microsoft Store can be suppressed by turning off "
"Report Notifications in NVDA's object presentation settings."
msgstr ""
"Anúncios como alterações de volume/brilho no Explorador de Arquivo e "
"notificações de atualização de aplicativos da Microsoft Store podem ser "
"suprimidos desativando Anunciar Notificações nas configurações de "
"apresentação de objetos do NVDA."

#. type: Bullet: '* '
msgid "NVDA will no longer announce graphing calculator screen message twice."
msgstr ""
"O NVDA não anunciará mais a mensagem de tela calculadora gráfica duas vezes."

#. type: Bullet: '* '
msgid "In Windows 10, history and memory list items are properly labeled."
msgstr ""
"No Windows 10, os itens de lista do histórico e memória são devidamente "
"rotulados."

#. type: Bullet: '* '
msgid "Textual responses from Cortana are announced in most situations."
msgstr ""
"As respostas textuais da Cortana são anunciadas na maioria das situações."

#. type: Bullet: '* '
msgid "NVDA will be silent when talking to Cortana via voice."
msgstr "O NVDA ficará em silêncio ao falar com Cortana via voz."

#. type: Bullet: '* '
msgid ""
"When reviewing items in messages list, you can now use table navigation "
"commands to review message headers. Note that navigating between rows "
"(messages) is not supported."
msgstr ""
"Ao explorar itens na lista de mensagens, agora pode usar os comandos de "
"navegação da tabela para explorar os respectivos cabeçalhos. Note que a "
"navegação entre linhas (mensagens) não é suportada."

#. type: Bullet: '* '
msgid "NVDA plays location beep for map locations."
msgstr "NVDA reproduz sinal sonoro de localização para locais no mapa."

#. type: Bullet: '* '
msgid ""
"When using street side view and if \"use keyboard\" option is enabled, NVDA "
"will announce street addresses as you use arrow keys to navigate the map."
msgstr ""
"Ao usar a vista lateral da rua e se a opção \"Usar teclado\" estiver "
"ativada, o NVDA anunciará os endereços de rua enquanto usa as teclas de seta "
"para navegar no mapa."

#. type: Bullet: '* '
msgid "NVDA will announce names of cards and card decks."
msgstr "O NVDA anunciará os nomes das cartas e dos baralhos."

#. type: Title ##
#, no-wrap
msgid "Modern keyboard"
msgstr "Teclado moderno"

#. type: Plain text
msgid ""
"This includes emoji panel, clipboard history, dictation/voice typing, "
"hardware input suggestions, and modern input method editors for certain "
"languages. When viewing emojis, for best experience, enable Unicode "
"Consortium setting from NVDA's speech settings and set symbol level to \"some"
"\" or higher. When pasting from clipboard history in Windows 10, press Space "
"key instead of Enter key to paste the selected item. NVDA also supports "
"updated input experience panel in Windows 11."
msgstr ""
"Isso inclui painel de emoji, histórico da área de transferência, ditado/"
"digitação por voz, sugestões de entrada de hardware e editores de métodos de "
"entrada modernos para determinados idiomas. Ao visualizar emojis, para "
"melhor experiência, habilite a configuração de Consórcio Unicode das "
"configurações de voz do NVDA e defina o nível do símbolo para \"pouco\" ou "
"superior. Ao colar do histórico da área de transferência no Windows 10, "
"pressione a tecla Espaço em vez da tecla Enter para colar o item "
"selecionado. O NVDA também suporta painel de experiência de entrada "
"atualizado no Windows 11."

#. type: Bullet: '* '
msgid ""
"In Windows 10, when an emoji group (including kaomoji and symbols group) is "
"selected, NVDA will no longer move navigator object to certain emojis."
msgstr ""
"No Windows 10, quando um grupo de emoji (incluindo kaomoji e grupo de "
"símbolos) é selecionado, o NVDA não moverá mais a navegação de objeto para "
"certos emojis."

#. type: Bullet: '* '
msgid ""
"Added support for updated input experience panel (combined emoji panel and "
"clipboard history) in Windows 11."
msgstr ""
"Adicionado suporte para painel de experiência de entrada atualizado (painel "
"de emoji combinado e histórico da área de transferência) no Windows 11."

#. type: Bullet: '* '
msgid ""
"In Windows 11, it is again possible to use the arrow keys to review emojis "
"when emoji panel opens."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Notepad"
msgstr "Bloco de Notas"

#. type: Plain text
msgid "This refers to Windows 11 Notepad version 11 or later."
msgstr ""
"Isso se refere à versão 11 ou posterior do Bloco de Notas do Windows 11."

#. type: Bullet: '* '
msgid ""
"NVDA will announce status items such as line and column information when "
"report status bar command (NVDA+End in desktop layout, NvDA+Shift+End in "
"laptop layout) is performed."
msgstr ""
"O NVDA anunciará itens de status, como informações de linha e coluna quando "
"o comando de informar barra de status (NVDA+End no leiaute de computador de "
"mesa, NvDA+Shift+End no leiaute de computador portátil) for executado."

#. type: Bullet: '* '
msgid ""
"NVDA will no longer announce entered text when pressing Enter key from the "
"document."
msgstr ""
"O NVDA não anunciará mais o texto inserido ao pressionar a tecla Enter no "
"documento."

#. type: Bullet: '* '
msgid ""
"When searching for contacts, first suggestion will be announced, "
"particularly if using recent app releases."
msgstr ""
"Ao pesquisar contatos, a primeira sugestão será anunciada, principalmente se "
"estiver usando versões recentes do aplicativo."

#. type: Title ##
#, no-wrap
msgid "Settings"
msgstr "Configurações"

#. type: Bullet: '* '
msgid ""
"Certain information such as Windows Update progress is reported "
"automatically, including Storage sense/disk cleanup widget and errors from "
"Windows Update."
msgstr ""
"Certas informações, como o progresso do Windows Update, são relatadas "
"automaticamente, incluindo o widget Sensor de armazenamento/limpeza de disco "
"e os erros do Windows Update."

#. type: Bullet: '* '
msgid ""
"Progress bar values and other information are no longer announced twice."
msgstr ""
"Os valores da barra de progresso e outras informações já não são anunciados "
"duas vezes."

#. type: Bullet: '* '
msgid ""
"Odd control labels seen in certain Windows installations has been corrected."
msgstr ""
"Foram corrigidos Rótulos de controle estranhos vistos em certas instalações "
"do Windows."

#. type: Bullet: '* '
msgid ""
"NVDA will announce the name of the optional quality update control if "
"present (download and install now link in Windows 10, download button in "
"Windows 11)."
msgstr ""
"O NVDA anunciará o nome do controle de atualização de qualidade opcional, se "
"houver (link para baixar e instalar agora no Windows 10, botão de download "
"no Windows 11)."

#. type: Bullet: '* '
msgid "In Windows 11, breadcrumb bar items are properly recognized."
msgstr ""
"No Windows 11, os itens da barra de localização atual são reconhecidos "
"corretamente."

#. type: Bullet: '* '
msgid ""
"Tabs such as \"forecast\" and \"maps\" are recognized as proper tabs (patch "
"by Derek Riemer)."
msgstr ""
"Abas como \"previsão\" e \"mapas\" são reconhecidas como guias adequadas "
"(patch de Derek Riemer)."

#. type: Bullet: '* '
msgid ""
"When reading a forecast, use the left and right arrows to move between "
"items. Use the up and down arrows to read the individual items. For example, "
"pressing the right arrow might report \"Monday: 79 degrees, partly "
"cloudy, ...\" pressing the down arrow will say \"Monday\" Then pressing it "
"again will read the next item (Like the temperature). This currently works "
"for daily and hourly forecasts."
msgstr ""
"Ao ler uma previsão, use as setas esquerda e direita para se mover entre os "
"itens. Use as setas para cima e para baixo para ler os itens individuais. "
"Por exemplo, pressionar a seta para a direita pode indicar \"Segunda-feira: "
"79 graus, parcialmente nublado, ...\" pressionar a seta para baixo dirá "
"\"Segunda-feira\". Em seguida, pressionar novamente lerá o próximo item "
"(como a temperatura). Atualmente, isso funciona para previsões diárias e "
"horárias."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=w10"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=w10"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"

#. type: Plain text
msgid "[3]: https://github.com/josephsl/wintenapps/wiki/w10changelog"
msgstr "[3]: https://github.com/josephsl/wintenapps/wiki/w10changelog"

#~ msgid "Microsoft Store"
#~ msgstr "Microsoft Store (Loja da Microsoft)"

#, fuzzy
#~| msgid ""
#~| "When downloading content such as apps and movies, NVDA will announce "
#~| "product name and download progress (does not work properly in updated "
#~| "Microsoft Store in Windows 11)."
#~ msgid ""
#~ "When downloading content such as apps and movies, NVDA will announce "
#~ "product name and download progress."
#~ msgstr ""
#~ "Ao baixar conteúdo como aplicativos e filmes, o NVDA anunciará o nome do "
#~ "produto e o andamento do download (não funciona corretamente na Microsoft "
#~ "Store atualizada no Windows 11)."

#~ msgid "Calculator (modern)"
#~ msgstr "Calculadora (moderna)"

#~ msgid "Cortana (Conversations)"
#~ msgstr "Cortana (Conversations)"

#~ msgid ""
#~ "Most items are applicable when using Cortana Conversations (Windows 10 "
#~ "2004 and later)."
#~ msgstr ""
#~ "A maioria dos itens são aplicáveis ao usar Cortana Conversations (Windows "
#~ "10 2004 e posterior)."

#~ msgid ""
#~ "After checking for app updates, app names in list of apps to be updated "
#~ "are correctly labeled."
#~ msgstr ""
#~ "Depois de verificar as atualizações de aplicativos, os nomes dos "
#~ "aplicativos na lista de aplicativos a serem atualizados são rotulados "
#~ "corretamente."

#, fuzzy
#~| msgid ""
#~| "Support for Windows 11 is experimental, and some features will not work "
#~| "(see relevant entries for details)."
#~ msgid ""
#~ "Support for Windows 11 is experimental, and some features will not work "
#~ "(see relevant entries for details). A warning dialog will be shown if "
#~ "trying to install stable versions of this add-on on Windows 11 prior to "
#~ "general availability."
#~ msgstr ""
#~ "O suporte para Windows 11 é experimental e alguns recursos não "
#~ "funcionarão (consulte as entradas relevantes para obter detalhes)."

#~ msgid ""
#~ "When searching in Start menu or File Explorer in Windows 10 1909 "
#~ "(November 2019 Update) and later, instances of NVDA announcing search "
#~ "results twice when reviewing results are less noticeable, which also "
#~ "makes braille output more consistent when reviewing items."
#~ msgstr ""
#~ "Ao pesquisar no menu Iniciar ou no Explorador de arquivos do Windows 10 "
#~ "1909 (Atualização de Novembro de 2019) e posteriores, casos de NVDA "
#~ "anunciando resultados de pesquisa duas vezes ao explorar resultados são "
#~ "menos perceptíveis, o que também torna a saída em braille mais "
#~ "consistente ao explorar os itens."

#~ msgid ""
#~ "In Windows 10 1909 (November 2019 Update) and later, modern search "
#~ "experience in File Explorer powered by Windows Search user interface is "
#~ "supported."
#~ msgstr ""
#~ "No Windows 10 1909 (Atualização de Novembro de 2019) e posteriores, a "
#~ "experiência de pesquisa moderna no Explorador de Arquivos alimentado pela "
#~ "interface de usuário de Pesquisa do Windows é suportada."

#~ msgid ""
#~ "When opening clipboard history, NVDA will no longer announce \"clipboard"
#~ "\" when there are items in the clipboard under some circumstances."
#~ msgstr ""
#~ "Ao abrir o histórico da área de transferência, o NVDA não anunciará mais "
#~ "a \"área de transferência\" quando houver itens na área de transferência "
#~ "em algumas circunstâncias."

#~ msgid ""
#~ "On some systems running Windows 10 1903 (May 2019 Update) and later, NVDA "
#~ "will no longer appear to do nothing when emoji panel opens."
#~ msgstr ""
#~ "Nalguns sistemas que executam o Windows 10 1903 (Atualização de Maio de "
#~ "2019) e posteriores, o NVDA não parecerá mais fazer nada quando o painel "
#~ "de emoji for aberto."

#~ msgid "Calendar"
#~ msgstr "Calendário"

#~ msgid ""
#~ "It is possible to tracke only specific events and/or events coming from "
#~ "specific apps."
#~ msgstr ""
#~ "É possível rastrear apenas eventos específicos e/ou eventos provenientes "
#~ "de aplicativos específicos."

#~ msgid ""
#~ "NVDA no longer announces \"edit\" or \"read-only\" in message body and "
#~ "other fields."
#~ msgstr ""
#~ "O NVDA já não anuncia \"editar\" ou \"somente leitura\" no corpo da "
#~ "mensagem e em outros campos."

#~ msgid ""
#~ "When writing a message, appearance of at mention suggestions are "
#~ "indicated by sounds."
#~ msgstr ""
#~ "Ao escrever uma mensagem, o aparecimento de sugestões de menção é "
#~ "indicado por sons."

#~ msgid "Windows Update reminder dialog is recognized as a proper dialog."
#~ msgstr ""
#~ "O diálogo de lembrete do Windows Update é reconhecido como uma caixa de "
#~ "diálogo apropriada."
