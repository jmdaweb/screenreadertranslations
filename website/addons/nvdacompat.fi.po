# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Addon-s and backwards incompatible NVDA releases\n"
"POT-Creation-Date: 2021-07-29 12:53+0000\n"
"PO-Revision-Date: 2021-07-31 14:10+0200\n"
"Last-Translator: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language-Team: fi_FI <janikinnunen340@gmail.com>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Add-ons and backwards incompatible NVDA releases\"]]\n"
msgstr "[[!meta title=\"Lisäosat ja taaksepäin yhteensopimattomat NVDA-versiot\"]]\n"

#. type: Plain text
msgid ""
"Note: this document is based on an older \"NVDA add-ons and Python 3\" "
"document, which is also available for historical reasons."
msgstr ""
"Huom: Tämä asiakirja perustuu aiempaan \"NVDA-lisäosat ja Python 3\" -"
"asiakirjaan, joka on myös edelleen saatavilla historiallisista syistä."

#. type: Plain text
msgid ""
"This page aims to document add-ons compatible with latest backwards "
"incompatible version of NVDA, as well as highlighting their status regarding "
"their availability on this website."
msgstr ""
"Tämän sivun tarkoituksena on dokumentoida viimeisimmän taaksepäin "
"yhteensopimattoman NVDA-version kanssa yhteensopivat lisäosat sekä ilmoittaa "
"niiden tilasta ja saatavuudesta tällä verkkosivustolla."

#. type: Title ##
#, no-wrap
msgid "What you need to know about backwards incompatible NVDA releases and add-ons"
msgstr "Mitä sinun tarvitsee tietää taaksepäin yhteensopimattomista NVDA-versioista ja lisäosista"

#. type: Plain text
msgid ""
"Once a year, NV Access may publish a backwards incompatible NVDA release. "
"Here, \"backwards incompatible\" refers to NVDA releases that make add-ons "
"incompatible due to API changes. When this happens, authors must test their "
"add-ons for compatibility and release updates accordingly."
msgstr ""
"NV Access saattaa julkaista Kerran vuodessa taaksepäin yhteensopimattoman "
"NVDA-version. \"Taaksepäin yhteensopimaton\" viittaa tässä NVDA-versioihin, "
"jotka tekevät lisäosista yhteensopimattomia rajapintamuutosten vuoksi. Kun "
"näin tapahtuu, tekijöiden on testattava lisäosiensa yhteensopivuus ja "
"julkaistava vastaavasti päivitykset."

#. type: Plain text
msgid "As mentioned in the NVDA's user guide:"
msgstr "Kuten NVDA:n käyttöoppaassa mainitaan:"

#. type: Plain text
msgid ""
"The Incompatible Add-ons Manager, which can be accessed via the \"view "
"incompatible add-ons\" button in the Add-on manager, allows you to inspect "
"any incompatible add-ons, and the reason they are considered incompatible. "
"Add-ons are considered incompatible when they have not been updated to work "
"with significant changes to NVDA, or when they rely on a feature not "
"available in the version of NVDA you are using. The Incompatible add-ons "
"manager has a short message to explain its purpose as well as the version of "
"NVDA."
msgstr ""
"Yhteensopimattomien lisäosien hallinnan, johon pääsee painamalla Lisäosien "
"hallinnassa \"Näytä yhteensopimattomat lisäosat\" -painiketta, avulla voit "
"tarkastella kaikkia yhteensopimattomia lisäosia sekä niiden "
"yhteensopimattomuuden syytä. Lisäosat ovat yhteensopimattomia, kun niitä ei "
"ole päivitetty toimimaan merkittävien NVDA:han tehtyjen muutosten kanssa tai "
"kun ne hyödyntävät ominaisuutta, jota ei ole käytettävissä NVDA:n käytössä "
"olevassa versiossa. Yhteensopimattomien lisäosien hallinnassa näytetään "
"lyhyt ilmoitus, joka kertoo sen tarkoituksen sekä NVDA:n version."

#. type: Plain text
msgid ""
"The Incompatible add-ons manager also has an \"About add-on...\" button. "
"This dialog will provide you with the full details of the add-on, which is "
"helpful when contacting the add-on author."
msgstr ""
"Valintaikkunassa on myös \"Tietoja lisäosasta...\" -painike. Sitä painamalla "
"saat näkyviin kaikki lisäosan tiedot, joista on apua otettaessa yhteyttä "
"lisäosan tekijään."

#. type: Title ###
#, no-wrap
msgid "Key changes in backwards incompatible NVDA releases"
msgstr "Keskeiset muutokset taaksepäin yhteensopimattomissa NVDA-versioissa"

#. type: Plain text
msgid ""
"Below is a list of backwards incompatible NVDA releases and key changes in "
"each:"
msgstr ""
"Alla on luettelo taaksepäin yhteensopimattomista NVDA-versioista sekä niiden "
"keskeisistä muutoksista:"

#. type: Bullet: '* '
msgid "2019.3: Python 2 to 3, speech refactor"
msgstr "2019.3: Python 2:sta 3:een, puhejärjestelmäkoodin uudelleenkirjoitus"

#. type: Bullet: '* '
msgid "2021.1: code refactoring, wxPython 4.1.1"
msgstr "2021.1: koodin korjaus, wxPython 4.1.1"

#. type: Plain text
msgid ""
"Note to add-on authors: when backwards incompatible NVDA release enters beta "
"testing phase (with the release of beta 1), be sure to test your add-ons and "
"report your next steps, including update plans and release announcements to "
"NVDA community through various channels (add-ons list, users list, Facebook, "
"Twitter, etc.). Also, send a pull request against nvaccess/addonFiles repo "
"on GitHub so that updated add-ons can be posted on this website."
msgstr ""
"Huomautus lisäosien tekijöille: Kun taaksepäin yhteensopimaton NVDA-versio "
"siirtyy beetatestivaiheeseen (beeta 1:n julkaisun myötä), muistakaa testata "
"lisäosanne ja ilmoittakaa seuraavat vaiheenne, mukaan lukien "
"päivityssuunnitelmat sekä julkaisutiedotteet NVDA-yhteisölle eri kanavissa "
"(lisäosien postituslista, käyttäjien postituslista, Facebook, Twitter jne.). "
"Lähettäkää myös GitHubissa vetopyyntö nvaccess/addonFiles-repoa vastaan, "
"jotta päivitetyt lisäosat voidaan julkaista tällä sivustolla."

#. type: Plain text
msgid ""
"Unless otherwise specified, add-ons listed on this page are checked against "
"latest backwards incompatible NVDA release (2021.1)."
msgstr ""
"Ellei toisin mainita, tällä sivulla luetellut lisäosat testataan uusimmalla "
"taaksepäin yhteensopimattomalla NVDA-versiolla (2021.1)."

#. type: Title ##
#, no-wrap
msgid "Status of add-ons compatible with NVDA 2021.1 and availability on the website"
msgstr "NVDA 2021.1:n kanssa yhteensopivien lisäosien tila ja saatavuus verkkosivustolla"

#. type: Plain text
msgid ""
"Below is a list of add-ons hosted on this website (in the stable section). "
"The list also includes contact information for add-on author(s)."
msgstr ""
"Alla on luettelo tällä verkkosivulla olevista lisäosista (vakaa-osiossa). "
"Luettelo sisältää myös tekijöiden yhteystiedot."

#. type: Title ###
#, no-wrap
msgid "Notes:"
msgstr "Huomautukset:"

#. type: Bullet: '* '
msgid "Add-on compatibility status subject to change without notice."
msgstr "Lisäosien yhteensopivuustila voi muuttua ilman erillistä ilmoitusta."

#. type: Bullet: '* '
msgid ""
"Please do NOT change the manifest.ini file of incompatible add-ons if you "
"don't know what you're doing since this may result in unpredictable behavior "
"such as making NVDA less stable."
msgstr ""
"ÄLÄ muuta yhteensopimattomien lisäosien manifest.ini-tiedostoa, jos et tiedä "
"mitä olet tekemässä, koska se voi johtaa arvaamattomaan toimintaan, kuten "
"NVDA:n epävakauteen."

#. type: Bullet: '* '
msgid ""
"When maintainers have asked to post an add-on on the website via a pull "
"request, this will be reflected as \"coming soon to the website\"."
msgstr ""
"Kun ylläpitäjät ovat vetopyynnön kautta pyytäneet lähettämään lisäosan "
"verkkosivustolle, tämä näkyy muodossa \"tulossa pian verkkosivustolle\"."

#. type: Title ###
#, no-wrap
msgid "Access8Math"
msgstr "Access8Math"

#. type: Bullet: '* '
msgid "Compatible: Yes (updated on the website)"
msgstr "Yhteensopiva: Kyllä (päivitetty verkkosivustolla)"

#. type: Bullet: '* '
msgid "Contact: Tseng Woody <tsengwoody.tw@gmail.com>"
msgstr "Yhteystiedot: Tseng Woody <tsengwoody.tw@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Add-on Updater"
msgstr "Lisäosien päivittäjä"

#. type: Bullet: '* '
msgid "Contact: Joseph Lee <joseph.lee22590@gmail.com>"
msgstr "Yhteystiedot: Joseph Lee <joseph.lee22590@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Add-ons Documentation"
msgstr "Lisäosien ohjeet"

#. type: Bullet: '* '
msgid ""
"Contact: Rui Fontes <rui.fontes@tiflotecnia.com>, Zougane, Rémy and Abdel"
msgstr ""
"Yhteystiedot: Rui Fontes <rui.fontes@tiflotecnia.com>, Zougane, Rémy and "
"Abdel"

#. type: Title ###
#, no-wrap
msgid "Addon to count elements of selected text"
msgstr "Sanalaskuri"

#. type: Bullet: '* '
msgid "Contact: Rui Fontes <rui.fontes@tiflotecnia.com>"
msgstr "Yhteystiedot: Rui Fontes <rui.fontes@tiflotecnia.com>"

#. type: Title ###
#, no-wrap
msgid "Audio Themes"
msgstr "Ääniteemat"

#. type: Bullet: '* '
msgid "Compatible: No"
msgstr "Yhteensopiva: Ei"

#. type: Bullet: '* '
msgid "Contact: Musharraf Omer <ibnomer2011@hotmail.com>"
msgstr "Yhteystiedot: Musharraf Omer <ibnomer2011@hotmail.com>"

#. type: Title ###
#, no-wrap
msgid "AudioChart"
msgstr "Äänikaavio"

#. type: Bullet: '* '
msgid "Contact: Tony Malykh <anton.malykh@gmail.com>"
msgstr "Yhteystiedot: Tony Malykh <anton.malykh@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Beep keyboard"
msgstr "Näppäimistön äänimerkki"

#. type: Bullet: '* '
msgid "Contact: David CM <dhf360@gmail.com>"
msgstr "Yhteystiedot: David CM <dhf360@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Bluetooth Audio"
msgstr "Bluetooth Audio"

#. type: Title ###
#, no-wrap
msgid "Braille Extender"
msgstr "Braille Extender"

#. type: Bullet: '* '
msgid "Contact: André-Abush Clause <dev@andreabc.net>"
msgstr "Yhteystiedot: André-Abush Clause <dev@andreabc.net>"

#. type: Title ###
#, no-wrap
msgid "BrowserNav"
msgstr "BrowserNav"

#. type: Title ###
#, no-wrap
msgid "Calibre"
msgstr "Calibre"

#. type: Bullet: '* '
msgid "Contact: Javi Dominguez <fjavids@gmail.com>"
msgstr "Yhteystiedot: Javi Dominguez <fjavids@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Character Information"
msgstr "Merkin tiedot"

#. type: Bullet: '* '
msgid "Contact: Cyrille Bougot <cyrille.bougot2@laposte.net>"
msgstr "Yhteystiedot: Cyrille Bougot <cyrille.bougot2@laposte.net>"

#. type: Title ###
#, no-wrap
msgid "Check Input Gestures"
msgstr "Tarkista syötekomennot"

#. type: Bullet: '* '
msgid "Contact: Oleksandr Gryshchenko <grisov.nvaccess@mailnull.com>"
msgstr "Yhteystiedot: Oleksandr Gryshchenko <grisov.nvaccess@mailnull.com>"

#. type: Title ###
#, no-wrap
msgid "Classic Selection"
msgstr "Perinteinen valitseminen"

#. type: Bullet: '* '
msgid "Contact: Tyler Spivey <tspivey@pcdesk.net>"
msgstr "Yhteystiedot: Tyler Spivey <tspivey@pcdesk.net>"

#. type: Title ###
#, no-wrap
msgid "Clip Contents Designer"
msgstr "Leikepöydän sisällön käsittelijä"

#. type: Bullet: '* '
msgid "Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>"
msgstr "Yhteystiedot: Noelia Ruiz Martínez <nrm1977@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Clipspeak"
msgstr "Puhu leikepöytä"

#. type: Bullet: '* '
msgid "Contact: Damien Sykes-Lindley <damien@dcpendleton.plus.com>"
msgstr "Yhteystiedot: Damien Sykes-Lindley <damien@dcpendleton.plus.com>"

#. type: Title ###
#, no-wrap
msgid "Clock and calendar Add-on for NVDA"
msgstr "Kello ja kalenteri"

#. type: Bullet: '* '
msgid ""
"Contact: Hrvoje Katić <hrvojekatic@gmail.com>, Abdel <abdelkrim."
"bensaid@gmail.com>"
msgstr ""
"Yhteystiedot: Hrvoje Katić <hrvojekatic@gmail.com>, Abdel <abdelkrim."
"bensaid@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Console Toolkit"
msgstr "Konsolin työkalupakki"

#. type: Title ###
#, no-wrap
msgid "Control Usage Assistant"
msgstr "Säätimen käyttöapu"

#. type: Title ###
#, no-wrap
msgid "Crash Hero"
msgstr "Crash Hero"

#. type: Bullet: '* '
msgid "Contact: Derek Riemer <driemer.riemer@gmail.com>"
msgstr "Yhteystiedot: Derek Riemer <driemer.riemer@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Day of the week"
msgstr "Viikonpäivä"

#. type: Bullet: '* '
msgid ""
"Contact: Abdel <abdelkrim.bensaid@gmail.com>, Noelia Ruiz Martínez "
"<nrm1977@gmail.com>"
msgstr ""
"Yhteystiedot: Abdel <abdelkrim.bensaid@gmail.com>, Noelia Ruiz Martínez "
"<nrm1977@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Debug Helper/Dev Helper"
msgstr "Virheenkorjausapuri/Kehittäjän apuri"

#. type: Bullet: '* '
msgid "Note: renamed to Dev Helper in 2021"
msgstr "Huom: uudelleennimetty Kehittäjän apuriksi vuonna 2021"

#. type: Bullet: '* '
msgid "Contact: Luke Davis <newanswertech@gmail.com>"
msgstr "Yhteystiedot: Luke Davis <newanswertech@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Developer Toolkit"
msgstr "Kehittäjän työkalupakki"

#. type: Bullet: '* '
msgid "Compatible: Yes (not updated on the website)"
msgstr "Yhteensopiva: Kyllä (ei päivitetty verkkosivustolla)"

#. type: Bullet: '* '
msgid "Contact: Andy Borka <ajborka@gmail.com>"
msgstr "Yhteystiedot: Andy Borka <ajborka@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Direct Link"
msgstr "Suora linkki"

#. type: Bullet: '* '
msgid "Contact: Fawaz Abdul rahman <fawaz.ar94@gmail.com>"
msgstr "Yhteystiedot: Fawaz Abdul rahman <fawaz.ar94@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Dropbox"
msgstr "Dropbox"

#. type: Bullet: '* '
msgid ""
"Contact: Patrick Zajda <patrick@zajda.fr>, Filaos and other contributors"
msgstr ""
"Yhteystiedot: Patrick Zajda <patrick@zajda.fr>, Filaos and other contributors"

#. type: Title ###
#, no-wrap
msgid "Dual Voice"
msgstr "Dual Voice"

#. type: Bullet: '* '
msgid "Compatible: Yes (external link"
msgstr "Yhteensopiva: Kyllä (ulkoinen linkki)"

#. type: Bullet: '* '
msgid "Contact: Seyed Mahmood Taghavi-Shahri"
msgstr "Yhteystiedot: Seyed Mahmood Taghavi-Shahri"

#. type: Title ###
#, no-wrap
msgid "Easy Table Navigator"
msgstr "Helppo taulukossa liikkuminen"

#. type: Title ###
#, no-wrap
msgid "Emoticons"
msgstr "Hymiöt"

#. type: Bullet: '* '
msgid ""
"Contact: Chris Leo <llajta2012@gmail.com>, Noelia Ruiz Martínez "
"<nrm1977@gmail.com>, Mesar Hameed <mesar.hameed@gmail.com>, Francisco Javier "
"Estrada Martínez <Fjestrad@hotmail.com>"
msgstr ""
"Yhteystiedot: Chris Leo <llajta2012@gmail.com>, Noelia Ruiz Martínez "
"<nrm1977@gmail.com>, Mesar Hameed <mesar.hameed@gmail.com>, Francisco Javier "
"Estrada Martínez <Fjestrad@hotmail.com>"

#. type: Title ###
#, no-wrap
msgid "eMule"
msgstr "eMule"

#. type: Bullet: '* '
msgid ""
"Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>, Chris <llajta2012@gmail."
"com>, Alberto <a.buffolino@gmail.com>"
msgstr ""
"Yhteystiedot: Noelia Ruiz Martínez <nrm1977@gmail.com>, Chris "
"<llajta2012@gmail.com>, Alberto <a.buffolino@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Enhanced Aria"
msgstr "Laajennettu Aria"

#. type: Bullet: '* '
msgid "Contact: José Manuel Delicado <jm.delicado@nvda.es>"
msgstr "Yhteystiedot: José Manuel Delicado <jm.delicado@nvda.es>"

#. type: Title ###
#, no-wrap
msgid "Enhanced Touch Gestures"
msgstr "Laajennetut kosketuseleet"

#. type: Title ###
#, no-wrap
msgid "Event Tracker"
msgstr "Event Tracker"

#. type: Title ###
#, no-wrap
msgid "extendedWinamp"
msgstr "Laajennettu Winamp"

#. type: Bullet: '* '
msgid "Contact: Hrvoje Katić <hrvojekatic@gmail.com>, NVDA Add-ons Team"
msgstr "Yhteystiedot: Hrvoje Katić <hrvojekatic@gmail.com>, NVDA Add-ons Team"

#. type: Title ###
#, no-wrap
msgid "Focus Highlight"
msgstr "Kohdistuksen korostus"

#. type: Bullet: '* '
msgid "Contact: Takuya Nishimoto <nishimotz@gmail.com>"
msgstr "Yhteystiedot: Takuya Nishimoto <nishimotz@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Golden Cursor"
msgstr "Golden Cursor"

#. type: Bullet: '* '
msgid "Note: no longer maintained"
msgstr "Huom: ei enää ylläpidetä"

#. type: Bullet: '* '
msgid ""
"Contact: Salah Atair <atair1978@gmail.com>, Wafeeq Taher, Joseph Lee <joseph."
"lee22590@gmail.com>, Abdel <abdelkrim.bensaid@gmail.com>"
msgstr ""
"Yhteystiedot: Salah Atair <atair1978@gmail.com>, Wafeeq Taher, Joseph Lee "
"<joseph.lee22590@gmail.com>, Abdel <abdelkrim.bensaid@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "GoldWave"
msgstr "GoldWave"

#. type: Bullet: '* '
msgid ""
"Contact: Joseph Lee <joseph.lee22590@gmail.com>, David Parduhn <xkill85@gmx."
"net>, Mesar Hameed <mhameed@src.gnome.org>"
msgstr ""
"Yhteystiedot: Joseph Lee <joseph.lee22590@gmail.com>, David Parduhn "
"<xkill85@gmx.net>, Mesar Hameed <mhameed@src.gnome.org>"

#. type: Title ###
#, no-wrap
msgid "IndentNav"
msgstr "IndentNav"

#. type: Title ###
#, no-wrap
msgid "Input Lock"
msgstr "Syöttölukko"

#. type: Title ###
#, no-wrap
msgid "instantTranslate"
msgstr "Pikakääntäjä"

#. type: Bullet: '* '
msgid ""
"Contact: Alexy Sadovoy aka Lex <lex@progger.su>, ruslan <ru2020slan@yandex."
"ru>, beqa <beqaprogger@gmail.com>, Mesar Hameed <mhameed@src.gnome.org>, "
"Alberto Buffolino <a.buffolino@gmail.com>"
msgstr ""
"Yhteystiedot: Alexy Sadovoy aka Lex <lex@progger.su>, ruslan "
"<ru2020slan@yandex.ru>, beqa <beqaprogger@gmail.com>, Mesar Hameed "
"<mhameed@src.gnome.org>, Alberto Buffolino <a.buffolino@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Kill NVDA"
msgstr "Tapa NVDA"

#. type: Title ###
#, no-wrap
msgid "Lambda Add-On for NVDA"
msgstr "Lambda Add-On for NVDA"

#. type: Bullet: '* '
msgid "Contact: Alberto Zanella, Ivan Novegil"
msgstr "Yhteystiedot: Alberto Zanella, Ivan Novegil"

#. type: Title ###
#, no-wrap
msgid "Mozilla Apps Enhancements"
msgstr "Mozilla-sovellusten laajennukset"

#. type: Title ###
#, no-wrap
msgid "mp3DirectCut"
msgstr "mp3DirectCut"

#. type: Bullet: '* '
msgid "Contact: Abdel, Rémy Ruiz, Abdellah Zineddine, Jean-François Colas"
msgstr ""
"Yhteystiedot: Abdel, Rémy Ruiz, Abdellah Zineddine, Jean-François Colas"

#. type: Title ###
#, no-wrap
msgid "Newfon"
msgstr "Newfon"

#. type: Bullet: '* '
msgid "Contact: Sergey Shishmintzev"
msgstr "Yhteystiedot: Sergey Shishmintzev"

#. type: Title ###
#, no-wrap
msgid "NoBeepsSpeechMode"
msgstr "Ei äänimerkit-puhetilaa"

#. type: Bullet: '* '
msgid ""
"Note: new releases are compatible with 2021.1 only, older versions should be "
"used for older NVDA releases"
msgstr ""
"Huom: uudet versiot ovat yhteensopivia vain NVDA 2021.1:n kanssa, vanhemmat "
"versiot ovat NVDA:n vanhempille versioille"

#. type: Bullet: '* '
msgid "Contact: Alberto Buffolino <a.buffolino@gmail.com>"
msgstr "Yhteystiedot: Alberto Buffolino <a.buffolino@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Notepad++"
msgstr "Notepad++"

#. type: Bullet: '* '
msgid "Compatible: No (Up to 2019.3)"
msgstr "Yhteensopiva: Ei (2019.3:een saakka)"

#. type: Title ###
#, no-wrap
msgid "Numpad Nav Mode"
msgstr "Numpad Nav Mode"

#. type: Bullet: '* '
msgid ""
"Contact: Luke Davis (Open Source Systems, Ltd.) <newanswertech@gmail.com>"
msgstr ""
"Yhteystiedot: Luke Davis (Open Source Systems, Ltd.) <newanswertech@gmail."
"com>"

#. type: Title ###
#, no-wrap
msgid "NVDA Remote Support"
msgstr "NVDA:n etäkäyttötuki"

#. type: Bullet: '* '
msgid ""
"Contact: Tyler Spivey <tspivey@pcdesk.net>, Christopher Toth <q@q-continuum."
"net>"
msgstr ""
"Yhteystiedot: Tyler Spivey <tspivey@pcdesk.net>, Christopher Toth <q@q-"
"continuum.net>"

#. type: Title ###
#, no-wrap
msgid "NVDA Unmute"
msgstr "Poista mykistys"

#. type: Title ###
#, no-wrap
msgid "NVDAUpdate Channel Selector"
msgstr "NVDA:n päivityskanavan valitsin"

#. type: Bullet: '* '
msgid "Contact: Jose Manuel Delicado <jm.delicado@nvda.es>"
msgstr "Yhteystiedot: Jose Manuel Delicado <jm.delicado@nvda.es>"

#. type: Title ###
#, no-wrap
msgid "NV Speech Player. "
msgstr "NV Speech Player. "

#. type: Bullet: '* '
msgid "Contact: NV Access"
msgstr "Yhteystiedot: NV Access"

#. type: Title ###
#, no-wrap
msgid "Object Location Tones"
msgstr "Objektien sijaintiäänet"

#. type: Bullet: '* '
msgid "Compatible: No (UP to 2019.3)"
msgstr "Yhteensopiva: Ei (2019.3:een saakka)"

#. type: Title ###
#, no-wrap
msgid "ObjPad"
msgstr "ObjPad"

#. type: Title ###
#, no-wrap
msgid "OCR"
msgstr "OCR"

#. type: Bullet: '* '
msgid "Contact: Łukasz Golonka <lukasz.golonka@mailbox.org>"
msgstr "Yhteystiedot: Łukasz Golonka <lukasz.golonka@mailbox.org>"

#. type: Title ###
#, no-wrap
msgid "Outlook Extended"
msgstr "Outlook Extended"

#. type: Bullet: '* '
msgid ""
"Contact: Cyrille Bougot <cyrille.bougot2@laposte.net>, Ralf Kefferpuetz "
"<ralf.kefferpuetz@elra-consulting.de>"
msgstr ""
"Yhteystiedot: Cyrille Bougot <cyrille.bougot2@laposte.net>, Ralf Kefferpuetz "
"<ralf.kefferpuetz@elra-consulting.de>"

#. type: Title ###
#, no-wrap
msgid "PC Keyboard Braille Input for NVDA"
msgstr "Pistekirjoituksen syöttö PC:n näppäimistöllä"

#. type: Title ###
#, no-wrap
msgid "Phonetic Punctuation"
msgstr "Foneettiset välimerkit"

#. type: Title ###
#, no-wrap
msgid "placeMarkers"
msgstr "Paikkamerkit"

#. type: Bullet: '* '
msgid ""
"Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>, Chris <llajta2012@gmail."
"com>"
msgstr ""
"Yhteystiedot: Noelia Ruiz Martínez <nrm1977@gmail.com>, Chris "
"<llajta2012@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Proxy support for NVDA"
msgstr "Välityspalvelimen tuki"

#. type: Title ###
#, no-wrap
msgid "Quick Dictionary"
msgstr "Pikasanakirja"

#. type: Title ###
#, no-wrap
msgid "Read Feeds"
msgstr "Lue syötteet"

#. type: Bullet: '* '
msgid "Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>, Mesar Hameed"
msgstr "Yhteystiedot: Noelia Ruiz Martínez <nrm1977@gmail.com>, Mesar Hameed"

#. type: Title ###
#, no-wrap
msgid "Report Passwords"
msgstr "Ilmoita salasanat"

#. type: Title ###
#, no-wrap
msgid "Report Symbols"
msgstr "Puhu symbolit"

#. type: Title ###
#, no-wrap
msgid "Resource Monitor"
msgstr "Resurssienvalvonta"

#. type: Bullet: '* '
msgid ""
"Contact: Alex Hall <mehgcap@gmail.com>, Joseph Lee <joseph.lee22590@gmail."
"com>, beqa gozalishvili <beqaprogger@gmail.com>, Tuukka Ojala <tuukka."
"ojala@gmail.com>, Ethin Probst <harlydavidsen@gmail.com> and other NVDA "
"contributors"
msgstr ""
"Yhteystiedot: Alex Hall <mehgcap@gmail.com>, Joseph Lee <joseph."
"lee22590@gmail.com>, beqa gozalishvili <beqaprogger@gmail.com>, Tuukka Ojala "
"<tuukka.ojala@gmail.com>, Ethin Probst <harlydavidsen@gmail.com> and other "
"NVDA contributors"

#. type: Title ###
#, no-wrap
msgid "Review Cursor Copier"
msgstr "Tarkastelukohdistimen kopioija"

#. type: Bullet: '* '
msgid "Contact: Tuukka Ojala <tuukka.ojala@gmail.com>"
msgstr "Yhteystiedot: Tuukka Ojala <tuukka.ojala@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "sayCurrentKeyboardLanguage"
msgstr "Sano nykyisen näppäimistön kieli"

#. type: Bullet: '* '
msgid ""
"Contact: Abdel <abdelkrim.bensaid@gmail.com>, Noelia <nrm1977@gmail.com>"
msgstr ""
"Yhteystiedot: Abdel <abdelkrim.bensaid@gmail.com>, Noelia <nrm1977@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "SentenceNav"
msgstr "SentenceNav"

#. type: Title ###
#, no-wrap
msgid "Speak Passwords"
msgstr "Puhu salasanat"

#. type: Title ###
#, no-wrap
msgid "Speech History"
msgstr "Puhehistoria"

#. type: Bullet: '* '
msgid "Contact: Tyler Spivey <tspivey@pcdesk.net>, James Scholes"
msgstr "Yhteystiedot: Tyler Spivey <tspivey@pcdesk.net>, James Scholes"

#. type: Title ###
#, no-wrap
msgid "Station Playlist"
msgstr "Station Playlist"

#. type: Bullet: '* '
msgid "Contact: Geoff Shang, Joseph Lee and other contributors"
msgstr "Yhteystiedot: Geoff Shang, Joseph Lee and other contributors"

#. type: Title ###
#, no-wrap
msgid "Switch synth"
msgstr "Vaihda syntetisaattoria"

#. type: Title ###
#, no-wrap
msgid "Synth ring settings selector"
msgstr "Syntetisaattorin asetusrenkaan asetusten valitsin"

#. type: Title ###
#, no-wrap
msgid "systrayList"
msgstr "Ilmoitusalueen kuvakkeet"

#. type: Bullet: '* '
msgid ""
"Contact: Rui Fontes <rui.fontes@tiflotecnia.com>, Rui Batista "
"<ruiandrebatista@gmail.com>, Joseph Lee <joseph.lee22590@gmail.com>, NVDA "
"Community Contributors"
msgstr ""
"Yhteystiedot: Rui Fontes <rui.fontes@tiflotecnia.com>, Rui Batista "
"<ruiandrebatista@gmail.com>, Joseph Lee <joseph.lee22590@gmail.com>, NVDA "
"Community Contributors"

#. type: Title ###
#, no-wrap
msgid "TeamTalk Classic"
msgstr "TeamTalk Classic"

#. type: Bullet: '* '
msgid "Contact: Doug Lee with initial work by Tyler Spivey and others"
msgstr ""
"Yhteystiedot: Doug Lee (alustavan kehitystyön tehnyt Tyler Spivey ja muut)"

#. type: Title ###
#, no-wrap
msgid "Text Information"
msgstr "Tekstin tiedot"

#. type: Bullet: '* '
msgid "Contact: Carter Temm <crtbraille@gmail.com>"
msgstr "Yhteystiedot: Carter Temm <crtbraille@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "TextNav"
msgstr "TextNav"

#. type: Bullet: '* '
msgid "Compatible: Yes (coming soon to the website)"
msgstr "Yhteensopiva: Kyllä (tulossa pian verkkosivustolle)"

#. type: Title ###
#, no-wrap
msgid "Time Zoner"
msgstr "Aikavyöhyke"

#. type: Bullet: '* '
msgid "Contact: Munawar Bijani"
msgstr "Yhteystiedot: Munawar Bijani"

#. type: Title ###
#, no-wrap
msgid "Tip of the Day"
msgstr "Päivän vinkki"

#. type: Title ###
#, no-wrap
msgid "Tony's enhancements"
msgstr "Tonyn laajennukset"

#. type: Title ###
#, no-wrap
msgid "ToolbarsExplorer"
msgstr "Työkalupalkkien selain"

#. type: Title ###
#, no-wrap
msgid "Tone Master"
msgstr "Tone Master"

#. type: Bullet: '* '
msgid "Contact: Hrvoje Katić <hrvojekatic@gmail.com>"
msgstr "Yhteystiedot: Hrvoje Katić <hrvojekatic@gmail.com>"

#. type: Title ###
#, no-wrap
msgid "Training Keyboard commands"
msgstr "Näppäinkomentojen harjoittelu"

#. type: Bullet: '* '
msgid "Contact: Ibrahim Hamadeh <ibra.hamadeh@hotmail.com>"
msgstr "Yhteystiedot: Ibrahim Hamadeh <ibra.hamadeh@hotmail.com>"

#. type: Title ###
#, no-wrap
msgid "UnicodeBrailleInput"
msgstr "Unicode-pistekirjoituksen syöttö"

#. type: Bullet: '* '
msgid ""
"Contact: Mesar Hameed <mhameed@src.gnome.org>, Patrick ZAJDA <patrick@zajda."
"fr>, Leonard de Ruijter (Babbage B.V.) <leonard@babbage.com>"
msgstr ""
"Yhteystiedot: Mesar Hameed <mhameed@src.gnome.org>, Patrick ZAJDA "
"<patrick@zajda.fr>, Leonard de Ruijter (Babbage B.V.) <leonard@babbage.com>"

#. type: Title ###
#, no-wrap
msgid "Virtual Review"
msgstr "Virtuaalitarkastelu"

#. type: Bullet: '* '
msgid "Contact: Rui Batista <ruiandrebatista@gmail.com> and NVDA Addon Team"
msgstr ""
"Yhteystiedot: Rui Batista <ruiandrebatista@gmail.com> ja NVDA:n lisäosatiimi"

#. type: Title ###
#, no-wrap
msgid "VLC Media Player"
msgstr "VLC-mediasoitin"

#. type: Title ###
#, no-wrap
msgid "Weather Plus"
msgstr "Weather Plus"

#. type: Bullet: '* '
msgid "Contact: Adriano Barbieri <adrianobarb@yahoo.it>"
msgstr "Yhteystiedot: Adriano Barbieri <adrianobarb@yahoo.it>"

#. type: Title ###
#, no-wrap
msgid "Windows App Essentials"
msgstr "Windows App Essentials"

#. type: Bullet: '* '
msgid ""
"Contact: Joseph Lee <joseph.lee22590@gmail.com>, Derek Riemer <driemer."
"riemer@gmail.com> and others"
msgstr ""
"Yhteystiedot: Joseph Lee <joseph.lee22590@gmail.com>, Derek Riemer <driemer."
"riemer@gmail.com> ja muut"

#. type: Title ###
#, no-wrap
msgid "Windows Magnifier"
msgstr "Windowsin suurennuslasi"

#. type: Title ###
#, no-wrap
msgid "Win Wizard"
msgstr "Win Wizard"

#. type: Bullet: '* '
msgid ""
"Contact: Oriol Gomez <ogomez.s92@gmail.com>, Łukasz Golonka <lukasz."
"golonka@mailbox.org>"
msgstr ""
"Yhteystiedot: Oriol Gomez <ogomez.s92@gmail.com>, Łukasz Golonka <lukasz."
"golonka@mailbox.org>"

#. type: Title ###
#, no-wrap
msgid "WordNav"
msgstr "WordNav"

#. type: Title ###
#, no-wrap
msgid "Zoom Accessibility Enhancements"
msgstr "Zoomin saavutettavuuslaajennukset"

#. type: Bullet: '* '
msgid ""
"Contact: Mohammad Suliman <mohmad.s93@gmail.com>, Eilana Benish <benish."
"ilana@gmail.com>"
msgstr ""
"Yhteystiedot: Mohammad Suliman <mohmad.s93@gmail.com>, Eilana Benish <benish."
"ilana@gmail.com>"

#. type: Plain text
#, no-wrap
msgid "[[!tag announcements]]\n"
msgstr "[[!tag announcements]]\n"
