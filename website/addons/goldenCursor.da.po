# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-07-16 06:18+0000\n"
"PO-Revision-Date: 2021-08-01 10:30+0200\n"
"Last-Translator: Nicolai Svendsen <chojiro1990@gmail.com>\n"
"Language-Team: \n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Golden Cursor\"]]\n"
msgstr "[[!meta title=\"Golden Cursor\"]]\n"

#. type: Bullet: '* '
msgid "Author: salah atair, Joseph Lee"
msgstr "Forfatter: Salah Atair, Joseph Lee"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Download [stabil version][1]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2019.3 and beyond"
msgstr "NVDA-kompatibilitet: 2019.3 og derefter"

#. type: Plain text
msgid ""
"This add-on allows you to move the mouse using a keyboard and save mouse "
"positions for applications."
msgstr ""
"Denne tilføjelse giver dig mulighed for at flytte musen ved hjælp af et "
"tastatur og gemme musepositioner til applikationer."

#. type: Title ##
#, no-wrap
msgid "Key commands"
msgstr "Tastaturkommandoer:"

#. type: Bullet: '* '
msgid "Control+NVDA+L: view saved mouse positions for an application if any."
msgstr ""
"Ctrl+NVDA+L: Få vist gemte musepositioner for et program, hvis der er nogen."

#. type: Bullet: '* '
msgid ""
"Shift+NVDA+l: save a tag or a label for the current mouse position in the "
"currently focused application."
msgstr ""
"Skift+NVDA+L: Gem et tag eller en etiket for den aktuelle museposition i den "
"aktuelt fokuserede applikation."

#. type: Bullet: '* '
msgid "Windows+NVDA+C: change mouse movement unit."
msgstr "Windows+NVDA+C: Skift musens bevægelsesenhed."

#. type: Bullet: '* '
msgid "Windows+NVDA+R: toggle mouse restriction."
msgstr "Windows+NVDA+R: Skift tilstand for musebegrænsning."

#. type: Bullet: '* '
msgid "Windows+NVDA+S: toggle reporting of mouse position in pixels."
msgstr "Windows+NVDA+S: Skifte rapportering af musens position i pixels."

#. type: Bullet: '* '
msgid "Windows+NVDA+J: move mouse to a specific x and y position."
msgstr "Windows+NVDA+J: Flyt musen til bestemte X og Y koordinater."

#. type: Bullet: '* '
msgid "Windows+NVDA+P: report mouse position."
msgstr "Windows+NVDA+P: Rapportér musens aktuelle position."

#. type: Bullet: '* '
msgid "Windows+NVDA+M: sswitch mouse arrows on or off."
msgstr "Windows+NVDA+M: Slå musepile til og fra."

#. type: Bullet: '* '
msgid ""
"Windows+NVDA+arrow keys (or just arrow keys if mouse arrows is on): move "
"mouse."
msgstr ""
"Windows+NVDA+Piletaster (eller blot piletasterne alene, hvis musepile er "
"slået til): Flyt mus."

#. type: Plain text
msgid ""
"Note: these gestures can be reassigned via NVDA's Input Gestures dialog "
"under Golden Cursor category."
msgstr ""
"Bemærk: Disse kommandoer kan ændres ved brug af NVDA-dialogen "
"\"Inputbevægelser\" under kategorien \"Golden Cursor\"."

#. type: Title ##
#, no-wrap
msgid "Notes"
msgstr "Bemærkninger"

#. type: Bullet: '* '
msgid ""
"When sharing positions (tags), each party should use same display resolution."
msgstr ""
"Når der deles positioner (tags), skal hver part anvende samme skærmopløsning."

#. type: Bullet: '* '
msgid ""
"For maximum compatibility, you should maximize windows by pressing Windows"
"+Up arrow."
msgstr ""
"For maksimal kompatibilitet bør du maksimere vinduer ved at trykke på Windows"
"+Pil op."

#. type: Bullet: '* '
msgid "When sharing positions, existing position labels should be renamed."
msgstr "Når man deler positioner, skal eksisterende positionetiketter omdøbes."

#. type: Bullet: '* '
msgid "Version 1.x and 2.x mouse position formats are incompatible."
msgstr "Version 1.x og 2.x muspositionsformater er inkompatible."

#. type: Bullet: '* '
msgid ""
"To perform functions that require use of arrow keys, turn off mouse arrows "
"first."
msgstr ""
"For at udføre funktioner, der kræver brug af piletasterne, skal du først slå "
"musepilene fra."

#. type: Bullet: '* '
msgid ""
"When deleting saved positions, if there are no saved positions left, "
"positions for the application will be cleared."
msgstr ""
"Når der slettes gemte positioner, vil alle positioner for den aktuelle "
"applikation ryddes, hvis der ikke længere eksistere gemte positioner."

#. type: Title ##
#, no-wrap
msgid "Version 5.0"
msgstr "Version 5.0"

#. type: Bullet: '* '
msgid "Modernized add-on source code to make it compatible with NVDA 2021.1."
msgstr ""
"Moderniserede tilføjelsens kildekode for at gøre den kompatibel med NVDA "
"2021.1."

#. type: Bullet: '* '
msgid "Resolved many coding style issues and potential bugs with Flake8."
msgstr "Løst mange problemer med kodningstil og potentielle fejl med Flake8."

#. type: Title ##
#, no-wrap
msgid "Version 4.0"
msgstr "Version 4.0"

#. type: Bullet: '* '
msgid "Requires NVDA 2019.3 or later."
msgstr "Kræver NVDA 2019.3 eller nyere."

#. type: Bullet: '* '
msgid ""
"Golden Cursor settings dialog has been replaced by Golden Cursor settings "
"panel."
msgstr ""
"Golden Cursor-indstillingsdialogen er blevet erstattet af Golden Cursor-"
"indstillingspanelet."

#. type: Title ##
#, no-wrap
msgid "Version 3.3"
msgstr "Version 3.3"

#. type: Bullet: '* '
msgid "Internal changes to support future NVDA releases."
msgstr ""
"Interne ændringer for at bedre kunne understøtte fremtidige versioner af "
"NVDA."

#. type: Title ##
#, no-wrap
msgid "Version 3.2"
msgstr "Version 3.2"

#. type: Bullet: '* '
msgid "Add-on is compatible with NVDA 2018.3 (wxPython 4)."
msgstr "Tilføjelse er kompatibel med NVDA 2018.3 (wxPython 4)."

#. type: Title ##
#, no-wrap
msgid "Version 3.0"
msgstr "Version 3.0"

#. type: Bullet: '* '
msgid ""
"If using NVDA 2018.2, add-on settings will be found in new multi-category "
"settings screen under \"Golden Cursor\" category."
msgstr ""
"Hvis du bruger NVDA 2018.2, findes yderligere indstillinger i det nye "
"indstillingspanel under kategorien \"Golden Cursor\"."

#. type: Title ##
#, no-wrap
msgid "Version 2.1"
msgstr "Version 2.1"

#. type: Bullet: '* '
msgid "Fixed unicode decode error when trying to delete tag name."
msgstr "Rettede Unicode-afkodningsfejl, når du forsøger at slette tagnavne."

#. type: Bullet: '* '
msgid "Prevent Multiple Instances When Opening various add-on Dialogs."
msgstr ""
"Forhindre flere forekomster, når du åbner forskellige tilføjelsesdialoger."

#. type: Bullet: '* '
msgid ""
"Improved appearance of mouse positions list and jump to position dialogs."
msgstr ""
"Forbedret udseende af muspositionsliste og hoppe til positionsdialoger."

#. type: Title ##
#, no-wrap
msgid "Version 2.0"
msgstr "Version 2.0"

#. type: Bullet: '* '
msgid "Requires NVDA 2017.3 and later."
msgstr "Kræver NVDA 2017.3 og senere."

#. type: Bullet: '* '
msgid ""
"Position file format is incompatible with 1.x versions. If 1.x position "
"format is found, old positions will be migrated to the new format during "
"installation."
msgstr ""
"Positionfilformat er ukompatibelt med 1.x versioner. Hvis 1.x "
"positionsformat er fundet, vil gamle positioner blive migreret til det nye "
"format under installationen."

#. type: Bullet: '* '
msgid ""
"Added a new Golden Cursor settings dialog in NVDA's Preferences menu to "
"configure mouse movement unit and announcement of mouse positions as mouse "
"moves."
msgstr ""
"Tilføjet en ny Golden Cursor-indstillingsdialog i NVDAs indstillingsmenu for "
"at konfigurere musens bevægelsesenhed og annoncering af museposition, når "
"musen bevæges."

#. type: Bullet: '* '
msgid "Various messages from this add-on has changed."
msgstr "Forskellige meddelelser fra denne tilføjelse er ændret."

#. type: Bullet: '* '
msgid "When toggling various settings, toggle tone will no longer be heard."
msgstr ""
"Når du skifter forskellige indstillinger, bliver tonen ikke længere "
"afspillet."

#. type: Bullet: '* '
msgid ""
"You can now enter mouse arrows mode where you can move the mouse by pressing "
"just arrow keys."
msgstr ""
"Du kan nu indtaste musepile-tilstanden, hvor du kan flytte musen ved blot at "
"trykke på piletasterne."

#. type: Bullet: '* '
msgid ""
"Changes to positions list dialog, including new name (now called Mouse "
"Positions) and layout, displaying mouse coordinates for a label, and showing "
"the name of the active app as part of the title."
msgstr ""
"Ændringer i positionslistedialog, herunder nyt navn (nu kaldet "
"Musepositioner) og layout, der viser musekoordinater for en etiket og viser "
"navnet på den aktive app som en del af titellinjen."

#. type: Bullet: '* '
msgid ""
"From Mouse Positions dialog, pressing Enter on a saved label will move the "
"mouse to the saved location."
msgstr ""
"Fra dialogboksen Musepositioner flyttes musen til den gemte placering ved at "
"trykke på Enter på en gemt etiket."

#. type: Bullet: '* '
msgid ""
"When renaming a mouse position, an error dialog will be shown if a label "
"with the same name as the new name exists."
msgstr ""
"Når du omdøber en museposition, vises en fejldialog, hvis der findes en "
"etiket med samme navn som det nye navn."

#. type: Bullet: '* '
msgid ""
"When deleting or clearing mouse positions, you must now answer Yes before "
"positions are deleted and/or cleared."
msgstr ""
"Når du sletter eller rydder musepositioner, skal du nu svare Ja før "
"positioner slettes og/eller ryddes."

#. type: Bullet: '* '
msgid ""
"Changes to mouse jump feature, including a new name (now called New mouse "
"position) and ability to enter X and Y coordinates separately or by using up "
"or down arrow keys."
msgstr ""
"Ændringer i musehopfunktionen, herunder et nyt navn (nu kaldet Ny "
"museposition) og evne til at indtaste X- og Y-koordinater hver for sig eller "
"ved hjælp af pilene op og ned."

#. type: Bullet: '* '
msgid ""
"The dialog shown when saving the current mouse position now shows "
"coordinates for current mouse location."
msgstr ""
"Dialogboksen, der vises, når du gemmer den aktuelle museposition, viser nu "
"koordinater for den aktuelle museplacering."

#. type: Bullet: '* '
msgid ""
"When saving positions, resolved an issue where NVDA may play error tones if "
"the positions folder does not exist."
msgstr ""
"Løst et problem der udløste en fejltone, når du gemmer positioner, hvis "
"positionsmappen ikke findes."

#. type: Title ##
#, no-wrap
msgid "Version 1.4"
msgstr "Version 1.4"

#. type: Bullet: '* '
msgid ""
"Removed win32api dependency to make it compatible with past and future "
"versions of NVDA."
msgstr ""
"Fjernet win32api afhængighed for at gøre den kompatibel med tidligere og "
"fremtidige versioner af NVDA."

#. type: Title ##
#, no-wrap
msgid "Version 1.0"
msgstr "Version 1.0"

#. type: Bullet: '* '
msgid "Initial release."
msgstr "Første udgivelse."

#. type: Plain text
#, no-wrap
msgid "[[!tag stable dev]]\n"
msgstr "[[!tag stable dev]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=gc"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=gc"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=gc-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=gc-dev"

#, fuzzy
#~| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=gc"
#~ msgid "[3]: https://addons.nvda-project.org/files/get.php?file=gc-2019"
#~ msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=gc"

#~ msgid "Download [development version][2]"
#~ msgstr "Download [udviklingsversion][2]"
