# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Windows 10 App Essentials\n"
"POT-Creation-Date: 2022-01-21 05:10+0000\n"
"PO-Revision-Date: 2021-12-05 12:51+0200\n"
"Last-Translator: Jani Kinnunen <janikinnunen340@gmail.com>\n"
"Language-Team: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Windows App Essentials\"]]\n"
msgstr "[[!meta title=\"Windows App Essentials\"]]\n"

#. type: Bullet: '* '
msgid "Authors: Joseph Lee, Derek Riemer and others"
msgstr "Tekijät: Joseph Lee, Derek Riemer sekä muut"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Lataa [vakaa versio][1]"

#. type: Bullet: '* '
msgid "Download [development version][2]"
msgstr "Lataa [kehitysversio][2]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2021.2 and later"
msgstr "Yhteensopivuus: NVDA 2021.2 ja uudemmat"

#. type: Plain text
msgid ""
"Note: Originally called Windows 10 App Essentials, it was renamed to Windows "
"App Essentials in 2021 to support Windows 10 and future releases such as "
"Windows 11. Parts of this add-on will still refer to the original add-on "
"name."
msgstr ""
"Huom: Tämä lisäosa (alkuperäiseltä nimeltään Windows 10 App Essentials) on "
"nimetty uudelleen Windows App Essentialsiksi vuonna 2021 tukemaan Windows 10:"
"tä sekä tulevia versioita, kuten Windows 11:tä. Lisäosassa viitataan vielä "
"osittain vanhaan nimeen."

#. type: Plain text
msgid ""
"This add-on is a collection of app modules for various modern Windows apps, "
"as well as enhancements and fixes for certain controls found in Windows 10 "
"and later."
msgstr ""
"Tämä lisäosa on kokoelma sovellusmoduuleita moderneille Windows-"
"sovelluksille sekä laajennuksia ja korjauksia tietyille säätimille Windows "
"10:ssä ja uudemmissa."

#. type: Plain text
msgid ""
"The following app modules or support modules for some apps are included (see "
"each app section for details on what is included):"
msgstr ""
"Kokoelmaan sisältyvät seuraavat sovellus- tai tukimoduulit (katso tiedot "
"käytettävissä olevista ominaisuuksista kunkin sovelluksen kappaleesta):"

#. type: Title ##
#, no-wrap
msgid "Calculator"
msgstr "Laskin"

#. type: Title ##
#, no-wrap
msgid "Cortana"
msgstr "Cortana"

#. type: Title ##
#, no-wrap
msgid "Mail"
msgstr "Sähköposti"

#. type: Title ##
#, no-wrap
msgid "Maps"
msgstr "Kartat"

#. type: Title ##
#, no-wrap
msgid "Microsoft Solitaire Collection"
msgstr "Microsoft Solitaire -kokoelma"

#. type: Bullet: '* '
msgid ""
"Modern keyboard (emoji panel/dictation/voice typing/hardware input "
"suggestions/clipboard history/modern input method editors)"
msgstr ""
"Moderni näppäimistö (emojipaneeli/sanelu/puhekirjoitus/fyysisen "
"näppäimistösyötteen ehdotukset/pilvileikepöydän historia/modernin "
"syöttömenetelmän muokkaimet)"

#. type: Bullet: '* '
msgid "Notepad (Windows 11)"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "People"
msgstr "Ihmiset"

#. type: Bullet: '* '
msgid "Settings (system settings, Windows+I)"
msgstr "Asetukset (järjestelmän asetukset, Windows+I)"

#. type: Title ##
#, no-wrap
msgid "Weather"
msgstr "Sää"

#. type: Bullet: '* '
msgid "Miscellaneous modules for controls such as Start Menu tiles"
msgstr "Sekalaisia moduuleita säätimille, esim. Käynnistä-valikon ruuduille."

#. type: Plain text
msgid "Notes:"
msgstr "Huomautuksia:"

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "This add-on requires Windows 10 20H2 (build 19042) or later and is "
#| "compatible with Windows 11."
msgid ""
"This add-on requires Windows 10 21H1 (build 19043) or later and is "
"compatible with Windows 11."
msgstr ""
"Tämä lisäosa edellyttää Windows 10:n versiota 20H2 (koontiversio 19042) tai "
"uudempaa ja on yhteensopiva Windows 11:n kanssa."

#. type: Bullet: '* '
msgid ""
"Although installation is possible, this add-on does not support Windows "
"Enterprise LTSC (Long-Term Servicing Channel) and Windows Server releases."
msgstr ""
"Tämä lisäosa ei tue Windows 10 Enterprise LTSC:tä (Long-Term Servicing "
"Channel) eikä Windows Server -versioita, vaikka asennus onkin mahdollista."

#. type: Bullet: '* '
msgid "Some add-on features are or will be part of NVDA screen reader."
msgstr "Jotkin lisäosan ominaisuudet ovat tai tulevat olemaan osa NVDA:ta."

#. type: Bullet: '* '
msgid ""
"For entries not listed below, you can assume that features are part of NVDA, "
"no longer applicable as the add-on does not support unsupported Windows "
"releases such as old Windows 10 versions, or changes were made to Windows "
"and apps that makes entries no longer applicable."
msgstr ""
"Voidaan olettaa, että ominaisuudet, joita ei ole lueteltu alla, joko "
"sisältyvät NVDA:han, eivät ole enää käytössä, koska lisäosa ei tue tuen "
"piiristä poistuneita Windowseja, kuten vanhoja 10:n versioita, tai eivät ole "
"enää käyttökelpoisia Windowsiin ja sovelluksiin tehtyjen muutosten vuoksi."

#. type: Bullet: '* '
msgid ""
"Some apps support compact overlay mode (always on top in Calculator, for "
"example), and this mode will not work properly with portable version of NVDA."
msgstr ""
"Jotkin sovellukset tukevat kompaktia peitetilaa (esim. Laskimessa Aina "
"päällimmäisenä), joka ei toimi oikein NVDA:n massamuistiversion kanssa."

#. type: Plain text
msgid ""
"For a list of changes made between each add-on releases, refer to "
"[changelogs for add-on releases][3] document."
msgstr ""
"Katso luettelo lisäosan kaikkiin versioihin tehdyistä muutoksista  "
"[lisäosajulkaisujen muutoslokeista.][3]"

#. type: Title ##
#, no-wrap
msgid "General"
msgstr "Yleistä"

#. type: Bullet: '* '
msgid ""
"NVDA can announce suggestion count when performing a search in majority of "
"cases, including when suggestion count changes as search progresses. This is "
"now part of NVDA 2021.3."
msgstr ""
"NVDA voi ilmoittaa ehdotusten määrän useimmissa tapauksissa hakua "
"suoritettaessa, mukaan lukien tilanteet, joissa ehdotusten määrä muuttuu "
"haun edistyessä. Tämä sisältyy NVDA 2021.3:een."

#. type: Bullet: '* '
msgid ""
"In addition to UIA event handlers provided by NVDA, the following UIA events "
"are recognized: drag complete, drop target dropped, layout invalidated. With "
"NVDA's log level set to debug, these events will be tracked, and for UIA "
"notification event, a debug tone will be heard if notifications come from "
"somewhere other than the currently active app. Events built into NVDA such "
"as name change and controller for events are tracked from an add-on called "
"Event Tracker."
msgstr ""
"Seuraavat UIA-tapahtumat tunnistetaan NVDA:n tarjoamien UIA-"
"tapahtumakäsittelijöiden lisäksi: vetäminen suoritettu, vetämisen kohde "
"pudotettu, asettelu mitätöity. Näitä tapahtumia seurataan, kun NVDA:n "
"lokitasoksi on määritetty \"virheenkorjaus\", ja UIA-ilmoitustapahtuma "
"ilmaistaan virheäänellä, mikäli ilmoitukset tulevat muualta kuin "
"aktiivisesta sovelluksesta. NVDA:han sisäänrakennettuja tapahtumia, kuten "
"nimen muutos ja ohjain tapahtumille seurataan Event Tracker -nimisestä "
"lisäosasta."

#. type: Bullet: '* '
msgid ""
"When opening, closing, reordering (Windows 11), or switching between virtual "
"desktops, NVDA will announce active virtual desktop name (desktop 2, for "
"example)."
msgstr ""
"NVDA ilmoittaa aktiivisen virtuaalityöpöydän nimen (esim. työpöytä 2) "
"avattaessa, suljettaessa, uudelleenjärjestettäessä (Windows 11) tai "
"siirryttäessä niiden välillä."

#. type: Bullet: '* '
msgid ""
"NVDA will no longer announce Start menu size text when changing screen "
"resolutions or orientation."
msgstr ""
"NVDA ei enää ilmoita Käynnistä-valikon kokoa  näytön resoluutiota tai "
"suuntaa vaihdettaessa."

#. type: Bullet: '* '
msgid ""
"When arranging Start menu tiles or Action Center quick actions with Alt+Shift"
"+arrow keys, NVDA will announce information on dragged items or new position "
"of the dragged item."
msgstr ""
"Kun Käynnistä-valikon ruutuja tai Toimintokeskuksen pikatoimintoja "
"järjestellään Alt+Vaihto+nuolinäppäimillä, NVDA puhuu raahattujen kohteiden "
"tiedot tai raahatun kohteen uuden sijainnin."

#. type: Bullet: '* '
msgid ""
"Announcements such as volume/brightness changes in File Explorer and app "
"update notifications from Microsoft Store can be suppressed by turning off "
"Report Notifications in NVDA's object presentation settings."
msgstr ""
"Ilmoitukset, kuten äänenvoimakkuuden/kirkkauden muutokset "
"resurssienhallinnassa sekä sovellusten päivitysilmoitukset Microsoft "
"Storesta voidaan estää poistamalla käytöstä Lue ilmoitukset -asetus NVDA:n "
"objektien lukemisen asetuksista."

#. type: Bullet: '* '
msgid "NVDA will no longer announce graphing calculator screen message twice."
msgstr "NVDA ei enää sano graafisen laskinnäytön ilmoitusta kahdesti."

#. type: Bullet: '* '
#, fuzzy
#| msgid "In Windows 11, breadcrumb bar items are properly recognized."
msgid "In Windows 10, history and memory list items are properly labeled."
msgstr "Navigointipolkupalkin kohteet tunnistetaan oikein Windows 11:ssä."

#. type: Bullet: '* '
msgid "Textual responses from Cortana are announced in most situations."
msgstr "Cortanan tekstimuotoiset vastaukset puhutaan useimmissa tilanteissa."

#. type: Bullet: '* '
msgid "NVDA will be silent when talking to Cortana via voice."
msgstr "NVDA on hiljaa puhuttaessa Cortanalle mikrofonin välityksellä."

#. type: Bullet: '* '
msgid ""
"When reviewing items in messages list, you can now use table navigation "
"commands to review message headers. Note that navigating between rows "
"(messages) is not supported."
msgstr ""
"Voit nyt käyttää viestiluettelon kohteita tarkastellessasi "
"taulukkonavigointikomentoja viestiotsakkeiden lukemiseen. Huomaa, että "
"rivien (viestien) välillä liikkumista ei tueta."

#. type: Bullet: '* '
msgid "NVDA plays location beep for map locations."
msgstr "NVDA toistaa äänimerkin karttasijainneille."

#. type: Bullet: '* '
msgid ""
"When using street side view and if \"use keyboard\" option is enabled, NVDA "
"will announce street addresses as you use arrow keys to navigate the map."
msgstr ""
"NVDA ilmoittaa katuosoitteet käyttäessäsi nuolinäppäimiä kartalla "
"liikkumiseen oltaessa katunäkymässä ja mikäli \"käytä näppäimistöä\" -"
"vaihtoehto on otettu käyttöön."

#. type: Bullet: '* '
msgid "NVDA will announce names of cards and card decks."
msgstr "NVDA sanoo korttien ja korttipakkojen nimet."

#. type: Title ##
#, no-wrap
msgid "Modern keyboard"
msgstr "Moderni näppäimistö"

#. type: Plain text
msgid ""
"This includes emoji panel, clipboard history, dictation/voice typing, "
"hardware input suggestions, and modern input method editors for certain "
"languages. When viewing emojis, for best experience, enable Unicode "
"Consortium setting from NVDA's speech settings and set symbol level to \"some"
"\" or higher. When pasting from clipboard history in Windows 10, press Space "
"key instead of Enter key to paste the selected item. NVDA also supports "
"updated input experience panel in Windows 11."
msgstr ""
"Näitä ovat emojipaneeli, leikepöydän historia, sanelu/puhekirjoitus, "
"ehdotukset syötettäessä tekstiä fyysisellä näppäimistöllä sekä modernin "
"syöttömenetelmän editorit tietyille kielille. Ota käyttöön emojeita "
"tarkasteltaessa Unicode-konsortion datan asetus NVDA:n puheasetuksista "
"parhaan kokemuksen saamiseksi, ja aseta symbolitasoksi \"jotain\" tai "
"korkeampi. Kun liität leikepöydän historiasta, liitä valittu kohde "
"painamalla välilyöntinäppäintä Enterin sijaan. NVDA tukee lisäksi "
"päivitettyä syöttökokemuksen paneelia Windows 11:ssä."

#. type: Bullet: '* '
msgid ""
"In Windows 10, when an emoji group (including kaomoji and symbols group) is "
"selected, NVDA will no longer move navigator object to certain emojis."
msgstr ""
"Kun emojiryhmä (kaomoji ja symboliryhmä mukaan lukien) valitaan Windows 10:"
"ssä, NVDA ei enää siirrä navigointiobjektia tiettyihin emojeihin."

#. type: Bullet: '* '
msgid ""
"Added support for updated input experience panel (combined emoji panel and "
"clipboard history) in Windows 11."
msgstr ""
"Lisätty tuki Windows 11:n päivitetylle syöttökokemuksen paneelille "
"(yhdistetty emojipaneeli ja leikepöydän historia)."

#. type: Bullet: '* '
msgid ""
"In Windows 11, it is again possible to use the arrow keys to review emojis "
"when emoji panel opens."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Notes:"
msgid "Notepad"
msgstr "Huomautuksia:"

#. type: Plain text
msgid "This refers to Windows 11 Notepad version 11 or later."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will announce status items such as line and column information when "
"report status bar command (NVDA+End in desktop layout, NvDA+Shift+End in "
"laptop layout) is performed."
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "NVDA will no longer announce Start menu size text when changing screen "
#| "resolutions or orientation."
msgid ""
"NVDA will no longer announce entered text when pressing Enter key from the "
"document."
msgstr ""
"NVDA ei enää ilmoita Käynnistä-valikon kokoa  näytön resoluutiota tai "
"suuntaa vaihdettaessa."

#. type: Bullet: '* '
msgid ""
"When searching for contacts, first suggestion will be announced, "
"particularly if using recent app releases."
msgstr ""
"Ensimmäinen ehdotus puhutaan kontakteja etsittäessä, erityisesti uusimpia "
"sovellusversioita käytettäessä."

#. type: Title ##
#, no-wrap
msgid "Settings"
msgstr "Asetukset"

#. type: Bullet: '* '
msgid ""
"Certain information such as Windows Update progress is reported "
"automatically, including Storage sense/disk cleanup widget and errors from "
"Windows Update."
msgstr ""
"Määrätyt tiedot, kuten Windows Updaten päivitysten asennuksen edistyminen, "
"mukaan lukien Tallennusseurannan/Levynsiivouksen pienoisohjelma sekä Windows "
"Updaten virheet, puhutaan nyt automaattisesti."

#. type: Bullet: '* '
msgid ""
"Progress bar values and other information are no longer announced twice."
msgstr "Edistymispalkkien arvoja tai muita tietoja ei lueta enää kahdesti."

#. type: Bullet: '* '
msgid ""
"Odd control labels seen in certain Windows installations has been corrected."
msgstr ""
"Joissakin Windows-asennuksissa näkyvät erikoiset säädinten nimet on korjattu."

#. type: Bullet: '* '
msgid ""
"NVDA will announce the name of the optional quality update control if "
"present (download and install now link in Windows 10, download button in "
"Windows 11)."
msgstr ""
"NVDA puhuu valinnaisen laatupäivityksen säätimen nimen (Lataa ja asenna nyt -"
"linkki Windows 10:ssä, Lataa-painike Windows 11:ssä), mikäli sellainen on "
"näkyvissä."

#. type: Bullet: '* '
msgid "In Windows 11, breadcrumb bar items are properly recognized."
msgstr "Navigointipolkupalkin kohteet tunnistetaan oikein Windows 11:ssä."

#. type: Bullet: '* '
msgid ""
"Tabs such as \"forecast\" and \"maps\" are recognized as proper tabs (patch "
"by Derek Riemer)."
msgstr ""
"Sellaiset välilehdet kuten \"ennuste\" ja \"kartat\" tunnistetaan oikeiksi "
"välilehdiksi (korjauksen tehnyt Derek Riemer)."

#. type: Bullet: '* '
msgid ""
"When reading a forecast, use the left and right arrows to move between "
"items. Use the up and down arrows to read the individual items. For example, "
"pressing the right arrow might report \"Monday: 79 degrees, partly "
"cloudy, ...\" pressing the down arrow will say \"Monday\" Then pressing it "
"again will read the next item (Like the temperature). This currently works "
"for daily and hourly forecasts."
msgstr ""
"Käytä ennustetta lukiessasi vasenta ja oikeaa nuolta kohteiden välillä "
"liikkumiseen. Käytä nuolta ylös ja alas yksittäisten kohteiden lukemiseen. "
"Esim.  oikeaa nuolta painettaessa saatetaan sanoa \"Maanantai: 26,1 astetta, "
"puolipilvistä, ...\", ja nuolta alas painettaessa \"Maanantai\". Uudelleen "
"painaminen lukee seuraavan kohteen (kuten lämpötilan). Toimii tällä hetkellä "
"päivittäisiin ja tunnin välein tehtäviin ennusteisiin."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=w10"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=w10"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"

#. type: Plain text
msgid "[3]: https://github.com/josephsl/wintenapps/wiki/w10changelog"
msgstr "[3]: https://github.com/josephsl/wintenapps/wiki/w10changelog"

#~ msgid "Microsoft Store"
#~ msgstr "Microsoft Store"

#~ msgid ""
#~ "When downloading content such as apps and movies, NVDA will announce "
#~ "product name and download progress."
#~ msgstr ""
#~ "NVDA ilmoittaa tuotteen nimen ja latauksen edistymisen sisältöä, kuten "
#~ "sovelluksia ja elokuvia ladattaessa."

#~ msgid "Calculator (modern)"
#~ msgstr "Laskin (moderni)"

#~ msgid "Cortana (Conversations)"
#~ msgstr "Cortana (keskustelut)"

#~ msgid ""
#~ "Most items are applicable when using Cortana Conversations (Windows 10 "
#~ "2004 and later)."
#~ msgstr ""
#~ "Useimpia kohtia sovelletaan Cortana-keskusteluja käytettäessä (Windows 10:"
#~ "n versio 2004 ja uudemmat)."

#~ msgid ""
#~ "After checking for app updates, app names in list of apps to be updated "
#~ "are correctly labeled."
#~ msgstr ""
#~ "Sovellusten nimet näytetään oikein päivitettävien sovellusten luettelossa "
#~ "päivitystarkistuksen jälkeen."

#~ msgid ""
#~ "Support for Windows 11 is experimental, and some features will not work "
#~ "(see relevant entries for details). A warning dialog will be shown if "
#~ "trying to install stable versions of this add-on on Windows 11 prior to "
#~ "general availability."
#~ msgstr ""
#~ "Windows 11:n tuki on kokeellinen, eivätkä kaikki ominaisudet toimi (katso "
#~ "lisätietoja asianmukaisista kohdista). Varoitusvalintaikkuna näytetään, "
#~ "jos yrität asentaa tämän lisäosan vakaita versioita Windows 11:een ennen "
#~ "yleistä saatavuutta."

#~ msgid ""
#~ "When searching in Start menu or File Explorer in Windows 10 1909 "
#~ "(November 2019 Update) and later, instances of NVDA announcing search "
#~ "results twice when reviewing results are less noticeable, which also "
#~ "makes braille output more consistent when reviewing items."
#~ msgstr ""
#~ "NVDA puhuu nyt hakua suoritettaessa vähemmän hakutuloksia kahdesti "
#~ "Käynnistä-valikossa tai Resurssienhallinnassa versiossa 1909 (November "
#~ "2019 -päivitys) ja uudemmissa, mikä tekee lisäksi "
#~ "pistekirjoitustulosteesta yhdenmukaisempaa kohteita tarkasteltaessa."

#~ msgid ""
#~ "In Windows 10 1909 (November 2019 Update) and later, modern search "
#~ "experience in File Explorer powered by Windows Search user interface is "
#~ "supported."
#~ msgstr ""
#~ "Modernia Windows-haun käyttöliittymän voimalla toimivaa "
#~ "resurssienhallinnan hakukokemusta tuetaan versiossa 1909 (marraskuun 2019 "
#~ "päivitys) ja uudemmissa."

#~ msgid ""
#~ "When opening clipboard history, NVDA will no longer announce \"clipboard"
#~ "\" when there are items in the clipboard under some circumstances."
#~ msgstr ""
#~ "NVDA ei enää sano leikepöydän historiaa avattaessa joissakin tilanteissa "
#~ "\"leikepöytä\", kun leikepöydällä on kohteita."

#~ msgid ""
#~ "On some systems running Windows 10 1903 (May 2019 Update) and later, NVDA "
#~ "will no longer appear to do nothing when emoji panel opens."
#~ msgstr ""
#~ "Emojipaneelin avautuessa ei enää näytä siltä, että NVDA  ei tee mitään "
#~ "joissakin järjestelmissä, joissa on asennettuna Windows 10:n versio 1903 "
#~ "(May 2019 -päivitys) tai uudempi."

#~ msgid "Calendar"
#~ msgstr "Kalenteri"

#~ msgid ""
#~ "It is possible to tracke only specific events and/or events coming from "
#~ "specific apps."
#~ msgstr ""
#~ "On mahdollista seurata vain tiettyjä tapahtumia ja/tai tietyistä "
#~ "sovelluksista tulevia tapahtumia."

#~ msgid ""
#~ "NVDA no longer announces \"edit\" or \"read-only\" in message body and "
#~ "other fields."
#~ msgstr ""
#~ "NVDA ei sano enää \"muokattava\" tai \"vain luku\" viestisisällössä ja "
#~ "muissa kentissä."

#~ msgid ""
#~ "When writing a message, appearance of at mention suggestions are "
#~ "indicated by sounds."
#~ msgstr ""
#~ "Ät-maininnan ehdotukset ilmaistaan äänimerkeillä viestiä kirjoitettaessa."

#~ msgid "Windows Update reminder dialog is recognized as a proper dialog."
#~ msgstr ""
#~ "Windows Updaten muistutusvalintaikkuna tunnistetaan asianmukaisesti "
#~ "valintaikkunaksi."

#, fuzzy
#~| msgid ""
#~| "In more recent revisions of Version 1803 and later, due to changes to "
#~| "Windows Update procedure for feature updates, a \"download and install "
#~| "now\" link has been added. NVDA will now announce the title for the new "
#~| "update if present."
#~ msgid ""
#~ "In more recent revisions of Windows 10 1803 and later, due to changes to "
#~ "Windows Update procedure for feature updates, a \"download and install now"
#~ "\" link has been added. NVDA will now announce the title for the new "
#~ "update if present."
#~ msgstr ""
#~ "Windows Update -prosessiin ominaisuuspäivitysten osalta tehtyjen "
#~ "muutosten vuoksi uudempiin 1803:n versioihin ja sitä tuoreempiin  on "
#~ "lisätty Lataa ja asenna nyt -linkki. Jos uusi päivitys on saatavilla, "
#~ "NVDA puhuu nyt sen nimen."

#~ msgid ""
#~ "In addition to dialogs recognized by NVDA, more dialogs are now "
#~ "recognized as proper dialogs and reported as such, including Insider "
#~ "Preview dialog (settings app)."
#~ msgstr ""
#~ "Entistä enemmän valintaikkunoita tunnistetaan ja ilmoitetaan nyt "
#~ "asianmukaisesti valintaikkunoina, mukaan lukien Insider-esiversion "
#~ "valintaikkuna (Asetukset-sovellus). Tämä sisältyy nyt NVDA 2018.3:een."

#~ msgid ""
#~ "NVDA will no longer play error tones or do nothing if this add-on becomes "
#~ "active from Windows 7, Windows 8.1, and unsupported releases of Windows "
#~ "10."
#~ msgstr ""
#~ "NVDA ei toista enää virheääniä tai tee mitään, mikäli tämä lisäosa "
#~ "aktivoituu Windows 7:ssä, 8.1:ssä tai sellaisissa Windows 10:n "
#~ "versioissa, joita ei enää tueta."

#~ msgid ""
#~ "When ENTER or Escape is pressed, NVDA will announce calculation results."
#~ msgstr ""
#~ "NVDA ilmoittaa laskutoimituksen tuloksen Enteriä tai Esciä painettaessa."

#~ msgid ""
#~ "For calculations such as unit converter and currency converter, NVDA will "
#~ "announce results as soon as calculations are entered."
#~ msgstr ""
#~ "NVDA puhuu laskutoimitusten tulokset (esim. yksikkö- ja "
#~ "valuuttamuuntimessa) heti laskukaavoja syötettäessä."

#~ msgid ""
#~ "NVDA will notify if maximum digit count has been reached while entering "
#~ "expressions."
#~ msgstr ""
#~ "NVDA ilmoittaa, jos lukujen enimmäismäärä saavutetaan ilmaisuja "
#~ "kirjoitettaessa."

#~ msgid ""
#~ "Added support for modern Chinese, Japanese, and Korean (CJK) IME "
#~ "candidates interface introduced in Version 2004 (build 18965 and later)."
#~ msgstr ""
#~ "Lisätty tuki modernille kiinan, japanin ja korean (CJK) IME-ehdotusten "
#~ "liittymälle, joka esiteltiin versiossa 2004 (koontiversio 18965 ja "
#~ "uudemmat)."

#~ msgid "Weather."
#~ msgstr "Sää"
