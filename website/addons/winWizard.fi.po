# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Win Wizard\n"
"POT-Creation-Date: 2021-06-22 18:33+0000\n"
"PO-Revision-Date: 2021-06-23 15:30+0200\n"
"Last-Translator: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language-Team: Jani Kinnunen <janikinnunen340@gmail.com>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Win Wizard\"]]\n"
msgstr "[[!meta title=\"Win Wizard\"]]\n"

#. type: Bullet: '* '
msgid "Author: Oriol Gómez, current maintenance by Łukasz Golonka"
msgstr "Tekijä: Oriol Gómez, nykyinen ylläpitäjä Łukasz Golonka"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2019.3 and beyond"
msgstr "Yhteensopivuus: NVDA 2019.3 tai uudempi"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Lataa [vakaa versio][1]"

#. type: Plain text
msgid ""
"This add-on allows you to perform some operations on the focused window or "
"the process associated with it."
msgstr ""
"Tämän lisäosan avulla voit suorittaa joitakin toimintoja aktiiviselle "
"ikkunalle tai siihen liittyvälle prosessille."

#. type: Title ##
#, no-wrap
msgid "Keyboard commands:"
msgstr "Näppäinkomennot:"

#. type: Plain text
msgid ""
"All these commands can be remapped from the Input gestures dialog in the Win "
"Wizard category."
msgstr ""
"Näitä komentoja voidaan muuttaa Syötekomennot-valintaikkunan Win Wizard -"
"kategoriasta."

#. type: Title ###
#, no-wrap
msgid "Hiding and showing hidden windows:"
msgstr "Ikkunoiden piilottaminen ja piilotettujen ikkunoiden näyttäminen:"

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+numbers from 1 to 0 - hides currently focused window in the "
"slot corresponding to the pressed number"
msgstr ""
"NVDA+Win+numerot 1-0: Piilottaa aktiivisen ikkunan painettua numeroa "
"vastaavaan paikkaan."

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+left arrow - moves to the previous stack of hidden windows."
msgstr ""
"NVDA+Win+Vasen nuoli: Siirtää edelliseen piilotettujen ikkunoiden pinoon."

#. type: Bullet: '* '
msgid "NVDA+Windows+right arrow - moves to the next stack of hidden windows."
msgstr ""
"NVDA+Win+Oikea nuoli: Siirtää seuraavaan piilotettujen ikkunoiden pinoon."

#. type: Bullet: '* '
msgid ""
"Windows+Shift+h - hides the currently focused window in the first available "
"slot"
msgstr ""
"Win+Vaihto+H: Piilottaa aktiivisen ikkunan ensimmäiseen käytettävissä "
"olevaan paikkaan."

#. type: Bullet: '* '
msgid "NVDA+Windows+h - shows the last hidden window"
msgstr "NVDA+Win+H: Näyttää viimeksi piilotetun ikkunan."

#. type: Bullet: '* '
msgid ""
"Windows+Shift+l - shows the list of all hidden windows grouped by the stacks "
"(please note that by default last hidden window is selected)"
msgstr ""
"Win+Vaihto+L: Näyttää luettelon kaikista piilotetuista ikkunoista pinojen "
"mukaan ryhmiteltyinä (viimeksi piilotettu ikkuna on oletusarvoisesti "
"valittuna)."

#. type: Title ###
#, no-wrap
msgid "Managing processes:"
msgstr "Prosessien hallinta:"

#. type: Bullet: '* '
msgid ""
"Windows+F4 - kills the process associated with the currently focused window"
msgstr "Win+F4: Lopettaa aktiiviseen ikkunaan liittyvän prosessin."

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+p - opens dialog allowing you to set priority of the process "
"associated with the currently focused window"
msgstr ""
"NVDA+Win+P: Avaa valintaikkunan, jossa voit asettaa aktiiviseen ikkunaan "
"liittyvän prosessin prioriteetin."

#. type: Title ###
#, no-wrap
msgid "Miscellaneous  commands:"
msgstr "Sekalaiset komennot:"

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+TAB - switches between top level windows of the current program "
"(useful in foobar2000, Back4Sure etc.) Since this command moves the system "
"focus it can be found in the System focus category of the Input gestures "
"dialog."
msgstr ""
"NVDA+Win+Sarkain: Vaihtaa nykyisen ohjelman ylätason ikkunoiden välillä "
"(hyödyllinen foobar2000:ssa, Back4Suressa jne.) Koska tämä komento siirtää "
"järjestelmän kohdistusta, se löytyy Syötekomennot-valintaikkunan "
"Järjestelmän kohdistus -kategoriasta."

#. type: Bullet: '* '
msgid ""
"CTRL+ALT+T - allows you to change title of the currently focused program"
msgstr ""
"Ctrl+Alt+T: Mahdollistaa aktiivisen ohjelman ikkunan nimen muuttamisen."

#. type: Title ##
#, no-wrap
msgid "Changes:"
msgstr "Muutokset:"

#. type: Title ##
#, no-wrap
msgid "Changes for 5.0.3:"
msgstr "Muutokset versiossa 5.0.3:"

#. type: Bullet: '* '
msgid "Compatibility with NVDA 2021.1"
msgstr "Yhteensopiva NVDA 2021.1:n kanssa"

#. type: Title ##
#, no-wrap
msgid "Changes for 5.0.2:"
msgstr "Muutokset versiossa 5.0.2:"

#. type: Plain text
msgid "- First release available from the add-ons website"
msgstr "- Ensimmäinen lisäosasivustolta saatavilla oleva versio."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=winwizard"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=winwizard"
