[[!meta title="Add-ons and backwards incompatible NVDA releases"]]

Note: this document is based on an older "NVDA add-ons and Python 3" document, which is also available for historical reasons.

This page aims to document add-ons compatible with latest backwards incompatible version of NVDA, as well as highlighting their status regarding their availability on this website.

## What you need to know about backwards incompatible NVDA releases and add-ons

Once a year, NV Access may publish a backwards incompatible NVDA release. Here, "backwards incompatible" refers to NVDA releases that make add-ons incompatible due to API changes. When this happens, authors must test their add-ons for compatibility and release updates accordingly.

As mentioned in the NVDA's user guide:

The Incompatible Add-ons Manager, which can be accessed via the "view incompatible add-ons" button in the Add-on manager, allows you to inspect any incompatible add-ons, and the reason they are considered incompatible. Add-ons are considered incompatible when they have not been updated to work with significant changes to NVDA, or when they rely on a feature not available in the version of NVDA you are using. The Incompatible add-ons manager has a short message to explain its purpose as well as the version of NVDA. 

The Incompatible add-ons manager also has an "About add-on..." button. This dialog will provide you with the full details of the add-on, which is helpful when contacting the add-on author. 

### Key changes in backwards incompatible NVDA releases

Below is a list of backwards incompatible NVDA releases and key changes in each:

* 2019.3: Python 2 to 3, speech refactor
* 2021.1: code refactoring, wxPython 4.1.1

Note to add-on authors: when backwards incompatible NVDA release enters beta testing phase (with the release of beta 1), be sure to test your add-ons and report your next steps, including update plans and release announcements to NVDA community through various channels (add-ons list, users list, Facebook, Twitter, etc.). Also, send a pull request against nvaccess/addonFiles repo on GitHub so that updated add-ons can be posted on this website.

Unless otherwise specified, add-ons listed on this page are checked against latest backwards incompatible NVDA release (2021.1).

## Status of add-ons compatible with NVDA 2021.1 and availability on the website

Below is a list of add-ons hosted on this website (in the stable section). The list also includes contact information for add-on author(s).

### Notes:

* Add-on compatibility status subject to change without notice.
* Please do NOT change the manifest.ini file of incompatible add-ons if you don't know what you're doing since this may result in unpredictable behavior such as making NVDA less stable.
* When maintainers have asked to post an add-on on the website via a pull request, this will be reflected as "coming soon to the website".

### Access8Math

* Compatible: Yes (updated on the website)
* Contact: Tseng Woody <tsengwoody.tw@gmail.com>

### Add-on Updater

* Compatible: Yes (updated on the website)
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### Add-ons Documentation

* Compatible: Yes (updated on the website)
* Contact: Rui Fontes <rui.fontes@tiflotecnia.com>, Zougane, Rémy and Abdel

### Addon to count elements of selected text

* Compatible: Yes (updated on the website)
* Contact: Rui Fontes <rui.fontes@tiflotecnia.com>

### Audio Themes

* Compatible: No
* Contact: Musharraf Omer <ibnomer2011@hotmail.com>

### AudioChart

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Beep keyboard

* Compatible: No
* Contact: David CM <dhf360@gmail.com>

### Bluetooth Audio

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Braille Extender

* Compatible: Yes (updated on the website)
* Contact: André-Abush Clause <dev@andreabc.net>

### BrowserNav

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Calibre

* Compatible: Yes (updated on the website)
* Contact: Javi Dominguez <fjavids@gmail.com>

### Character Information

* Compatible: Yes (updated on the website)
* Contact: Cyrille Bougot <cyrille.bougot2@laposte.net>

### Check Input Gestures

* Compatible: Yes (updated on the website)
* Contact: Oleksandr Gryshchenko <grisov.nvaccess@mailnull.com>

### Classic Selection

* Compatible: Yes (updated on the website)
* Contact: Tyler Spivey <tspivey@pcdesk.net>

### Clip Contents Designer

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>

### Clipspeak

* Compatible: No
* Contact: Damien Sykes-Lindley <damien@dcpendleton.plus.com>

### Clock and calendar Add-on for NVDA

* Compatible: Yes (updated on the website)
* Contact: Hrvoje Katić <hrvojekatic@gmail.com>, Abdel <abdelkrim.bensaid@gmail.com>

### Console Toolkit

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Control Usage Assistant

* Compatible: Yes (updated on the website)
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### Crash Hero

* Compatible: No
* Contact: Derek Riemer <driemer.riemer@gmail.com>

### Day of the week

* Compatible: Yes (updated on the website)
* Contact: Abdel <abdelkrim.bensaid@gmail.com>, Noelia Ruiz Martínez <nrm1977@gmail.com>

### Debug Helper/Dev Helper

* Compatible: Yes (updated on the website)
* Note: renamed to Dev Helper in 2021
* Contact: Luke Davis <newanswertech@gmail.com>

### Developer Toolkit

* Compatible: Yes (not updated on the website)
* Contact: Andy Borka <ajborka@gmail.com>

### Direct Link

* Compatible: Yes (updated on the website)
* Contact: Fawaz Abdul rahman <fawaz.ar94@gmail.com>

### Dropbox

* Compatible: Yes (updated on the website)
* Contact: Patrick Zajda <patrick@zajda.fr>, Filaos and other contributors

### Dual Voice

* Compatible: Yes (external link
* Contact: Seyed Mahmood Taghavi-Shahri

### Easy Table Navigator

* Compatible: Yes (updated on the website)
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### Emoticons

* Compatible: Yes (updated on the website)
* Contact: Chris Leo <llajta2012@gmail.com>, Noelia Ruiz Martínez <nrm1977@gmail.com>, Mesar Hameed <mesar.hameed@gmail.com>, Francisco Javier Estrada Martínez <Fjestrad@hotmail.com>

### eMule

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>, Chris <llajta2012@gmail.com>, Alberto <a.buffolino@gmail.com>

### Enhanced Aria

* Compatible: No
* Contact: José Manuel Delicado <jm.delicado@nvda.es>

### Enhanced Touch Gestures

* Compatible: Yes (updated on the website)
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### Event Tracker

* Compatible: Yes (updated on the website)
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### extendedWinamp

* Compatible: Yes (updated on the website)
* Contact: Hrvoje Katić <hrvojekatic@gmail.com>, NVDA Add-ons Team

### Focus Highlight

* Compatible: No
* Contact: Takuya Nishimoto <nishimotz@gmail.com>

### Golden Cursor

* Compatible: Yes (updated on the website)
* Note: no longer maintained
* Contact: Salah Atair <atair1978@gmail.com>, Wafeeq Taher, Joseph Lee <joseph.lee22590@gmail.com>, Abdel <abdelkrim.bensaid@gmail.com>

### GoldWave

* Compatible: Yes (updated on the website)
* Note: no longer maintained
* Contact: Joseph Lee <joseph.lee22590@gmail.com>, David Parduhn <xkill85@gmx.net>, Mesar Hameed <mhameed@src.gnome.org>

### IndentNav

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Input Lock

* Compatible: Yes (updated on the website)
* Contact: José Manuel Delicado <jm.delicado@nvda.es>

### instantTranslate

* Compatible: Yes (updated on the website)
* Contact: Alexy Sadovoy aka Lex <lex@progger.su>, ruslan <ru2020slan@yandex.ru>, beqa <beqaprogger@gmail.com>, Mesar Hameed <mhameed@src.gnome.org>, Alberto Buffolino <a.buffolino@gmail.com>

### Kill NVDA

* Compatible: Yes (updated on the website)
* Contact: Tyler Spivey <tspivey@pcdesk.net>

### Lambda Add-On for NVDA

* Compatible: Yes (not updated on the website)
* Contact: Alberto Zanella, Ivan Novegil

### Mozilla Apps Enhancements

* Compatible: Yes (updated on the website)
* Contact: Javi Dominguez <fjavids@gmail.com>

### mp3DirectCut

* Compatible: Yes (updated on the website)
* Contact: Abdel, Rémy Ruiz, Abdellah Zineddine, Jean-François Colas

### Newfon

* Compatible: No
* Contact: Sergey Shishmintzev

### NoBeepsSpeechMode

* Compatible: Yes (updated on the website)
* Note: new releases are compatible with 2021.1 only, older versions should be used for older NVDA releases
* Contact: Alberto Buffolino <a.buffolino@gmail.com>

### Notepad++

* Compatible: No (Up to 2019.3)
* Contact: Derek Riemer <driemer.riemer@gmail.com>

### Numpad Nav Mode

* Compatible: Yes (updated on the website)
* Contact: Luke Davis (Open Source Systems, Ltd.) <newanswertech@gmail.com>

### NVDA Remote Support

* Compatible: Yes (updated on the website)
* Contact: Tyler Spivey <tspivey@pcdesk.net>, Christopher Toth <q@q-continuum.net>

### NVDA Unmute

* Compatible: Yes (updated on the website)
* Contact: Oleksandr Gryshchenko <grisov.nvaccess@mailnull.com>

### NVDAUpdate Channel Selector

* Compatible: Yes (updated on the website)
* Contact: Jose Manuel Delicado <jm.delicado@nvda.es>

### NV Speech Player. 

* Compatible: No
* Contact: NV Access

### Object Location Tones

* Compatible: No (UP to 2019.3)
* Note: no longer maintained
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### ObjPad

* Compatible: Yes (updated on the website)
* Note: no longer maintained
* Contact: Joseph Lee <joseph.lee22590@gmail.com>

### OCR

* Compatible: Yes (updated on the website)
* Contact: Łukasz Golonka <lukasz.golonka@mailbox.org>

### Outlook Extended

* Compatible: Yes (updated on the website)
* Contact: Cyrille Bougot <cyrille.bougot2@laposte.net>, Ralf Kefferpuetz <ralf.kefferpuetz@elra-consulting.de>

### PC Keyboard Braille Input for NVDA

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>

### Phonetic Punctuation

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### placeMarkers

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>, Chris <llajta2012@gmail.com>

### Proxy support for NVDA

* Compatible: Yes (updated on the website)
* Contact: Jose Manuel Delicado <jm.delicado@nvda.es>

### Quick Dictionary

* Compatible: Yes (updated on the website)
* Contact: Oleksandr Gryshchenko <grisov.nvaccess@mailnull.com>

### Read Feeds

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>, Mesar Hameed

### Report Passwords

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>

### Report Symbols

* Compatible: Yes (updated on the website)
* Contact: Noelia Ruiz Martínez <nrm1977@gmail.com>

### Resource Monitor

* Compatible: Yes (updated on the website)
* Contact: Alex Hall <mehgcap@gmail.com>, Joseph Lee <joseph.lee22590@gmail.com>, beqa gozalishvili <beqaprogger@gmail.com>, Tuukka Ojala <tuukka.ojala@gmail.com>, Ethin Probst <harlydavidsen@gmail.com> and other NVDA contributors

### Review Cursor Copier

* Compatible: Yes (updated on the website)
* Contact: Tuukka Ojala <tuukka.ojala@gmail.com>

### sayCurrentKeyboardLanguage

* Compatible: Yes (updated on the website)
* Contact: Abdel <abdelkrim.bensaid@gmail.com>, Noelia <nrm1977@gmail.com>

### SentenceNav

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Speak Passwords

* Compatible: No
* Contact: Tyler Spivey <tspivey@pcdesk.net>

### Speech History

* Compatible: Yes (updated on the website)
* Contact: Tyler Spivey <tspivey@pcdesk.net>, James Scholes

### Station Playlist

* Compatible: Yes (updated on the website)
* Contact: Geoff Shang, Joseph Lee and other contributors

### Switch synth

* Compatible: Yes (updated on the website)
* Contact: Tyler Spivey <tspivey@pcdesk.net>

### Synth ring settings selector

* Compatible: No
* Contact: David CM <dhf360@gmail.com>

### systrayList

* Compatible: Yes (updated on the website)
* Contact: Rui Fontes <rui.fontes@tiflotecnia.com>, Rui Batista <ruiandrebatista@gmail.com>, Joseph Lee <joseph.lee22590@gmail.com>, NVDA Community Contributors

### TeamTalk Classic

* Compatible: Yes (not updated on the website)
* Contact: Doug Lee with initial work by Tyler Spivey and others

### Text Information

* Compatible: No
* Contact: Carter Temm <crtbraille@gmail.com>

### TextNav

* Compatible: Yes (coming soon to the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Time Zoner

* Compatible: No
* Contact: Munawar Bijani

### Tip of the Day

* Compatible: No
* Contact: Derek Riemer <driemer.riemer@gmail.com>

### Tony's enhancements

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### ToolbarsExplorer

* Compatible: Yes (updated on the website)
* Contact: Alberto Buffolino <a.buffolino@gmail.com>

### Tone Master

* Compatible: No
* Contact: Hrvoje Katić <hrvojekatic@gmail.com>

### Training Keyboard commands

* Compatible: Yes (updated on the website)
* Contact: Ibrahim Hamadeh <ibra.hamadeh@hotmail.com>

### UnicodeBrailleInput

* Compatible: Yes (updated on the website)
* Contact: Mesar Hameed <mhameed@src.gnome.org>, Patrick ZAJDA <patrick@zajda.fr>, Leonard de Ruijter (Babbage B.V.) <leonard@babbage.com>

### Virtual Review

* Compatible: Yes (updated on the website)
* Contact: Rui Batista <ruiandrebatista@gmail.com> and NVDA Addon Team

### VLC Media Player

* Compatible: Yes (updated on the website)
* Contact: Javi Dominguez <fjavids@gmail.com>

### Weather Plus

* Compatible: Yes (not updated on the website)
* Contact: Adriano Barbieri <adrianobarb@yahoo.it>

### Windows App Essentials

* Compatible: Yes (updated on the website)
* Contact: Joseph Lee <joseph.lee22590@gmail.com>, Derek Riemer <driemer.riemer@gmail.com> and others

### Windows Magnifier

* Compatible: Yes (updated on the website)
* Contact: Cyrille Bougot <cyrille.bougot2@laposte.net>

### Win Wizard

* Compatible: Yes (updated on the website)
* Contact: Oriol Gomez <ogomez.s92@gmail.com>, Łukasz Golonka <lukasz.golonka@mailbox.org>

### WordNav

* Compatible: Yes (updated on the website)
* Contact: Tony Malykh <anton.malykh@gmail.com>

### Zoom Accessibility Enhancements

* Compatible: Yes (updated on the website)
* Contact: Mohammad Suliman <mohmad.s93@gmail.com>, Eilana Benish <benish.ilana@gmail.com>

[[!tag announcements]]
