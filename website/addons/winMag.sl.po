# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-06-30 12:58+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Windows Magnifier\"]]\n"
msgstr ""

#. type: Bullet: '* '
msgid "Author: Cyrille Bougot"
msgstr ""

#. type: Bullet: '* '
msgid "NVDA compatibility: 2018.3 and beyond"
msgstr ""

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr ""

#. type: Bullet: '* '
msgid "Download [development version][2]"
msgstr ""

#. type: Plain text
msgid "This add-on improves the use of Windows Magnifier with NVDA."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Features"
msgstr ""

#. type: Bullet: '* '
msgid "Allows to report the result of some native Magnifier keyboard commands."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Allows to reduce the cases where table navigation commands conflict with "
"Magnifier's commands."
msgstr ""

#. type: Bullet: '* '
msgid "Adds some keyboard shortcuts to toggle various Magnifier options."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Settings"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The setting panel of Windows Magnifier add-on allows to configure how NVDA reacts to native Windows Magnifier commands.\n"
"You may want to have more or less commands reported according to what you are able to see.\n"
"This panel may be opened choosing Preferences -> Settings in the NVDA menu and then selecting the Windows Magnifier category in the Settings window.\n"
"The keyboard shortcut NVDA+Windows+O then O also allows to open this settings panel directly.\n"
msgstr ""

#. type: Plain text
msgid "The panel contains the following options:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Report view moves: controls what is reported when you move the view with "
"Control+Alt+Arrows commands. The three options are:"
msgstr ""

#. type: Bullet: '    * '
msgid "Off: Nothing is reported."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"With speech: a speech message indicates the position of the zoomed view on "
"the dimension the view is being moved."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"With tones: a tone is played and its pitch indicates the position of the "
"zoomed view on the dimension the view is being moved."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  This option only affects full view mode.\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Report turn on or off: If checked, the Magnifier's state is reported when "
"you use Windows++ or Windows+Escape commands to turn it on or off."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Report zoom: If checked, the Magnifier's zoom level is reported when you use "
"Windows++ or Windows+- zoom commands."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Report color inversion: If checked, the color inversion state is reported "
"when you use the control+Alt+I toggle command."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Report view change: If checked, the view type is reported when you use a "
"command that changes the view type (Control+Alt+M, Control+Alt+F, Control+Alt"
"+D, Control+Alt+L)"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Report lens or docked window resizing: If checked, a message is reported "
"when you use the resizing commands (Alt+Shift+Arrows).  In docked window "
"mode, the height or the width is reported.  In lens mode, the new dimension "
"cannot be reported for now.  These resizing command do not seem to be "
"available on all versions of Windows; if your Windows version does not "
"support them, you should keep this option unchecked."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In documents and list views, pass control+alt+arrows shortcuts to Windows "
"Magnifier: There are three possible choices:"
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Never: The command is not passed to Windows Magnifier and standard NVDA "
"table navigation can operate.  When used in documents out of a table, the "
"Control+Alt+Arrow command reports a \"Not in a table\" error message.  This "
"is the standard behaviour of NVDA without this add-on."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Only when not in table: In table or in list views, Control+Alt+Arrow "
"commands perform standard table navigation.  When used in documents out of a "
"table, Control+Alt+Arrow commands perform standard Magnifier view move "
"commands.  If you still want to move Windows Magnifier view while in table "
"or in list view, you will need to press NVDA+F2 before using Control+Alt"
"+Arrow commands.  This option is the best compromise if you want to use "
"Control+Alt+Arrow for both Magnifier and table navigation."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Always: Control+Alt+Arrow commands moves the Magnifier's view in any case.  "
"This option may be useful if you do not use Control+Alt+Arrow to navigate in "
"table, e.g. because you have changed table navigation shortcuts in NVDA or "
"because you exclusively use [Easy table navigator][5] add-on for table "
"navigation."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Commands added by this add-on"
msgstr ""

#. type: Plain text
msgid ""
"In addition to native Magnifier commands, this add-on provide additional "
"commands that allow to control Magnifier's options without opening its "
"configuration page.  All the commands added to control Magnifier options are "
"accessible through the Magnifier layer command NVDA+Windows+O:"
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then C: Toggles on or off caret tracking."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then F: Toggles on or off focus tracking."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then M: Toggles on or off mouse tracking."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then T: Toggles on or off tracking globally."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then S: Toggles on or off smoothing."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+O then R: Switches between mouse tracking modes (within the "
"edge of the screen or centered on the screen); this feature is only "
"available on Windows 10 build 17643 or higher."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+O then X: Switches between text cursor tracking modes (within "
"the edge of the screen or centered on the screen); this feature is only "
"available on Windows 10 build 18894 or higher."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA+Windows+O then V: Moves the mouse cursor in the center of the magnified "
"view (command available in full screen view only)."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then O: Opens Windows Magnifier add-on settings."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA+Windows+O then H: Displays help on Magnifier layer commands."
msgstr ""

#. type: Plain text
msgid ""
"There is no default direct gesture for each command, but you can attribute "
"one normally in the input gesture dialog if you wish.  The same way, You can "
"also modify or delete the Magnifier layer access gesture (NVDA+Windows+O).  "
"Yet, you cannot modify the shortcut key of the Magnifier layer sub-commands."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Magnifier's native commands"
msgstr ""

#. type: Plain text
msgid ""
"The result of the following Magnifier native commands may be reported by "
"this add-on, according to its configuration:"
msgstr ""

#. type: Bullet: '* '
msgid "Start Magnifier: Windows++ (on alpha-numeric keyboard or on numpad)"
msgstr ""

#. type: Bullet: '* '
msgid "Quit Magnifier: Windows+Escape"
msgstr ""

#. type: Bullet: '* '
msgid "Zoom in: Windows++ (on alpha-numeric keyboard or on numpad)"
msgstr ""

#. type: Bullet: '* '
msgid "Zoom out: Windows+- (on alpha-numeric keyboard or on numpad)"
msgstr ""

#. type: Bullet: '* '
msgid "Toggle color inversion: Control+Alt+I"
msgstr ""

#. type: Bullet: '* '
msgid "Select the docked view: Control+Alt+D"
msgstr ""

#. type: Bullet: '* '
msgid "Select the full screen view: Control+Alt+F"
msgstr ""

#. type: Bullet: '* '
msgid "Select the lens view: Control+Alt+L"
msgstr ""

#. type: Bullet: '* '
msgid "Cycle through the three view types: Control+Alt+M"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Resize the lens with the keyboard: Shift+Alt+Left/Right/Up/DownArrow.  Note: "
"although this does not seem to be documented, this shortcut seems to have "
"been withdrawn in recent Windows versions such as Windows 2004."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Move the magnified view: Control+Alt+Arrows (reporting only affects full "
"screen mode)"
msgstr ""

#. type: Plain text
msgid ""
"Here is also a list of other Magnifier native commands, just for information:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Control+Alt+mouseScrollWheel: Zooms in and out using the mouse scroll wheel."
msgstr ""

#. type: Bullet: '* '
msgid "Control+Windows+M: Opens the Magnifier's settings window."
msgstr ""

#. type: Bullet: '* '
msgid "Control+Alt+R: Resizes the lens with the mouse."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Control+Alt+Space: Quickly shows the entire desktop when using full screen "
"view."
msgstr ""

#. type: Plain text
msgid "None of the Magnifier native commands can be modified."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Notes"
msgstr ""

#. type: Bullet: '* '
msgid ""
"For computers equipped with an Intel graphic card, control+alt+arrow (left/"
"right/up/down) are also shortcuts to modify the orientation of the screen.  "
"These shortcut are enabled by default and conflict with Windows Magnifiers "
"shortcuts to move the view.  You will need to disable them to be able to use "
"them for the Magnifier.  They can be disabled in the Intel control panel or "
"in the Intel menu present in the system tray."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Depending on your Windows version, Alt+Shift+Arrow are Windows Magnifier "
"shortcuts to resize the magnified view (lens or docked).  When Magnifier is "
"active (even in full screen mode), these shortcuts are captured by Magnifier "
"and cannot be passed to the application, even if you press NVDA+F2 before.  "
"To use these shortcuts in the current application, you need to quit the "
"Magnifier (Windows+Escape) and re-open it after (Windows++).  For example in "
"MS word, to decrease title level:"
msgstr ""

#. type: Bullet: '    * '
msgid "Press Windows+Escape to quit Magnifier."
msgstr ""

#. type: Bullet: '    * '
msgid "Press Alt+Shift+RightArrow to decrease current title level."
msgstr ""

#. type: Bullet: '    * '
msgid "Press Windows++ to re-open the Magnifier."
msgstr ""

#. type: Bullet: '* '
msgid ""
"For more information about Windows Magnifier's features and shortcuts, you "
"may want to consult the following pages:"
msgstr ""

#. type: Bullet: '    * '
msgid ""
"[Use Magnifier to make things on the screen easier to see](https://support."
"microsoft.com/en-us/help/11542/windows-use-magnifier-to-make-things-easier-"
"to-see)"
msgstr ""

#. type: Bullet: '    * '
msgid "[Windows keyboard shortcuts for accessibility][4]"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Change log"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Version 1.0"
msgstr ""

#. type: Bullet: '* '
msgid "Initial release."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr ""

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=winmag"
msgstr ""

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=winmag-dev"
msgstr ""

#. type: Plain text
msgid "[4]: https://support.microsoft.com/en-us/help/13810"
msgstr ""

#. type: Plain text
msgid "[5]: https://addons.nvda-project.org/addons/easyTableNavigator.en.html"
msgstr ""
