# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-12-04 20:14+0000\n"
"PO-Revision-Date: 2020-01-05 12:46+0200\n"
"Last-Translator: Florian Ionașcu <florianionascu@hotmail.com>\n"
"Language-Team: \n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Control Usage Assistant\"]]\n"
msgstr "[[!meta title=\"Control Usage Assistant\"]]\n"

#. type: Bullet: '* '
msgid "Author: Joseph Lee"
msgstr "Autor: Joseph Lee"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Descărcați [versiunea stabilă][1]"

#. type: Bullet: '* '
#, fuzzy
#| msgid "NVDA compatibility: 2019.3 and beyond"
msgid "NVDA compatibility: 2021.2 and later"
msgstr "Compatibilitate NVDA: 2019.3 și mai nou"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Use this add-on to find out how to interact with the focused control.  "
#| "Press NvDA+H to obtain a short help message on interacting with the "
#| "focused control, such as checkboxes, edit fields and so on."
msgid ""
"Use this add-on to find out how to interact with the focused control.  Press "
"NVDA+H to obtain a short help message on interacting with the focused "
"control, such as checkboxes, edit fields and so on."
msgstr ""
"Folosiți acest supliment pentru a descoperi cum să interacționați cu "
"controlul focalizat.  Apăsați NVDA+H pentru a obține un mesaj scurt de "
"ajutor în interacționarea cu controlul focalizat, precum casete de bifat, "
"câmpuri de editare și așa mai departe."

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Version 20.01"
msgid "Version 21.10"
msgstr "Versiunea 20.01"

#. type: Bullet: '* '
msgid ""
"NVDA 2021.2 or later is required due to changes to NVDA that affects this "
"add-on."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Version 20.01"
msgid "Version 20.10"
msgstr "Versiunea 20.01"

#. type: Bullet: '* '
msgid "Help messages are announced in languages other than English."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Version 20.01"
msgid "Version 20.06"
msgstr "Versiunea 20.01"

#. type: Bullet: '* '
msgid "Resolved many coding style issues and potential bugs with Flake8."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will no longer appear to do nothing or play error tones when trying to "
"obtain help for certain browse mode controls."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Version 20.01"
msgstr "Versiunea 20.01"

#. type: Bullet: '* '
msgid "NVDA 2019.3 or later is required."
msgstr "Este necesar NVDA 2019.3 sau mai nou."

#. type: Title ##
#, no-wrap
msgid "Version 3.0/19.11"
msgstr "Versiunea 3.0/19.11"

#. type: Bullet: '* '
msgid "Version scheme is now year.month."
msgstr "Acum, schema versiunii este an.lună."

#. type: Bullet: '* '
msgid ""
"When NVDA+H is pressed, a help screen will be displayed instead of a flash "
"message being shown."
msgstr ""
"Când este apăsat NVDA+H, va fi afișat un ecram de ajutor în loc de un mesaj "
"flash."

#. type: Title ##
#, no-wrap
msgid "Version 2.5"
msgstr "Versiunea 2.5"

#. type: Bullet: '* '
msgid "Compatible with Python 3."
msgstr "Compatibil cu Python 3."

#. type: Title ##
#, no-wrap
msgid "Version 2.1"
msgstr "Versiunea 2.1"

#. type: Bullet: '* '
msgid "New and updated translations."
msgstr "Au fost incluse traduceri noi și actualizate cele existente."

#. type: Title ##
#, no-wrap
msgid "Version 2.0"
msgstr "Versiunea 2.0"

#. type: Bullet: '* '
msgid "Help messages for more controls added, including terminal windows."
msgstr ""
"Au fost adăugate mesaje de ajutor pentru multe controale, incluzând "
"ferestrele terminale."

#. type: Bullet: '* '
msgid ""
"Added help messages for working in some areas of applications, such as "
"Microsoft Excel and Powerpoint and Windows 8 start screen."
msgstr ""
"Au fost adăugate mesaje de ajutor pentru lucru în anumite domenii ale "
"aplicațiilor, cum ar fi Microsoft Excel, Powerpoint și Windows 8 start "
"screen."

#. type: Bullet: '* '
msgid ""
"Added help messages for working with forms in both browse and focus modes in "
"virtual buffer documents (Internet Explorer, Adobe Reader, Mozilla Firefox, "
"etc.)."
msgstr ""
"Au fost adăugate mesaje de ajutor pentru lucru cu formulare în ambele moduri "
"de focalizare și navigare în documentele buffer-ului virtual (Internet "
"Explorer, Adobe Reader, Mozilla Firefox, etc.)."

#. type: Bullet: '* '
msgid "New language: Danish."
msgstr "Limbă nouă: Daneză."

#. type: Title ##
#, no-wrap
msgid "Version 1.0"
msgstr "Versiunea 1.0"

#. type: Bullet: '* '
msgid "Initial version."
msgstr "Versiunea inițială."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=cua"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=cua"

#~ msgid "[2]: https://addons.nvda-project.org/files/get.php?file=cua-dev"
#~ msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=cua-dev"

#~ msgid ""
#~ "Download [older version][3] compatible with NVDA 2019.2.1 and earlier"
#~ msgstr ""
#~ "Descărcați [versiunea mai veche][3], compatibilă cu NVDA 2019.2.1 și cu "
#~ "versiunile mai vechi ale acestuia"

#~ msgid "[3]: https://addons.nvda-project.org/files/get.php?file=cua-2019"
#~ msgstr "[3]: https://addons.nvda-project.org/files/get.php?file=cua-2019"

#~ msgid "Download [development version][2]"
#~ msgstr "Descărcați [versiunea în dezvoltare][2]"

#~ msgid "2.1"
#~ msgstr "2.1"

#~ msgid "2.0"
#~ msgstr "2.0"

#~ msgid "1.0"
#~ msgstr "1.0"
