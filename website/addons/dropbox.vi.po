# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-07-29 12:59+0000\n"
"PO-Revision-Date: 2020-02-11 14:39+0700\n"
"Last-Translator: Dang Manh Cuong <dangmanhcuong@gmail.com>\n"
"Language-Team: \n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"dropbox\"]]\n"
msgstr "[[!meta title=\"dropbox\"]]\n"

#. type: Bullet: '* '
msgid ""
"Authors: Patrick ZAJDA <patrick@zajda.fr>, Filaos and other contributors"
msgstr ""
"Tác giả: Patrick ZAJDA <patrick@zajda.fr>, Filaos và các cộng tác viên khác"

#. type: Bullet: '* '
#, fuzzy
#| msgid "NVDA compatibility: from 2019.1 to 2019.3 or later"
msgid "NVDA compatibility: NVDA 2019.1 or later"
msgstr "NVDA tương thích: từ 2019.1 đến 2019.3 hay cao hơn"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Tải về [phiên bản chính thức][1]"

#. type: Bullet: '* '
msgid "Download [development version][2]"
msgstr "tải về [phiên bản thử nghiệm][2]"

#. type: Plain text
msgid ""
"This plugin add a shortcut to announce Dropbox status or open the Dropbox "
"systray menu when pressed once or twice respectively.  It also enhances "
"DropBox item lists."
msgstr ""
"Plugin này thêm một phím tắt để thông báo trạng thái của Dropbox hoặc mở "
"trình đơn của nó từ khay hệ thống khi bấm một hay hai lần.  Nó cũng cải "
"thiện các thành phần trong danh sách của DropBox."

#. type: Bullet: '* '
msgid "Shortcut: NVDA+Alt+D"
msgstr "Phím tắt: NVDA+Alt+D"

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 4.4"
msgid "Changes for 4.6"
msgstr "Các thay đổi cho 4.4"

#. type: Bullet: '* '
msgid "Specify NVDA 2021.1 compatibility"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Changes for 4.4"
msgstr "Các thay đổi cho 4.4"

#. type: Bullet: '* '
msgid "Python 3 compatibility"
msgstr "Đã tương thích với Python 3"

#. type: Bullet: '* '
msgid "Use the last addon template"
msgstr "Dùng mẫu addon cuối cùng"

#. type: Bullet: '* '
msgid "Repository change to be built with Appveyor"
msgstr "thay đổi kho dữ liệu để dựng với Appveyor"

#. type: Bullet: '* '
msgid "Fixed wrong and removed unused shortcuts in the documentation"
msgstr "Sửa lỗi và gỡ bỏ các lệnh không dùng đến trong tài liệu hướng dẫn"

#. type: Bullet: '* '
msgid ""
"Update the description in the documentation which still referenced the "
"announcement of the version"
msgstr ""
"Cập nhật mô tả trong tài liệu hướng dẫn mà vẫn tham khảo thông báo của phiên "
"bản"

#. type: Title ##
#, no-wrap
msgid "Changes for 4.0"
msgstr "Các thay đổi cho 4.0"

#. type: Bullet: '* '
msgid "Add-on help is available from the Add-ons Manager."
msgstr "Đã có trợ giúp cho add-on từ trình quản lý Add-on."

#. type: Bullet: '* '
msgid ""
"The shortcut to get Dropbox status has been changed to Alt+NVDA+D to avoid "
"conflict with audio ducking support."
msgstr ""
"Phím tắt xem trạng thái của Dropbox đã thay đổi thành Alt+NVDA+D để tránh "
"xung đột với lệnh giảm âm thanh."

#. type: Title ##
#, no-wrap
msgid "Changes for 3.1"
msgstr "Các thay đổi cho 3.1"

#. type: Bullet: '* '
msgid ""
"Use another way to get cancel button and page tab. Now we don't have to "
"focus them before using shortcuts."
msgstr ""
"Dùng cách khác để nhận biết nút cancel và các thẻ. giờ chúng ta không cần "
"phải c ó focus trước khi dùng phím tắt."

#. type: Bullet: '* '
msgid ""
"When changing the active tab, the focus move to the tab page so when "
"pressing tab, the first item of the tab is activated instead of staying to "
"the previous used tab even if it is not activated anymore."
msgstr ""
"Khi chuyển sang thẻ khác, con trỏ di chuyển đến danh sách các thẻ nên khi "
"bấm tab, thành phần đầu tiên của thẻ được kích hoạt thay vì đứng ở thành "
"phần của thẻ đã dùng trước đó, thậm chí khi nó không còn được kích hoạt nữa."

#. type: Bullet: '* '
msgid ""
"In the preferences dialog, it is now possible to press control+page up/down "
"to switch between tabs. Control+tab and control+shift+tab still work."
msgstr ""
"Trong hộp thoại preferences, giờ đây đã có thể bấm control+page up/down để "
"chuyển giữa các thẻ. Control+tab và control+shift+tab vẫn hoạt động."

#. type: Bullet: '* '
msgid "All localized manifest files should now be OK."
msgstr "Tất cả các tập tin manifest được bản địa hóa giờ đây hoạt động tốt."

#. type: Bullet: '* '
msgid "Minor corrections."
msgstr "Các sửa lỗi phụ."

#. type: Title ##
#, no-wrap
msgid "Changes for 3.0"
msgstr "Các thay đổi cho 3.0"

#. type: Bullet: '* '
msgid ""
"Minor correction in the main manifest file (authors are correctly displayed)."
msgstr ""
"Vài chỉnh sửa trong tập tin manifest chính (hiển thị đúng tên tác giả)."

#. type: Bullet: '* '
msgid "Improved context menu detection when pressing Shift+NVDA+D three times."
msgstr ""
"Cải thiện khả năng nhận dạng trình đơn ngữ cảnh khi bấm  Shift+NVDA+D ba lần."

#. type: Bullet: '* '
msgid ""
"The escape button now works (only when using Dropbox in the same language "
"NVDA uses)."
msgstr ""
"Nút escape giờ đã hoạt động (chỉ khi dùng Dropbox bằng ngôn ngữ giống với "
"ngôn ngữ của NVDA)."

#. type: Bullet: '* '
msgid "A lot of corrections in the code."
msgstr "Nhiều chỉnh sửa trong mã nguồn."

#. type: Bullet: '* '
msgid "Added/updated documentations of all scripts."
msgstr "Thêm / cập nhật tài liệu cho tất cả kịch bản."

#. type: Bullet: '* '
msgid ""
"New languages: Arabic, Brazilian Portuguese, Czech, Dutch, Finnish, "
"Galician, German, Hungarian, Japanese, Nepali, Polish, Russian, Spanish, "
"Slovak, Tamil, Turkish."
msgstr ""
"Các ngôn ngữ mới: Arabic, Brazilian Portuguese, Czech, Dutch, Finnish, "
"Galician, German, Hungarian, Japanese, Nepali, Polish, Russian, Spanish, "
"Slovak, Tamil, Turkish."

#. type: Title ##
#, no-wrap
msgid "Changes for 2.0"
msgstr "Các thay đổi cho 2.0"

#. type: Bullet: '* '
msgid "New languages: Italian"
msgstr "Ngôn ngữ mới: tiếng Ý"

#. type: Bullet: '* '
msgid ""
"Pressing the shortcut three times or more when already being in the context "
"menu doesn't cause problem anymore."
msgstr ""
"Bấm phím tắt ba lần trở lên khi đã đứng ở trình đơn ngữ cảnh không còn gây "
"ra lỗi nữa."

#. type: Title ##
#, no-wrap
msgid "Changes for 1.0"
msgstr "Các thay đổi cho 1.0"

#. type: Bullet: '* '
msgid "Initial Release"
msgstr "Phiên bản đầu tiên"

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=dx"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=dx"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=dx-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=dx-dev"

#~ msgid "Ctrl+Alt+T announce the active tab."
#~ msgstr "Ctrl+Alt+T thông báo thẻ đang hoạt động."

#~ msgid "Known issues"
#~ msgstr "Các vấn đề còn tồn tại"

#~ msgid ""
#~ "* If you switch between tabs using the shortcuts, when you'll close the "
#~ "preferences window, NVDA won't be able to know the windows doesn't exist "
#~ "anymore.\n"
#~ "It is a known issue on NVDA and cannot be fixed.\n"
#~ msgstr ""
#~ "* Nếu chuyển giữa các thẻ bằng phím tắt, khi đóng cửa sổ preferences, "
#~ "NVDA không nhận biết được rằng cửa sổ đó không còn tồn tại nữa.\n"
#~ "Đây là lỗi của NVDA và không thể khắc phục.\n"
