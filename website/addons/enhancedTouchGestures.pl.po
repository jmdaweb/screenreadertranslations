# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Enhanced Touch gestures\n"
"POT-Creation-Date: 2021-12-04 20:14+0000\n"
"PO-Revision-Date: 2020-12-09 10:13+0100\n"
"Last-Translator: Zvonimir stanecic <zvonimirek222@yandex.com>\n"
"Language-Team: polish <killer@tyflonet.com>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Enhanced Touch Gestures\"]]\n"
msgstr "[[!meta title=\"Enhanced Touch Gestures\"]]\n"

#. type: Bullet: '* '
msgid "Author: Joseph Lee"
msgstr "Autor: Joseph Lee"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Pobierz [wersja stabilna][1]"

#. type: Bullet: '* '
#, fuzzy
#| msgid "NVDA compatibility: 2020.1 to 2020.3"
msgid "NVDA compatibility: 2021.2 and later"
msgstr "Zgodność z wersjami NVDA: 2020.1 do 2020.3"

#. type: Plain text
msgid ""
"This add-on provides additional touchscreen gestures for NVDA. It also "
"provides a set of gestures for easier browse mode navigation."
msgstr ""
"Ten dodatek udostępnia gesty dotykowe dla NVDA oraz zestaw specjalnych "
"gestów do łatwiejszej nawigacji w trybie czytania."

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Note: this add-on requires NVDA 2019.3 or later running on a touchscreen "
#| "computer with Windows 8.1 or 10."
msgid ""
"Note: this add-on requires NVDA 2021.2 or later running on a touchscreen "
"computer with Windows 8.1, 10 or 11."
msgstr ""
"Uwaga! Ten dodatek wymaga NVDA w wersji 2019.3 lub nowszej, uruchomionej na "
"komputerze z ekranem dotykowym i systemem operacyjnym Windows 8.1 lub 10."

#. type: Title ##
#, no-wrap
msgid "Commands"
msgstr "Polecenia"

#. type: Title ###
#, no-wrap
msgid "Available everywhere"
msgstr "Dostępne wszędzie"

#. type: Bullet: '* '
msgid "4 finger double tap: toggle input help mode."
msgstr "Podwójne stuknięcie czterema palcami: przełącza pomoc wprowadzania."

#. type: Bullet: '* '
msgid "Four finger flick right: toggle touch keyboard (usually enables it)."
msgstr ""
"Przesunięcie w prawo czterema palcami: Przełączanie klawiatury dotykowej (za "
"zwyczaj ją włącza)."

#. type: Bullet: '* '
msgid ""
"Four finger flick left: toggle dictation (Windows+H; Windows 10 Version 1709 "
"or later)."
msgstr ""

#. type: Title ###
#, no-wrap
msgid "Object mode"
msgstr "Tryb obiektu"

#. type: Bullet: '* '
msgid "3 finger flick down: read current window."
msgstr "Przesunięcie w dół trzema palcami: odczytanie bieżącego okna."

#. type: Bullet: '* '
msgid "3 finger flick left: report object with focus."
msgstr ""
"Przesunięcie w lewo trzema palcami: odczytaj obiekt posiadający fokus.."

#. type: Bullet: '* '
msgid "3 finger flick right: report current navigator object."
msgstr ""
"Przesunięcie w prawo trzema palcami: odczytaj bieżący obiekt nawigacyjny."

#. type: Bullet: '* '
msgid "4 finger flick up: report title of the current window."
msgstr "Przesunięcie w górę czterema palcami: odczytaj tytuł bieżącego okna."

#. type: Bullet: '* '
msgid "4 finger flick down: report status bar text."
msgstr "Przesunięcie w dół czterema palcami: odczytaj tekst paska stanu."

#. type: Title ##
#, no-wrap
msgid "Web touch mode"
msgstr "dotykowy tryb czytania"

#. type: Plain text
msgid ""
"This touch mode, available in browse mode, allows you to navigate the "
"document by selected element. To switch to web mode, from browse mode "
"documents, perform 3 finger tap. From this mode, flicking up or down with "
"one finger cycles through available element navigation modes, while flicking "
"right or left with one finger moves to next or previous chosen element, "
"respectively. Once you move away from browse mode documents, object touch "
"mode is used."
msgstr ""
"Ten tryb gestów, dostępny w trybie czytania, pozwala nawigować po wybranych "
"elementach dokumentu. Aby przełączyć się w ten tryb z dokumentów trybu "
"czytania, wykonaj stuknięcie trzema palcami. W tym trybie, Przesunięcie w "
"górę lub w dół jednym palcem przełącza między dostępnymi sposobami "
"nawigacji, Przesunięcie jednym palcem w lewo lub prawo przenosi do "
"poprzedniego lub następnego wybranego elementu. Po wyjściu z dokumentu trybu "
"czytania, używany jest obiektowy tryb gestów dotykowych."

#. type: Title ##
#, no-wrap
msgid "Synth settings touch mode"
msgstr "Tryb dotykowej zmiany ustawień syntezatora"

#. type: Plain text
msgid ""
"You can use this mode to quickly change synthesizer settings such as "
"choosing a voice and changing volume. In this mode, use two finger flick "
"left or right to move between synth settings and use two finger flick up and "
"down gestures to change values. This gestures mirrors that of synth settings "
"ring commands on the keyboard."
msgstr ""
"Możesz użyć tego trybu do szybkiej zmiany ustawień syntezatora, np. by "
"wybrać głos, albo zmienić głośność. Przesuń dwoma palcami w lewo lub prawo "
"aby przechodzić między ustawieniami. Przesuń dwoma palcami w górę lub w dół "
"aby zmieniać wartości danego ustawienia. Gesty te odpowiadają klawiszom "
"szybkiej zmiany ustawień syntezatora."

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Version 20.01"
msgid "Version 21.10"
msgstr "Wersja 20.01"

#. type: Bullet: '* '
msgid ""
"NVDA 2021.2 or later is required due to changes to NVDA that affects this "
"add-on."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Version 20.01"
msgid "Version 21.08"
msgstr "Wersja 20.01"

#. type: Bullet: '* '
msgid "Initial support for Windows 11."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Version 20.01"
msgid "Version 21.01"
msgstr "Wersja 20.01"

#. type: Bullet: '* '
#, fuzzy
#| msgid "NVDA 2019.3 or later is required."
msgid "NVDA 2020.3 or later is required."
msgstr "Wymagana jest wersja NVDA 2019.3 lub nowsza."

#. type: Bullet: '* '
msgid ""
"On Windows 10 Version 1709 and later, doing a four finger flick left will "
"toggle dictation (Windows+H)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Remove dedicated touch interaction support toggle command from the add-on."
msgstr ""

#. type: Bullet: '* '
msgid ""
"As touch interaction support can be toggled from NVDA's touch interaction "
"settings panel, a dedicated Enhanced Touch Gestures settings panel has been "
"removed."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Version 20.09"
msgstr "Wersja 20.04"

#. type: Bullet: '* '
msgid ""
"Removed ability to let NVDA turn off touch interaction for up to ten seconds "
"(touch command passthrough)."
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid "Coordinate announcement beep"
msgid "Removed coordinate announcement beep feature."
msgstr "Dźwięk oznajmiania położenia"

#. type: Title ##
#, no-wrap
msgid "Version 20.07"
msgstr "Wersja 20.07"

#. type: Bullet: '* '
msgid ""
"Added a keyboard command to toggle touch interaction or enable/disable touch "
"passthrough (Control+Alt+NVDA+T)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"As NVDA 2020.1 and later includes a touch command to perform right mouse "
"click (one finger tap and hold), the command has been removed from this add-"
"on. AS a result, NVDA 2020.1 or later is required."
msgstr ""

#. type: Bullet: '* '
msgid ""
"The ability to let NVDA turn off touch interaction for up to ten seconds "
"(touch command passthrough) is deprecated. In the future this feature will "
"toggle touch interaction instead."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In NVDA development snapshots, due to touch interaction feature changes, "
"touch command passthrough feature and Enhanced Touch Gestures settings panel "
"will be disabled. The command used to enable touch command passthrough will "
"toggle touch interaction instead."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Coordinate announcement beep feature is deprecated and will be removed in a "
"future add-on release."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Coordinate announcement beep will not be heard while using touch keyboard."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will no longer appear to do nothing or play error tones while exploring "
"modern input facility such as emoji panel via touch."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will present an error message if touch keyboard cannot be activated "
"(four finger flick right)."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Version 20.06"
msgstr "Wersja 20.06"

#. type: Bullet: '* '
msgid "Resolved many coding style issues and potential bugs with Flake8."
msgstr ""
"Naprawiono niektóre błędy stylistyczne związane z kodem,  a także naprawiono "
"błędy związane z linterem Flake8."

#. type: Title ##
#, no-wrap
msgid "Version 20.04"
msgstr "Wersja 20.04"

#. type: Bullet: '* '
msgid ""
"Right mouse click gesture (one finger tap and hold) is now part of NVDA "
"2020.1."
msgstr ""
"Polecenie prawego przycisku myszy (stuknięcie z przytrzymaniem jednym "
"palcem) jest wbudowane w NVDA w wersji 2020.1."

#. type: Title ##
#, no-wrap
msgid "Version 20.01"
msgstr "Wersja 20.01"

#. type: Bullet: '* '
msgid "NVDA 2019.3 or later is required."
msgstr "Wymagana jest wersja NVDA 2019.3 lub nowsza."

#. type: Bullet: '* '
msgid ""
"Touch support toggle command (including touch passthrough) will no longer "
"function if touch support is turned off completely."
msgstr ""
"Polecenie do przełączania trybu dotykowego (włączając w to dotykowe "
"przełączanie) nie będzie więcej funkcjonowało, gdy wsparcie dotykowe jest "
"kompletnie wyłączone."

#. type: Title ##
#, no-wrap
msgid "Version 19.11"
msgstr "Wersja 19.11"

#. type: Bullet: '* '
msgid "Added input help messages for additional touch commands."
msgstr "Dodano komunikaty pomocy dla dodatkowych poleceń dotykowych."

#. type: Title ##
#, no-wrap
msgid "Version 19.09"
msgstr "Wersja 19.09"

#. type: Bullet: '* '
msgid ""
"Touch support can now be disabled from everywhere, not just from profiles "
"other than normal profile."
msgstr ""
"Wsparcie dotykowe może być wyłączone z jakiegokolwiek miejsca, czyli nie "
"tylko przy aktywnym normalnym profilu."

#. type: Title ##
#, no-wrap
msgid "Version 19.07"
msgstr "Wersja 19.07"

#. type: Bullet: '* '
msgid "Internal changes to support future NVDA releases."
msgstr "Zmiany wewnętrzne dla wsparcia nowszych wersji NVDA."

#. type: Title ##
#, no-wrap
msgid "Version 18.12"
msgstr "Wersja 18.12"

#. type: Title ##
#, no-wrap
msgid "Version 18.08"
msgstr "Wersja 18.08"

#. type: Bullet: '* '
msgid "Compatible with NVDA 2018.3 and future versions."
msgstr "Zgodne z NVDA 2018.3 i nowszymi."

#. type: Title ##
#, no-wrap
msgid "Version 18.06"
msgstr "Wersja 18.06"

#. type: Bullet: '* '
msgid ""
"Add-on settings is now found in new multi-category NVDA Settings screen "
"under \"Enhanced Touch Gestures\" category. As a result, NVDA 2018.2 is "
"required."
msgstr ""
"Od teraz ustawienia dodatku znajdują się w nowym, wielopanelowym oknie "
"ustawień NVDA, w kategorii \"Enhanced Touch Gestures\" . Z powodu licznych "
"zmian, wymagana jest wersja NVDA 2018.2."

#. type: Bullet: '* '
msgid "Fixed compatibility issues with wxPython 4."
msgstr "Naprawiono problemy zgodności z wxPython 4."

#. type: Title ##
#, no-wrap
msgid "Version 18.04"
msgstr "Wersja 18.04"

#. type: Bullet: '* '
msgid ""
"Resolves an issue where touch interaction category in NVDA Settings panel "
"may cause error sounds to be heard due to changes made from this add-on."
msgstr ""
"Rozwiązuje problem, przez który kategoria reakcji na dotyk w panelu ustawień "
"NVDA może wywoływać dźwięki błędu, z powodu zmian wprowadzonych w tym "
"dodatku."

#. type: Title ##
#, no-wrap
msgid "Version 18.03"
msgstr "Wersja 18.03"

#. type: Bullet: '* '
msgid "NVDA 2018.1 is required."
msgstr "Wymaga NVDA 2018.1."

#. type: Bullet: '* '
msgid ""
"Because NVDA 2018.1 comes with touch typing checkbox, the checkbox is no "
"longer included in this add-on."
msgstr ""
"NVDA 2018.1 posiada już pole wyboru, które włącza wpisywanie dotykowe, "
"dlatego zostało ono usunięte z dodatku."

#. type: Title ##
#, no-wrap
msgid "Version 17.12"
msgstr "Wersja 17.12"

#. type: Bullet: '* '
msgid ""
"Requires NVDA 2017.4. Specifically, this add-on can now handle configuration "
"profile switches."
msgstr ""
"Wymaga NVDA 2017.4. Przede wszystkim, dodatek może już przełączać się między "
"profilami."

#. type: Bullet: '* '
msgid ""
"As NVDA 2017.4 includes screen orientation announcement, this feature is no "
"longer part of this add-on."
msgstr ""
"Ponieważ NVDA 2017.4 może ogłaszać orientację ekranu, usunięto tę funkcję z "
"dodatku."

#. type: Bullet: '* '
msgid ""
"Added a hidden checkbox in Touch Interaction dialog to completely disable "
"touch support (available if profiles other than normal configuration is "
"active)."
msgstr ""
"Aby zupełnie wyłączyć wsparcie dotyku, Do dialogu reakcji na dotyk dodano "
"ukryte pole wyboru, dostępne tylko wtedy gdy aktywny jest inny profil niż "
"standardowy."

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "If using NVDA 2018.1 or later, Touch Interaction dialog will be listed "
#| "twice under NvDA's preferences menu. The second item is the dialog that "
#| "comes with the add-on."
msgid ""
"If using NVDA 2018.1 or later, Touch Interaction dialog will be listed twice "
"under NVDA's preferences menu. The second item is the dialog that comes with "
"the add-on."
msgstr ""
"W NVDA 2018.1 i nowszych, dialog reakcji na dotyk pojawi się dwukrotnie w "
"meni ustawień NvDA. Drugi dialog jest oryginalną częścią dodatku."

#. type: Bullet: '* '
msgid ""
"In Touch Interaction dialog for the add-on, touch typing mode is no longer "
"shown if using NVDA 2018.1 or later."
msgstr ""
"W oknie dialogowym reakcji dotykowej dodatku, od wersji NVDA 2018.1 tryb "
"wpisywania dotykowego nie jest już pokazywany."

#. type: Title ##
#, no-wrap
msgid "Version 17.10"
msgstr "Wersja 17.10"

#. type: Bullet: '* '
msgid ""
"Due to support policy from Microsoft, Windows 8 (original release) is no "
"longer supported."
msgstr ""
"W związku z polityką wsparcia firmy Microsoft, Windows 8 nie jest już "
"wspierany."

#. type: Bullet: '* '
msgid ""
"NVDA will no longer announce screen orientation twice when running NVDA "
"2017.4 development snapshots."
msgstr ""
"NVDA nie będzie już dwukrotnie ogłaszać orientacji ekranu w wersjach "
"rozwojowych  NVDA 2017.4."

#. type: Title ##
#, no-wrap
msgid "Version 17.07.1"
msgstr "Wersja 17.07.1"

#. type: Bullet: '* '
msgid ""
"Added an option in touch interaction dialog to manually toggle touch "
"passthrough without use of a timer."
msgstr ""
"Do dialogu reakcji na dotyk dodano opcję służącą do ręcznego przełączania "
"przepuszczania dotyku bez potrzeby użycia timera."

#. type: Bullet: '* '
msgid ""
"With manual passthrough mode off, if touch passthrough is turned on before "
"the passthrough duration expires, touch interaction would be enabled."
msgstr ""
"Jeśli ręczny tryb przepuszczania jest wyłączony a przepuszczanie dotyku "
"zostało włączone zanim wygasł jego czas, włączy się reakcja na dotyk."

#. type: Title ##
#, no-wrap
msgid "Version 17.07"
msgstr "Wersja 17.07"

#. type: Bullet: '* '
msgid ""
"Added a new dialog named Touch Interaction under NVDA's preferences menu to "
"configure how NVDA works with touchscreens."
msgstr ""
"Do Meni Ustawień NVDA dodano okno dialogowe reakcji na dotyk. Służy ono do "
"konfigurowania sposobu w jaki NVDA współpracuje z ekranami dotykowymi."

#. type: Bullet: '* '
msgid ""
"After installing this version, when pressing keys on the touch keyboard, one "
"must double tap the desired key. You can switch back to the old way by "
"enabling touch typing from Touch Interaction dialog."
msgstr ""
"Po instalacji tej wersji dodatku trzeba dwukrotnie stuknąć w żądany klawisz "
"na klawiaturze dotykowej. Do poprzedniego sposobu można powrócić włączając "
"wpisywanie dotykowe w oknie reakcji na dotyk."

#. type: Bullet: '* '
msgid ""
"Added a command (unassigned) to allow NVDA to ignore touch gestures for up "
"to 10 seconds."
msgstr ""
"Dodano funkcję nieprzypisanego polecenia, dzięki której NVDA ignoruje gesty "
"dotykowe przez czas do 10 sekund."

#. type: Bullet: '* '
msgid ""
"Added an option in Touch Interaction dialog to allow NVDA to pause touch "
"interaction between 3 to 10 seconds in order to perform touchscreen gestures "
"directly (as though NVDA is not running; default is 5 seconds)."
msgstr ""
"Do okna dialogowego reakcji na dotyk dodano opcję, która pozwala NVDA "
"wstrzymać reakcję na dotyk od 3 do 10 sekund. Dzięki temu można wykonywać "
"gesty na ekranie dotykowym jakgdyby screenreader w ogóle nie był "
"uruchomiony. Domyślna wartość wynosi 5 sekund."

#. type: Bullet: '* '
msgid ""
"Added debug logging messages when performing right clicks (tap and hold) to "
"be recorded in the NVDA log (requires NVDA 2017.1 or later)."
msgstr ""
"Dodano zapisywanie wiadomości debugowania w dzienniku NVDA podczas "
"wykonywania prawego kliknięcia (stuknięcie i przytrzymanie). Wymagana wersja "
"NVDA 2017.1 lub nowsza."

#. type: Bullet: '* '
msgid ""
"Due to changes made when playing screen coordinates, NVDA 2017.1 or later is "
"required."
msgstr ""
"W związku ze zmianami w odtwarzaniu współrzędnych ekranu, wymagana jest "
"wersja NVDA 2017.1 lub nowsza."

#. type: Plain text
#, fuzzy
#| msgid "##Version 17.03\n"
msgid "##Version 17.03"
msgstr "##Wersja 17.03\n"

#. type: Bullet: '* '
msgid ""
"Fixed an issue where coordinate announcement beep did not play or an error "
"tone played instead when using NVDA 2017.1 or later."
msgstr ""
"Poprawiono błąd, z powodu którego dźwięk oznajmiania położenia nie był "
"odtwarzany lub zamiast niego pojawiał się dźwięk błędu. Działo się to w NVDA "
"2017.1 i nowszych."

#. type: Plain text
#, fuzzy
#| msgid "##Version 16.12\n"
msgid "##Version 16.12"
msgstr "##Wersja 16.12\n"

#. type: Bullet: '* '
msgid ""
"Web touch mode works in Microsoft Edge, Microsoft Word and others where "
"browse mode is used."
msgstr ""
"Dotykowy tryb czytania działa w Microsoft Edge, Microsoft Word i innych "
"programach, gdzie używa się trybu przeglądu."

#. type: Bullet: '* '
msgid "Added lists and landmarks to web touch mode elements."
msgstr ""
"Do elementów dotykowego trybu czytania dodano listy i punkty orientacyjne."

#. type: Title ##
#, no-wrap
msgid "Version 16.06"
msgstr "Wersja 16.06"

#. type: Bullet: '* '
msgid "Initial stable version."
msgstr "Pierwsza wersja stabilna."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=ets"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=ets"

#~ msgid "[2]: https://addons.nvda-project.org/files/get.php?file=ets-dev"
#~ msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=ets-dev"

#~ msgid ""
#~ "If you've enabled play mouse coordinates setting in mouse settings, "
#~ "you'll hear beeps to indicate current screen coordinate when you invoke "
#~ "touch exploration gestures."
#~ msgstr ""
#~ "Jeśli włączony jest dźwięk wskaźnika myszy,  będziesz słyszał dźwięki "
#~ "określające aktualne położenie na ekranie, po wywołaniu gestu dotykowej "
#~ "eksploracji."

#, fuzzy
#~| msgid "Touch command passthrough"
#~ msgid "Touch interaction toggle/passthrough"
#~ msgstr "Przepuszczanie polecenia dotykowego"

#, fuzzy
#~| msgid ""
#~| "An unassigned command is available to allow you to use touchscreen "
#~| "gestures as though NVDA is not running. In order to use this, you need "
#~| "to assign a command (via Input Gestures dialog) under Enhanced Touch "
#~| "Gestures category to let you do this for up to ten seconds or toggle "
#~| "this manually. Then go to NVDA menu/Preferences/Touch Interaction, then "
#~| "configure pause NVDA's touch command value between 3 to 10 seconds "
#~| "(default is 5 seconds)."
#~ msgid ""
#~ "An unassigned command is available to allow you to toggle touch "
#~ "interaction or temporarily use touchscreen gestures (touch command "
#~ "passthrough) as though NVDA is not running. In order to use this, you "
#~ "need to assign a command (via Input Gestures dialog) under Enhanced Touch "
#~ "Gestures category to let you do this for up to ten seconds or toggle this "
#~ "manually. Then go to NVDA menu/Preferences/Touch Interaction, then "
#~ "configure pause NVDA's touch command value between 3 to 10 seconds "
#~ "(default is 5 seconds)."
#~ msgstr ""
#~ "Nieprzypisane polecenie pozwala używać gestów ekranu tak, jakby NVDA nie "
#~ "był uruchomiony. Aby użyć tej funkcji, przypisz je w kategorii Enhanced "
#~ "Touch Gestures przez dialog zdarzeń wejścia. Pozwoli to wstrzymać reakcję "
#~ "na dotyk do dziesięciu sekund lub przełączyć ją ręcznie. Następnie należy "
#~ "wejść do meni NVDA/Ustawienia/Reakcja na Dotyk, i skonfigurować czas "
#~ "wstrzymania poleceń dotykowych NVDA od 3 do 10 sekund. Wartość domyślna "
#~ "to 5 sekund."

#~ msgid ""
#~ "Tap and hold: performs right click at the object under your finger (now "
#~ "part of NVDA 2020.1 and later)."
#~ msgstr ""
#~ "Stuknięcie z przytrzymaniem: wykonuje kliknięcie prawym przyciskiem myszy "
#~ "na obiekcie pod palcem (wbudowane w NVDA 2020.1 i nowsze)."

#~ msgid ""
#~ "Download [older version][3] compatible with NVDA 2019.2.1 and earlier"
#~ msgstr "Pobierz [starszą wersję][3] zgodna z NVDA 2019.2.1 i starszymi"

#~ msgid "[3]: https://addons.nvda-project.org/files/get.php?file=ets-2019"
#~ msgstr "[3]: https://addons.nvda-project.org/files/get.php?file=ets-2019"

#, fuzzy
#~| msgid "Download [stable version][1]"
#~ msgid "Download [development version][2]"
#~ msgstr "Pobierz [wersja stabilna][1]"

#~ msgid "Disable touch support in profiles"
#~ msgstr "Wyłączanie wsparcia dotyku w profilach"

#~ msgid ""
#~ "If profiles other than normal configuration is active and if you go to "
#~ "Touch Interaction dialog, you'll see a checkbox named \"completely "
#~ "disable touch support\". Checking this box and answering yes if prompted "
#~ "will completely turn off touch support for that profile. This is useful "
#~ "in apps that provide their own touch commands. To restore touch "
#~ "functionality, either uncheck this checkbox or manually toggle touch "
#~ "passthrough."
#~ msgstr ""
#~ "Jeżeli aktywny jest inny profil niż standardowy, po wejściu do dialogu "
#~ "reakcji na dotyk zobaczysz pole wyboru o nazwie \"Wyłącz wsparcie dotyku"
#~ "\". Zaznaczenie tego pola i potwierdzenie przyciskiem Tak całkowicie "
#~ "wyłączy wsparcie dotyku dla tego profilu. Jest to użyteczne w "
#~ "aplikacjach, które posiadają własne gesty dotykowe. Aby przywrócić "
#~ "funkcjonalność dotyku, odznacz to pole wyboru lub ręcznie przełącz "
#~ "przepuszczanie dotyku."
