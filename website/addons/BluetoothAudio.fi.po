# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Bluetooth Audio\n"
"POT-Creation-Date: 2018-11-11 03:13+1000\n"
"PO-Revision-Date: 2018-11-12 12:33+0200\n"
"Last-Translator: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language-Team: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.11\n"
"X-Poedit-SourceCharset: UTF-8\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"BluetoothAudio\"]]\n"
msgstr "[[!meta title=\"BluetoothAudio\"]]\n"

#. type: Bullet: '* '
msgid "Author: Tony Malykh"
msgstr "Tekijä: Tony Malykh"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Lataa [vakaa versio][1]"

#. type: Plain text
msgid ""
"Bluetooth Audio is an NVDA add-on that improves sound quality when working "
"with bluetooth headphones or speakers."
msgstr ""
"Bluetooth Audio on lisäosa, joka parantaa äänenlaatua bluetooth-kuulokkeita "
"tai -kaiuttimia käytettäessä."

#. type: Plain text
msgid ""
"Most bluetooth devices enter standby mode after a few seconds of inactivity. "
"That means that when NVDA starts speaking again, the first split second of "
"sound will be lost. Bluetooth Audio add-on prevents bluetooth devices from "
"entering standby mode by constantly playing a silent sound, that is "
"inaudible to a human ear."
msgstr ""
"Useimmat bluetooth-laitteet siirtyvät valmiustilaan, kun ne ovat olleet "
"käyttämättömänä muutaman sekunnin. Tämä tarkoittaa, että kun NVDA alkaa "
"puhua uudestaan, pieni osa äänen alusta menetetään. Bluetooth Audio -lisäosa "
"estää bluetooth-laitteita siirtymästä valmiustilaan toistamalla jatkuvasti "
"hiljaista, ihmiskorvalle kuulumatonta ääntä."

#. type: Plain text
msgid ""
"Warning: using Bluetooth Audio add-on might reduce battery life of your "
"bluetooth device."
msgstr ""
"Varoitus: Tämän lisäosan käyttäminen saattaa lyhentää bluetooth-laitteen "
"akun käyttöikää."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=btaudio"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=btaudio"

#~ msgid "Source code:"
#~ msgstr "Lähdekoodi:"

#~ msgid "https://github.com/mltony/nvda-bluetooth-audio/"
#~ msgstr "https://github.com/mltony/nvda-bluetooth-audio/"

#~ msgid ""
#~ "[1]: https://github.com/mltony/nvda-bluetooth-audio/releases/download/"
#~ "v1.0/bluetoothaudio-1.0.nvda-addon"
#~ msgstr ""
#~ "[1]: https://github.com/mltony/nvda-bluetooth-audio/releases/download/"
#~ "v1.0/bluetoothaudio-1.0.nvda-addon"
