# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-09-01 09:13+0000\n"
"PO-Revision-Date: 2021-09-03 15:01+0800\n"
"Last-Translator: 完美很难 <1872265132@qq.com>\n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Easy Table Navigator\"]]\n"
msgstr "[[!meta title=\"Easy Table Navigator-简单表格导航插件\"]]\n"

#. type: Bullet: '* '
msgid "Authors: Corentin Bacqué-Cazenave, Joseph Lee"
msgstr "作者： Corentin Bacqué-Cazenave, Joseph Lee"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "下载 [稳定版][1]"

#. type: Bullet: '* '
msgid "Download [development version][2]"
msgstr "下载 [开发板][2]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2019.3 to 2021.1"
msgstr "NVDA兼容版本： 2019.3 到 2021.1"

#. type: Plain text
msgid ""
"This plugin adds a layer command to use arrow keys to navigate table cells."
msgstr "此插件添加了一个图层命令来使用光标键来导航表单元格。"

#. type: Plain text
msgid "Currently supported tables are:"
msgstr "目前支持的表格有："

#. type: Bullet: '* '
msgid "Browse mode (Internet Explorer, Firefox, etc.)."
msgstr "浏览模式（Internet Explorer，Firefox等）。"

#. type: Bullet: '* '
msgid "Microsoft Word."
msgstr "Microsoft Word。"

#. type: Title ##
#, no-wrap
msgid "Commands"
msgstr "快捷键"

#. type: Bullet: '* '
msgid "Toggles table navigator layer on and off (unassigned)."
msgstr "切换表格导航器图层（未指定）。"

#. type: Title ##
#, no-wrap
msgid "Changes for 2.2.1"
msgstr "版本 2.2.1"

#. type: Bullet: '* '
msgid "Fixed an error in some type of documents including Word and Outlook"
msgstr "修复了某些类型的文档（包括 Word 和 Outlook）中的错误"

#. type: Title ##
#, no-wrap
msgid "Changes for 2.2"
msgstr "版本 2.2"

#. type: Bullet: '* '
msgid "Update documentation style from addons template"
msgstr "从插件模板更新文档样式"

#. type: Bullet: '* '
msgid "First translated version"
msgstr "首个翻译版本"

#. type: Title ##
#, no-wrap
msgid "Changes for 2.1.1"
msgstr "版本 2.1.1"

#. type: Bullet: '* '
msgid "New author in manifest and documentation"
msgstr "更新 manifest 文件和文档中的作者信息"

#. type: Title ##
#, no-wrap
msgid "Changes for 2.1"
msgstr "版本 2.1"

#. type: Bullet: '* '
msgid "Compatibility with NVDA 2021.1"
msgstr "兼容 NVDA 2021.1"

#. type: Title ##
#, no-wrap
msgid "Changes for 2.0"
msgstr "版本2.0"

#. type: Bullet: '* '
msgid "Requires NVDA 2019.3 or later."
msgstr "需要NVDA 2019.3或更高版本。"

#. type: Bullet: '* '
msgid "Made various add-on messages translatable."
msgstr "使各种插件消息可翻译。"

#. type: Title ##
#, no-wrap
msgid "Changes for 1.2"
msgstr "版本1.2"

#. type: Bullet: '* '
msgid "Internal changes to support future NVDA releases."
msgstr "当前，内部更改以支持以后的NVDA版本。"

#. type: Title ##
#, no-wrap
msgid "Changes for 1.1"
msgstr "版本1.1"

#. type: Bullet: '* '
msgid ""
"Fixed an issue where errors might be heard when spell checking a message in "
"Outlook."
msgstr "修复了在Outlook中拼写检查邮件时可能会听到错误的问题。"

#. type: Title ##
#, no-wrap
msgid "Changes for 1.0"
msgstr "版本1.0"

#. type: Bullet: '*   '
msgid "Initial release."
msgstr "发布初始版本。"

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=etn"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=etn"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=etn-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=etn-dev"

#~ msgid "Author: Joseph Lee"
#~ msgstr "作者: Joseph Lee"

#, fuzzy
#~| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=etn"
#~ msgid "[3]: https://addons.nvda-project.org/files/get.php?file=etn-2019"
#~ msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=etn"

#, fuzzy
#~| msgid ""
#~| "*   Author: Joseph Lee\n"
#~| "* Download [stable version][1]\n"
#~| "* Download [development version][2]\n"
#~| "* NVDA compatibility: 2018.2 to 2019.1\n"
#~ msgid ""
#~ "*   Author: Joseph Lee\n"
#~ "* Download [stable version][1]\n"
#~ "* Download [development version][2]\n"
#~ "* NVDA compatibility: 2019.3 and beyond\n"
#~ "* Download [older version][3] compatible with NVDA 2019.2.1 and earlier\n"
#~ msgstr ""
#~ "*   作者: Joseph Lee\n"
#~ "* 下载 [稳定版][1]\n"
#~ "* 下载 [开发板][2]\n"
#~ "* NVDA兼容性：2018.2至2019.1 \\ n\n"
