# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Página do complemento Histórico de Falas\n"
"POT-Creation-Date: 2017-06-03 04:36+1000\n"
"PO-Revision-Date: 2017-09-27 13:31-0300\n"
"Last-Translator: Cleverson Casarin Uliana <clever97@gmail.com>\n"
"Language-Team: Equipe de tradução do NVDA para Português do Brasil "
"<clever97@gmail.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Speech History\"]]\n"
msgstr "[[!meta title=\"Histórico de falas\"]]\n"

#. type: Bullet: '* '
msgid "Author: James Scholes"
msgstr "Autor: James Scholes"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Baixe a [versão estável][1]"

#. type: Plain text
msgid ""
"An updated version of the Clip Copy add-on, initially created by Tyler "
"Spivey.  The add-on allows you to review the most recent 100 items spoken by "
"NVDA, as well as copy the selected item to the clipboard.  By default, use "
"Shift+F11 to move back through the history, Shift+F12 to move forwards and "
"F12 on its own to copy the selected item.  These hotkeys can be updated from "
"within the Speech category of NVDA's Input gestures dialog."
msgstr ""
"Uma versão atualizada do complemento Clip Copy, criado inicialmente por "
"Tyler Spivey. O complemento possibilita rever os últimos 100 itens falados "
"pelo NVDA, bem como copiar o item selecionado para a área de transferência. "
"Por padrão, use Shift+F11 para retroceder ao longo do histórico, Shift+F12 "
"para avançar e apenas F12 para copiar o item selecionado. Estas teclas podem "
"ser modificadas na categoria Fala, no diálogo Gestos para Entrada do NVDA."

#. type: Plain text
#, no-wrap
msgid "[[!tag stable]]\n"
msgstr "[[!tag stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=sps"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=sps"
