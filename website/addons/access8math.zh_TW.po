# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-07-03 13:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Access8Math\"]]\n"
msgstr ""

#. type: Bullet: '* '
msgid "Authors: Woody Tseng"
msgstr ""

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr ""

#. type: Plain text
msgid ""
"This NVDA addon provides the function of reading math content. Although the "
"original NVDA already equipped this feature by applying MathPlayer, some "
"functions still needed to be improved, such as not providing or incomplete "
"specific language translation, not providing specific language navigation "
"and browsing and many more."
msgstr ""

#. type: Plain text
msgid ""
"Navigation interactive mode can segment a math content into smaller partial "
"fragments for speaking, and select the read fragment and method through a "
"series of keyboard key operations. This function can better understand the "
"structure and items of long math content. The hierarchical relationship with "
"the item."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Reading feature"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Read math content written in MathML in web browser(Mozilla Firefox, "
"Microsoft Internet Explorer and Google Chrome) or read Microsoft Word math "
"content written in MathType. (MathPlayer installed only)"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Interaction: Press space or enter on the MathML math object to enter "
"navigation interactive mode. It means you can browse part of the sub-content "
"in the math content and move between sub-contents or zoom the size of the "
"sub-content"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Pressing \"Space\" in math content to open \"Access8Math interaction window"
"\" which contains \"interactive\" and \"copy\" button."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"interaction: Into math content to navigate and browse. Also, you can "
"partially explore the subparts in expression and move or zoom the content "
"between the subpart."
msgstr ""

#. type: Bullet: '    * '
msgid "copy: Copy MathML object source code."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Text review: Press the numeric keyboard 1-9 during navigation to read the "
"mathematical content of the serialized text word by word and line by line"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Analyze the overall mathematical meaning of the content: analyze the "
"structure of MathML, and when it meets a specific rule, read it aloud in the "
"mathematical meaning of the rule"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Analyze the mathematical meaning of the content item: When navigating and "
"browsing, it will prompt the meaning of the content under its upper content. "
"For example, there are two score items, and moving between them will enroll "
"the item as the denominator or numerator"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "navigation interactive mode command："
msgstr ""

#. type: Bullet: '* '
msgid "\"Down Arrow\": Zoom in on a smaller subpart of the math content."
msgstr ""

#. type: Bullet: '* '
msgid "\"Up Arrow\": Zoom out to a larger subpartthe of the math content ."
msgstr ""

#. type: Bullet: '* '
msgid "\"Left Arrow\": Move to the previous math content."
msgstr ""

#. type: Bullet: '* '
msgid "\"Right Arrow\": Move to the next math content."
msgstr ""

#. type: Bullet: '* '
msgid "\"Home\": Move back to the top.(Entire math content)\t"
msgstr ""

#. type: Bullet: '* '
msgid "\"Ctrl+c\": Copy object MathML source code"
msgstr ""

#. type: Bullet: '* '
msgid ""
"\"Numpad 1~9\": Reading the math content into serialized text using NVDA "
"Reviewing Text."
msgstr ""

#. type: Bullet: '* '
msgid "\"ESC\": Exit the navigation mode."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Writing feature"
msgstr ""

#. type: Plain text
msgid "Writing mixed content (text content and mathematical content):"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "write mixed content"
msgstr ""

#. type: Plain text
msgid ""
"Use delimiter(start delimiter \"\\(\" and end delimiter \"\\)\", LaTeX "
"block) to determine the area between the text content and the mathematical "
"content, that is, the data in LaTeX block is mathematical content (LaTeX), "
"and the data outside LaTeX block is text content."
msgstr ""

#. type: Plain text
msgid ""
"Press alt+h in edit field to convert an HTML document with mixed text data "
"and mathematical data and can be reviewed or exported. The data in the LaTeX "
"block will be converted to MathML for presentation with normal text."
msgstr ""

#. type: Bullet: '* '
msgid ""
"review: Open the converted HTML document through a program that opens the ."
"HTML extension by default."
msgstr ""

#. type: Bullet: '* '
msgid "export: Pack the converted HTML document into a zip file."
msgstr ""

#. type: Plain text
msgid ""
"Press alt+m key in edit field to pop up the markup command window, select "
"\"LaTeX\" and press enter, the LaTeX block will be added to the current "
"cursor and the cursor will be automatically moved into it for quick input "
"the content."
msgstr ""

#. type: Plain text
msgid ""
"Press alt+l key in edit field to pop up the LaTeX command window, select the "
"LaTeX command item to be added and press enter to add the corresponding "
"LaTeX syntax at the current cursor and automatically move the cursor to the "
"appropriate input point for quick Enter the content."
msgstr ""

#. type: Plain text
msgid "LaTeX command window"
msgstr ""

#. type: Bullet: '* '
msgid "Select the LaTeX command item and press f1~f12 to set the shortcut"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Select the LaTeX command item and press d to remove the shortcut that has "
"been set"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Select the LaTeX command item and press enter to add the corresponding LaTeX "
"syntax at the current cursor"
msgstr ""

#. type: Plain text
msgid "Edit cursor navigation move"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In edit field, press alt+left arrow key to move to the start point of the "
"previous data block"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In edit field, press alt+down key without moving, but only read the content "
"of the current data block"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In edit field, press alt+right arrow key to move to the start point of the "
"next data block"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In edit field, press alt+home to move to the start point of the current data "
"block"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In edit field, press alt+end to move to the end point of the current data "
"block"
msgstr ""

#. type: Plain text
msgid "Edit cursor navigation move and select"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In the editing area, press alt+shift+left arrow key to move to the previous "
"data block and select"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In the editing area, press alt+shift+down key to move to the current data "
"block and select"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In the editing area, press alt+shift+right arrow to move to the next data "
"block and select"
msgstr ""

#. type: Plain text
msgid ""
"Press alt+s in edit field to turn on or off the shortcut mode. When the "
"shortcut mode is on, press f1~f12 to quickly insert LaTeX syntax. When the "
"shortcut mode is on, press shift+f1~f12 to read out the LaTeX commands "
"currently bound to the shortcut."
msgstr ""

#. type: Plain text
msgid ""
"In edit field and the cursor is in the LaTeX block, press alt+i to enter "
"navigation interactive mode"
msgstr ""

#. type: Plain text
msgid ""
"Press NVDA+shift+space in edit field to turn on or off the edit single "
"letter navigation mode. When the edit single letter navigation mode is "
"turned on, you can move the edit cursor with single letter navigation"
msgstr ""

#. type: Plain text
msgid ""
"The following keys by themselves jump edit cursor to the next available "
"block, while adding the shift key causes them to jump edit cursor to the "
"previous block:"
msgstr ""

#. type: Bullet: '* '
msgid "l: move to the next LaTeX block"
msgstr ""

#. type: Bullet: '* '
msgid "t: move to the next text block"
msgstr ""

#. type: Plain text
msgid ""
"mixed content example: The solution of the quadratic equation in one "
"variable \\(ax^2+bx+c=0\\) is \\(\\frac{-b\\pm\\sqrt{b^2-4ac}}{2a}\\)."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "settings"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "All Access8Math menus are centralized in tools -> Access8Math.\n"
msgstr ""

#. type: Title ###
#, no-wrap
msgid "read feature settings"
msgstr ""

#. type: Bullet: '* '
msgid "General Settings dialog:"
msgstr ""

#. type: Bullet: '    * '
msgid "Language: Access8Math speaking language"
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Item interval time: Setting pause time between items. Values from 1 to 100, "
"the smaller the value, the shorter the pause time, and the greater the "
"value, the longer the pause time."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Showing Access8Math interaction window when entering interaction mode: "
"Whether to show \"Access8Math interaction window\" when pressing the space "
"key on the math object."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Analyze the mathematical meaning of the content: perform semantic analysis "
"on the mathematical content, and when it meets a specific rule, using that "
"rule to speak."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Reading pre-defined meaning in dictionary when navigating in interactive "
"mode: When the pattern is definied in the dictionary, use dictionary to read "
"the meaning of subpart in the upper layer part."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Reading of auto-generated meaning when navigating in interactive mode: When "
"the pattern is not difined or incomplete in dictionary, use automatic "
"generation function to read the meaning of subpart in the upper layer part."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Using a beep to alert no move: When navigating in interactive mode, It will "
"hint by beep. If it is not checked, it will hint by speaking \"no move\"."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Using NVDA+gesture to active action: Whether shortcut key needs to be added "
"with NVDA key when write mixed content in edit field"
msgstr ""

#. type: Bullet: '* '
msgid "Rule Settings dialog box: select whether rules are actived."
msgstr ""

#. type: Title ###
#, no-wrap
msgid "localization"
msgstr ""

#. type: Bullet: '* '
msgid ""
"\"Unicode dictionary\" allows customizing the reading method for each symbol "
"text."
msgstr ""

#. type: Bullet: '* '
msgid ""
"\"Mathematics Rules\" allows customizing the reading method for each type of "
"mathematics."
msgstr ""

#. type: Bullet: '* '
msgid ""
"\"Add a new language\" can add languages: that were not originally provided "
"in the built-in. After adding, there will be more newly added language "
"families in the general settings and can be used to define the reading "
"method through the \"unicode dictionary\" and \"mathematics rules\" to reach "
"localization"
msgstr ""

#. type: Title ####
#, no-wrap
msgid "Math Rules"
msgstr ""

#. type: Plain text
msgid ""
"Access8Math establishes 46 mathematical rules according to the mathematical "
"type and logic to decide the reading math method and order. According to "
"different local math reading logic, the math reading text and order can be "
"changed. The method is as follows:"
msgstr ""

#. type: Plain text
msgid ""
"Edit: After entering the \"math rule\", the window lists 46 math rules. "
"Choose any math rule and select the \"Edit\" to enter the editing entry."
msgstr ""

#. type: Plain text
msgid ""
"The \"editing entry\" can be divided into two major blocks, the \"Serialized "
"ordering\" and the \"Child role\"."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Serialized ordering: Math rule is divided into multiple blocks according to "
"the reading order. In this area, the reading order of child node and the "
"delimitation text of start, inter- and the end can be changed. Taking the "
"fractional rule mfrac as an example, this rule is divided into five reading "
"blocks. The order 0, 2, and 4 represent the initial prompt, the project "
"segmentation prompt, and the end prompt, respectively, and the meanings text "
"can be changed in each field. Order 1 and 3 adjust the reading\tsequence of "
"child node which can be changed in the drop-down menu."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Child role: The next-level sub-item of the mathematical rule. Taking the "
"fractional rule mfrac as an example, the rule contains the numerator and the "
"denominator. The sub-content in the upper sub-content meaning can be changed "
"in the child-node role field."
msgstr ""

#. type: Plain text
msgid ""
"Example: You can check the reading method of this math rule after editing. "
"After clicking, a math content is preset the corresponding math rules for "
"confirming whether the reading method is as expected."
msgstr ""

#. type: Plain text
msgid ""
"Recover default: Restores the list of math rules to their initial presets."
msgstr ""

#. type: Plain text
msgid ""
"Import: Import math rules files, which can be used to load math rules files."
msgstr ""

#. type: Plain text
msgid ""
"Export: Save the math rules file to the specified path to share or keep."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "example"
msgstr ""

#. type: Plain text
msgid "Math contents in Wiki are all written by MathML."
msgstr ""

#. type: Bullet: '* '
msgid "Quadratic equation: https://en.wikipedia.org/wiki/Quadratic_equation"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Matrix multiplication: https://en.wikipedia.org/wiki/Matrix_multiplication"
msgstr ""

#. type: Bullet: '* '
msgid "Cubic function: https://en.wikipedia.org/wiki/Cubic_function"
msgstr ""

#. type: Plain text
msgid "Quadratic equation"
msgstr ""

#. type: Bullet: '* '
msgid "LaTeX: \\(ax^2+bx+c=0\\)"
msgstr ""

#. type: Bullet: '* '
msgid ""
"MathML: <math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mrow><mo>-"
"</mo><mi>b</mi><mo>&#xB1;</mo><msqrt><msup><mi>b</mi><mn>2</mn></msup><mo>-</"
"mo><mn>4</mn><mi>a</mi><mi>c</mi></msqrt></mrow><mrow><mn>2</mn><mi>a</mi></"
"mrow></mfrac></math>"
msgstr ""

#. type: Plain text
msgid "github: https://github.com/tsengwoody/Access8Math"
msgstr ""

#. type: Plain text
msgid "Please report any bugs or comments, thank you!"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v3.0 Update"
msgstr ""

#. type: Bullet: '* '
msgid "Write mathematical content in AsciiMath"
msgstr ""

#. type: Bullet: '* '
msgid "Write mathematical content in LaTeX"
msgstr ""

#. type: Bullet: '* '
msgid "Writing mixed content (text content and mathematical content)"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Use shortcut keys to move the cursor to different types of blocks in edit "
"field"
msgstr ""

#. type: Bullet: '* '
msgid "Use command menu to select commands in edit field"
msgstr ""

#. type: Bullet: '* '
msgid "Set shortcuts in the LaTeX command menu"
msgstr ""

#. type: Bullet: '* '
msgid "Review and export content in edit field to HTML"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.6 Update"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Auto entering interactive mode when showing Access8Math interaction window."
msgstr ""

#. type: Bullet: '* '
msgid ""
"You can choose how to hint no movement in interactive mode: beep or speech "
"'no move' two way."
msgstr ""

#. type: Bullet: '* '
msgid ""
"The content of the current item will be repeated again When there is no "
"movement."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.5 Update"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Adding Russian translation of rules and UI. Thanks to the translation work "
"of Futyn-Maker."
msgstr ""

#. type: Bullet: '* '
msgid "Fixing compound symbol translation failed bug."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Removing duplicates of lowercase letters and added general uppercases in en "
"unicode.dic(0370~03FF)."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.4 Update"
msgstr ""

#. type: Bullet: '* '
msgid "Fix bug."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.3 Update"
msgstr ""

#. type: Bullet: '* '
msgid "Compatibility with Python3"
msgstr ""

#. type: Bullet: '* '
msgid "refactoring module and fix code style"
msgstr ""

#. type: Bullet: '* '
msgid "Adding one symbol vector rule"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.2 Update"
msgstr ""

#. type: Bullet: '* '
msgid "fix bug incorrect speech when a single node has more characters."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Fix compatibility issue in NVDA 2019.2, thanks to pull requests of "
"CyrilleB79."
msgstr ""

#. type: Bullet: '* '
msgid "Fix bug in unicode dict has duplicate symbols."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Add translations in French, thanks to the translation work of CyrilleB79."
msgstr ""

#. type: Bullet: '* '
msgid "Adjust keyboard shortcut."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.1 Update"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In \"General Settings\", you can set whether \"Access8Math interaction window"
"\" is automatically displayed when entering interactive mode."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In interactive mode, \"interaction window\" can be displayed manually via "
"ctrl+m when \"interaction window\" are not showed."
msgstr ""

#. type: Bullet: '* '
msgid "Fix multi-language switching bug."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Add translations in Turkish, thanks to the translation work of cagri (çağrı "
"doğan)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Compatibility update for nvda 2019.1 check for add-on`s manifest.ini flag."
msgstr ""

#. type: Bullet: '* '
msgid "Refactoring dialog window source code."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v2.0 Update"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Add multi-language new-adding and customizing settings,and add three windows "
"of \"unicode dictionary\", \"math rule\", \"New language adding\""
msgstr ""

#. type: Bullet: '* '
msgid ""
"The \"unicode dictionary\" can customize the reading way of each math "
"symbolic text."
msgstr ""

#. type: Bullet: '* '
msgid ""
"\"math rule\" can customize the reading method and preview the modification "
"through the sample button before completed."
msgstr ""

#. type: Bullet: '* '
msgid ""
"\"New language adding\" allows adding language not provided in the built-in "
"system. The newly language will be added to the general settings, and multi-"
"language customization can be achieved through reading definition of "
"\"unicode dictionary\" and \"mathematical rules\"."
msgstr ""

#. type: Bullet: '* '
msgid ""
"improved in interactive mode, you can use the number keys 7~9 to read "
"sequence text in the unit of line."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v1.5 update log"
msgstr ""

#. type: Bullet: '* '
msgid ""
"In \"general setting\" dialog box add setting pause time between items. "
"Values from 1 to 100, the smaller the value, the shorter the pause time, and "
"the greater the value, the longer the pause time."
msgstr ""

#. type: Bullet: '* '
msgid "Fix setting dialog box can't save configure in NVDA 2018.2."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v1.4 update log"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Adjust settings dialog box which divided into \"general setting\" and "
"\"rules setting\" dialog box. \"General Settings\" is the original "
"\"Access8Math Settings\" dialog box, and \"Rule Settings\" dialog box is for "
"selecting whether specific rules are enabled."
msgstr ""

#. type: Bullet: '* '
msgid "New rules"
msgstr ""

#. type: Bullet: '    * '
msgid ""
"vector rule: When there is a \"⇀\" right above two Identifier, the item is "
"read as \"Vector...\"."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"frown rule：When there is a \" ⌢ \" right above two Identifier, the item is "
"read as \"frown...\"."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v1.3 update log"
msgstr ""

#. type: Bullet: '* '
msgid "New rule"
msgstr ""

#. type: Bullet: '    * '
msgid ""
"positive rule: Read \"positive\" rather than \"plus\" when plus sign in "
"first item or its previous item is certain operator."
msgstr ""

#. type: Bullet: '    * '
msgid "square rule: When the power is 2, the item is read as \"squared\"."
msgstr ""

#. type: Bullet: '    * '
msgid "cubic rule: When the power is 3, the item is read as \"cubed\"."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"line rule: When there is \"↔\" right above two Identifier, the item is read "
"as \"Line ...\"."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"line segment rule: When there is \"¯\" right above two Identifier, the item "
"is read as \"Line segement ...\"."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"ray rule: When there is a \"→\" right above two Identifier, the item is read "
"as \"Ray ...\""
msgstr ""

#. type: Bullet: '* '
msgid ""
"Add interaction window: Pressing \"Space\" in math content to open "
"\"Access8Math interaction window\" which contains \"interaction\" and \"copy"
"\" button."
msgstr ""

#. type: Bullet: '    * '
msgid "interaction: Into math content to navigate and browse."
msgstr ""

#. type: Bullet: '* '
msgid "Add zh_CN UI language(.po)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Adjust inheritance relationship between rules to ensure proper use of the "
"appropriate rules in conflict."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v1.2 update log"
msgstr ""

#. type: Bullet: '    * '
msgid ""
"negative number rule: Read 'negative' rather than 'minus sign' when minus "
"sign in first item or its previous item is certain operator."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"integer add fraction rule: Read 'add' between integer and fraction when "
"fraction previous item is integer."
msgstr ""

#. type: Bullet: '* '
msgid "Program architecture improve"
msgstr ""

#. type: Bullet: '    * '
msgid "add sibling class"
msgstr ""

#. type: Bullet: '    * '
msgid "add dynamic generate Complement class"
msgstr ""

#. type: Bullet: '* '
msgid "Fix bug"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Access8Math v1.1 update log"
msgstr ""

#. type: Bullet: '* '
msgid "In navigation mode command, \"Ctrl+c\" copy object MathML source code."
msgstr ""

#. type: Bullet: '* '
msgid "Settings dialog box in Preferences:"
msgstr ""

#. type: Bullet: '    * '
msgid "Language: Access8Math reading language on math content."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Analyze the mathematical meaning of content: Semantically analyze the math "
"content, in line with specific rules, read in mathematical meaning of that "
"rules."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Read defined meaning in dictionary: When the pattern is definied in the "
"dictionary, use dictionary to read the meaning of subpart in the upper layer "
"part."
msgstr ""

#. type: Bullet: '    * '
msgid ""
"Read of auto-generated meaning: When the pattern is not difined or "
"incomplete in dictionary, use automatic generation function to read the "
"meaning of subpart in the upper layer part."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Add some simple rule. Single rules are simplified versions of various rules. "
"When the content only has one single item, for better understanding and "
"reading without confusion, you can omit to choose not to read the script "
"before and after the content."
msgstr ""

#. type: Bullet: '* '
msgid "Update unicode.dic."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr ""

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=access8math"
msgstr ""
