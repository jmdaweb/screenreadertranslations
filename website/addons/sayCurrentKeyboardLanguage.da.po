# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-05-26 01:10+1000\n"
"PO-Revision-Date: 2019-02-14 19:50+0100\n"
"Last-Translator: Nicolai Svendsen <chojiro1990@gmail.com>\n"
"Language-Team: \n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"sayCurrentKeyboardLanguage\"]]\n"
msgstr "[[!meta title=\"sayCurrentKeyboardLanguage\"]]\n"

#. type: Bullet: '* '
msgid "Author(s): Abdel, Noelia;"
msgstr "Forfatter (e): Abdel, Noelia;"

#. type: Bullet: '* '
msgid "Download [stable version][1];"
msgstr "Download [stabil version][1];"

#. type: Bullet: '* '
msgid "Download [development version][2]."
msgstr "Download [udviklingsversion][2]."

#. type: Title #
#, no-wrap
msgid "Presentation"
msgstr "Præsentation"

#. type: Plain text
msgid ""
"This addon was created following a request from a member on the nvda-addons "
"mailing list."
msgstr ""
"Denne tilføjelse blev oprettet efter en anmodning fra et medlem på "
"mailinglisten for nvda-addons."

#. type: Plain text
msgid ""
"It provides a keyboard shortcut, NVDA + F4, which allows to retrieve and "
"give the language of the current keyboard."
msgstr ""
"Denne pakke benyttes via en tastaturgenvej, NVDA+F4, som giver mulighed for "
"at angive sproget af den aktuelle tastatursprog."

#. type: Plain text
msgid "If pressed twice, gives the default language of the system."
msgstr "Hvis du trykker to gange, angiver systemets standardsprog."

#. type: Plain text
msgid ""
"At the first version of this module, it had been proposed as simple "
"globalPlugin to paste in the configuration directory of NVDA, it was then "
"transformed into addon."
msgstr ""
"Ved den første version af dette modul blev det foreslået, at det skulle være "
"et simpelt globalPlugin i NVDA' konfigurationsmappe. Senere blev dette modul "
"omdannet til en tilføjelsespakke."

#. type: Title ##
#, no-wrap
msgid "Notes"
msgstr "Noter"

#. type: Plain text
msgid ""
"If the NVDA + F4 keyboard shortcut conflicts with another command, you can "
"change it by going to the Preferences menu of NVDA, in the \"Input gestures"
"\" submenu."
msgstr ""
"Hvis genvejen NVDA+F4 skaber konflikt med en anden kommando, kan du ændre "
"den ved at gå til menuen Opsætning i NVDA-menuen, og derefter vælge "
"inputbevægelser."

#. type: Plain text
msgid "You will then find the script in the \"System status\" category."
msgstr "Du finder derefter scriptet i kategorien \"Systemstatus\"."

#. type: Title ##
#, no-wrap
msgid "Compatibility"
msgstr "Kompatibilitet"

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "This add-on is compatible with the versions of NVDA ranging from 2014.3 "
#| "until 2019.1."
msgid ""
"This add-on is compatible with the versions of NVDA ranging from 2014.3 "
"until 2019.3."
msgstr ""
"Denne tilføjelse er kompatibel med versionerne af NVDA fra 2014.3 til 2019.1."

#. type: Title ##
#, no-wrap
msgid "Changes for version 19.02"
msgstr "Ændringer for version 19.02"

#. type: Bullet: '* '
msgid ""
"Changed version numbering using YY.MM (The year in 2 digits, followed by a "
"dot, followed by the month in 2 digits);"
msgstr ""
"Ændret versionsnummerering til åå.MM (År i 2 cifre efterfulgt af et punktum, "
"efterfulgt af måneden i 2 cifre);"

#. type: Bullet: '* '
msgid ""
"Added compatibility with the new versioning format of add-on, appeared since "
"nvda 2019.1."
msgstr ""
"Tilføjede kompatibilitet med det nye versionsformat af der fra nu af "
"benyttes i tilføjelser, der blev aktuelt siden NVDA 2019.1."

#. type: Title ##
#, no-wrap
msgid "Changes for version 1.1"
msgstr "Ændringer for version 1.1"

#. type: Bullet: '* '
msgid ""
"The addon has been renamed from getCurKeyboardLanguage to "
"sayCurrentKeyboardLanguage;"
msgstr ""
"Tilføjelsen er blevet omdøbt fra getCurKeyboardLanguage til "
"sayCurrentKeyboardLanguage;"

#. type: Bullet: '* '
msgid "Added the GPL license to the addon;"
msgstr "Tilføjet GPL-licensen til tilføjelsen;"

#. type: Bullet: '* '
msgid ""
"Added the script getCurKeyboardLanguage to the \"System status\" category;"
msgstr ""
"Tilføjet script getCurKeyboardLanguage til kategorien \"Systemstatus\";"

#. type: Bullet: '* '
msgid "Fixed some errors in the code."
msgstr "Rettede nogle fejl i koden."

#. type: Title ##
#, no-wrap
msgid "Changes for version 1.0"
msgstr "Ændringer for version 1.0"

#. type: Bullet: '* '
msgid "Initial version."
msgstr "Første version."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=ckbl"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=ckbl"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=ckbl-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=ckbl-dev"
