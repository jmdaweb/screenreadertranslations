# Croatian translation for sayProductNameAndVersion.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the sayProductNameAndVersion package.
# Translators:
# Milo Ivir <mail@milotype.de>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: sayProductNameAndVersion\n"
"POT-Creation-Date: 2021-08-05 12:29+0000\n"
"PO-Revision-Date: 2021-11-08 00:56+0100\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: Milo Ivir <mail@milotype.de>\n"
"Language-Team: \n"
"X-Generator: Poedit 3.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Say Product Name and Version\"]]\n"
msgstr "[[!meta title=\"Izgovori naziv proizvoda i verziju (Say Product Name and Version)\"]]\n"

#. type: Bullet: '* '
msgid "Author: Patrick ZAJDA <patrick@zajda.fr>"
msgstr "Autor: Patrick ZAJDA <patrick@zajda.fr>"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2017.3 or later"
msgstr "NVDA kompatibilnost: 2017.3 ili novija"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Preuzmi [stabilnu verziju][1]"

#. type: Plain text
msgid ""
"This NVDA add-on add a shortcut to announce product name and version of the "
"foreground application, or copy these informations to the clipboard."
msgstr ""
"Ovaj NVDA dodatak dodaje prečac za najavu naziva proizvoda i verzije "
"aktualno korištenog programa ili kopira te informacije u međuspremnik."

#. type: Bullet: '* '
msgid "Shortcut: NVDA+Shift+V"
msgstr "Prečac: NVDA+šift+V"

#. type: Bullet: '* '
msgid "Press it twice to copy product name and version to the clipboard."
msgstr ""
"Pritisni dvaput za kopiranje naziva i verzije proizvoda u međuspremnik."

#. type: Title ##
#, no-wrap
msgid "Changes for 2021.07"
msgstr "Promjene u 2021.07"

#. type: Bullet: '* '
msgid "Add Croatian translation thanks to Tarik Hadžirović and Goran Parać"
msgstr "Prevodioci na hrvatski jezik: Tarik Hadžirović i Goran Parać"

#. type: Bullet: '* '
msgid "Update help message"
msgstr "Aktualizirana poruka pomoći"

#. type: Bullet: '* '
msgid "Update documentation style for visualy impaired users"
msgstr "Aktualiziran stil dokumentacije za slabovidne korisnike"

#. type: Bullet: '* '
msgid "Move shortcut from Speech to tools cathegory"
msgstr "Prečac premješten iz govora u kategoriju alata"

#. type: Title ##
#, no-wrap
msgid "Changes for 2020.02"
msgstr "Promjene u 2020.02"

#. type: Bullet: '* '
msgid "Python 3 compatibility"
msgstr "Python 3 kompatibilnost"

#. type: Bullet: '* '
msgid "Use informations from NVDA itself instead of using an external module"
msgstr ""
"Koristi informacije izravno iz NVDA čitača umjesto korištenja vanjskog "
"modula"

#. type: Bullet: '* '
msgid "Use script decorator for global plugin informations"
msgstr "Koristi dekorator skripta za globalne podatke dodataka"

#. type: Bullet: '* '
msgid "Use last version of add-on template"
msgstr "Koristi zadnju verziju predloška dodatka"

#. type: Title ##
#, no-wrap
msgid "Changes for 1.0.1"
msgstr "Promjene u 1.0.1"

#. type: Bullet: '* '
msgid ""
"Fixed the message after copying informations to the clipboard which was not "
"translatted."
msgstr ""
"Ispravljena je poruka nakon kopiranja informacija u međuspremnik koji nisu "
"prevedeni."

#. type: Title ##
#, no-wrap
msgid "Changes for 1.0"
msgstr "Promjene u 1.0"

#. type: Bullet: '* '
msgid "Initial Release"
msgstr "Prvo izdanje"

#. type: Plain text
#, no-wrap
msgid "[[!tag stable]]\n"
msgstr "[[!tag stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=spnav"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=spnav"
