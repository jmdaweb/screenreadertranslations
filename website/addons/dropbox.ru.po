# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: NVDA Community Addons website\n"
"POT-Creation-Date: 2021-07-29 12:59+0000\n"
"PO-Revision-Date: 2017-07-04 01:15+0400\n"
"Last-Translator: Ruslan Kolodyazhni <eye0@rambler.ru>\n"
"Language-Team: Translators <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"dropbox\"]]\n"
msgstr "[[!meta title=\"dropbox\"]]\n"

#. type: Bullet: '* '
msgid ""
"Authors: Patrick ZAJDA <patrick@zajda.fr>, Filaos and other contributors"
msgstr "Авторы: Patrick ZAJDA <patrick@zajda.fr>, Filaos и другие участники"

#. type: Bullet: '* '
msgid "NVDA compatibility: NVDA 2019.1 or later"
msgstr ""

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Загрузить [стабильную версию][1]"

#. type: Bullet: '* '
#, fuzzy
#| msgid "Download [stable version][1]"
msgid "Download [development version][2]"
msgstr "Загрузить [стабильную версию][1]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "This plugin will add a shortcut to announce Dropbox status, version or "
#| "open the Dropbox systray menu.  Also page tabs working on the preferences "
#| "dialog with Ctrl+tab / Ctrl+Shift+Tab and Ctrl+PageUp/Down.  To conclude, "
#| "make the cancel button working with escape."
msgid ""
"This plugin add a shortcut to announce Dropbox status or open the Dropbox "
"systray menu when pressed once or twice respectively.  It also enhances "
"DropBox item lists."
msgstr ""
"Объявляет информацию о состоянии, версию или открывает меню Dropbox в "
"системном трее Страницы вкладок также работают в диалоге настроек при помощи "
"Ctrl+Tab и Shift+Ctrl+Tab.Вы можете активировать кнопку \"cancel\", нажав "
"клавишу \"escape\"."

#. type: Bullet: '* '
msgid "Shortcut: NVDA+Alt+D"
msgstr "Горячая клавиша: NVDA+Alt+D"

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 4.0"
msgid "Changes for 4.6"
msgstr "Изменения в версии 4.0"

#. type: Bullet: '* '
msgid "Specify NVDA 2021.1 compatibility"
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 4.0"
msgid "Changes for 4.4"
msgstr "Изменения в версии 4.0"

#. type: Bullet: '* '
msgid "Python 3 compatibility"
msgstr ""

#. type: Bullet: '* '
msgid "Use the last addon template"
msgstr ""

#. type: Bullet: '* '
msgid "Repository change to be built with Appveyor"
msgstr ""

#. type: Bullet: '* '
msgid "Fixed wrong and removed unused shortcuts in the documentation"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Update the description in the documentation which still referenced the "
"announcement of the version"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Changes for 4.0"
msgstr "Изменения в версии 4.0"

#. type: Bullet: '* '
msgid "Add-on help is available from the Add-ons Manager."
msgstr "Справка дополнения доступна в диспетчере дополнений."

#. type: Bullet: '* '
msgid ""
"The shortcut to get Dropbox status has been changed to Alt+NVDA+D to avoid "
"conflict with audio ducking support."
msgstr ""
"Горячая клавиша для выяснения состояния Dropbox была изменена на Alt+NVDA+D, "
"чтобы исключить конфликт с поддержкой приглушения звуков."

#. type: Title ##
#, no-wrap
msgid "Changes for 3.1"
msgstr "Изменения в версии 3.1"

#. type: Bullet: '* '
msgid ""
"Use another way to get cancel button and page tab. Now we don't have to "
"focus them before using shortcuts."
msgstr ""
"Используйте другой способ получить кнопку cancel и вкладку. Теперь мы не "
"должны приводить их к фокусу перед использованием горячих клавиш."

#. type: Bullet: '* '
msgid ""
"When changing the active tab, the focus move to the tab page so when "
"pressing tab, the first item of the tab is activated instead of staying to "
"the previous used tab even if it is not activated anymore."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In the preferences dialog, it is now possible to press control+page up/down "
"to switch between tabs. Control+tab and control+shift+tab still work."
msgstr ""
"В диалоге настроек теперь можно нажать Ctrl+Page Up/Down для переключения "
"между вкладками. Ctrl+ Tab и Ctrl+Shift+Tab всё ещё работает."

#. type: Bullet: '* '
msgid "All localized manifest files should now be OK."
msgstr "Все локализованные файлы манифеста теперь должны быть в порядке."

#. type: Bullet: '* '
msgid "Minor corrections."
msgstr "Мелкие исправления."

#. type: Title ##
#, no-wrap
msgid "Changes for 3.0"
msgstr "Изменения в версии 3.0"

#. type: Bullet: '* '
msgid ""
"Minor correction in the main manifest file (authors are correctly displayed)."
msgstr ""
"Незначительная поправка в основном файле манифеста (авторы отображаются "
"корректно)."

#. type: Bullet: '* '
msgid "Improved context menu detection when pressing Shift+NVDA+D three times."
msgstr ""
"Улучшено обнаружение контекстного меню при нажатии Shift+NVDA+D три раза."

#. type: Bullet: '* '
msgid ""
"The escape button now works (only when using Dropbox in the same language "
"NVDA uses)."
msgstr ""
"Клавиша Escape Теперь работает  (только тогда когда Dropbox используется на "
"томже языке который использует NVDA)."

#. type: Bullet: '* '
msgid "A lot of corrections in the code."
msgstr "Множество исправлений в коде."

#. type: Bullet: '* '
msgid "Added/updated documentations of all scripts."
msgstr "Добавлена / обновлена документация всех скриптов."

#. type: Bullet: '* '
msgid ""
"New languages: Arabic, Brazilian Portuguese, Czech, Dutch, Finnish, "
"Galician, German, Hungarian, Japanese, Nepali, Polish, Russian, Spanish, "
"Slovak, Tamil, Turkish."
msgstr ""
"Новые языки: Арабский, Бразильский Португальский, Чежский, Голландский, "
"Финский, Галисийский, Немецкий, Венгерский, Японский, Непальский, Польский, "
"Русский, Испанский, Словацкий, Тамильский, Турецкий."

#. type: Title ##
#, no-wrap
msgid "Changes for 2.0"
msgstr "Изменения в версии 2.0"

#. type: Bullet: '* '
msgid "New languages: Italian"
msgstr "Новые языки: Итальянский"

#. type: Bullet: '* '
msgid ""
"Pressing the shortcut three times or more when already being in the context "
"menu doesn't cause problem anymore."
msgstr ""
"Нажатие сочетания три или более раз , когда уже контекстное меню открыто, не "
"вызывает более никаких проблем."

#. type: Title ##
#, no-wrap
msgid "Changes for 1.0"
msgstr "Изменения в версии 1.0"

#. type: Bullet: '* '
msgid "Initial Release"
msgstr "Первый публичный релиз"

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
#, fuzzy
#| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=dx"
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=dx"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=dx"

#. type: Plain text
#, fuzzy
#| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=dx"
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=dx-dev"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=dx"

#~ msgid "Ctrl+Alt+T announce the active tab."
#~ msgstr "Ctrl+Alt+T объявить активную вкладку."

#~ msgid "Known issues"
#~ msgstr "Известные проблемы"

#~ msgid ""
#~ "* If you switch between tabs using the shortcuts, when you'll close the "
#~ "preferences window, NVDA won't be able to know the windows doesn't exist "
#~ "anymore.\n"
#~ "It is a known issue on NVDA and cannot be fixed.\n"
#~ msgstr ""
#~ "* Если вы переключаетесь между вкладками с помощью горячих клавиш, когда "
#~ "вы Закроете окно настроек, NVDA не сможет знать о существовании окна.\n"
#~ "Это известная проблема, в NVDA, которая не может быть исправлена.\n"
