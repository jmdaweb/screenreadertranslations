# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-12-14 14:43+1000\n"
"PO-Revision-Date: 2018-07-22 15:57+0300\n"
"Last-Translator: Florian Ionașcu <florianionascu@hotmail.com>\n"
"Language-Team: \n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.6\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Clipspeak\"]]\n"
msgstr "[[!meta title=\"Clipspeak\"]]\n"

#. type: Bullet: '* '
msgid "Author: Damien Garwood"
msgstr "Autor: Damien Garwood"

#. type: Bullet: '* '
msgid "download [stable version][1]"
msgstr "descărcați [versiunea stabilă][1]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2018.3 to 2019.3"
msgstr ""

#. type: Plain text
msgid ""
"Clipspeak is an addon that allows NVDA to automatically announce clipboard "
"operations (such as cut, copy and paste), along with other common editing "
"operations such as select all, undo and redo.  In order to prevent "
"announcement in inappropriate situations, Clipspeak performs checks on the "
"control and the clipboard in order to make an informed decision as to "
"whether such an announcement is necessary. Because of this, Clipspeak's "
"announcements may be inaccurate.  By default, Clipspeak's gestures are "
"mapped to those commonly used by English versions of Windows, I.E.:"
msgstr ""
"Clipspeak este un supliment care îi permite NVDA-ului să anunțe automat "
"operațiile planșetei (cum ar fi tăierea, copierea și lipirea), împreună cu "
"alte operații comune de editare, cum ar fi selectarea întregului conținut de "
"pe planșetă, anularea și refacerea acestuia.  Pentru a preveni anunțarea în "
"situații inadecvate, Clipspeak efectuează verificări asupra comenzii și a "
"planșetei pentru a lua o decizie în cunoștință de cauză cu privire la "
"necesitatea unui astfel de anunț. Din acest motiv, anunțurile Clipspeak-ului "
"pot fi inexacte.  În mod implicit, gesturile Clipspeak-ului sunt puse la "
"cele utilizate în mod obișnuit de versiunile în engleză ale Windows-ului, I."
"E.:"

#. type: Bullet: '* '
msgid "CTRL+A: Select all"
msgstr "CTRL+A: Selectare tot"

#. type: Bullet: '* '
msgid "CTRL+Z: Undo"
msgstr "CTRL+Z: Anulare"

#. type: Bullet: '* '
msgid "CTRL+Y: Redo"
msgstr "CTRL+Y: Refacere"

#. type: Bullet: '* '
msgid "CTRL+X: Cut"
msgstr "CTRL+X: Tăiere"

#. type: Bullet: '* '
msgid "CTRL+C: Copy"
msgstr "CTRL+C: Copiere"

#. type: Bullet: '* '
msgid "CTRL+V: Paste"
msgstr "CTRL+V: Lipire"

#. type: Plain text
msgid ""
"If these are not the shortcuts commonly used for these tasks on your version "
"of Windows, you will need to remap these gestures in the input gestures "
"configuration."
msgstr ""
"Dacă acestea nu sunt scurtăturile de taste utilizate în comun pentru aceste "
"activități pe versiunea dumneavoastră de Windows, veți fi nevoit să repuneți "
"aceste gesturi în configurarea de intrare a lor."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=cs"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=cs"
