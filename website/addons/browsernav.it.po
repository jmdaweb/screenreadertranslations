# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-04-28 11:11+0000\n"
"PO-Revision-Date: 2020-12-28 02:32+0100\n"
"Last-Translator: Fabrizio Marini <marini.carlo@fastwebnet.it>\n"
"Language-Team: \n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"BrowserNav\"]]\n"
msgstr "[[!meta title=\"BrowserNav\"]]\n"

#. type: Bullet: '* '
msgid "Author: Tony Malykh"
msgstr "Autore: Tony Malykh"

#. type: Bullet: '* '
msgid "NVDA compatibility: from 2018.1 to 2019.1"
msgstr "Compatibilità con NVDA: dalla versione 2018.1 alla 2019.2"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Scarica la [versione stabile][1]"

#. type: Title #
#, no-wrap
msgid "BrowserNav addon for NVDA"
msgstr "Componente aggiuntivo BrowserNav per NVDA"

#. type: Plain text
msgid ""
"This add-on provides NVDA users powerful navigation commands in browser "
"mode.  For example, with BrowserNav you can find vertically aligned "
"paragraphs, that is paragraphs with the same horizontal offset. This can be "
"used to read hierarchical trees of comments or malformed HTML tables.  You "
"can also find paragraphs written in the same font size or style.  BrowserNav "
"also provides new QuickNav commands: P for next paragraph and Y for next tab."
msgstr ""
"Questo componente aggiuntivo fornisce agli utenti di NVDA comandi avanzati e "
"molto potenti per la modalità navigazione. Ad esempio, con BrowserNav è "
"possibile trovare paragrafi allineati verticalmente, ovvero paragrafi con la "
"stessa distanza dal margine sinistro della pagina. Ciò può essere utile per "
"leggere commenti organizzati gerarchicamente o tabelle HTML non valide. Puoi "
"anche trovare paragrafi scritti con la medesima dimensione o stile del "
"carattere. BrowserNav fornisce anche nuovi comandi di navigazione veloce: P "
"per il paragrafo successivo e Y per la scheda successiva."

#. type: Title ##
#, no-wrap
msgid "Usage in browsers"
msgstr "Utilizzo nei browser"

#. type: Plain text
msgid ""
"BrowserNav can be used to navigate by horizontal offset from the left edge "
"of the screen, by font size, or by font style."
msgstr ""
"BrowserNav può essere utilizzato per spostarsi tra i paragrafi allineati "
"verticalmente, oppure tra i paragrafi con una certa dimensione o tipo di "
"carattere. "

#. type: Bullet: '* '
msgid ""
"When navigating by horizontal offset, you can easily find paragraphs that "
"are vertically aligned on the page. IN particular, you can press NVDA+Alt"
"+DownArrow or UpArrow to jump to the next or previous paragraph that has the "
"same offset. For example, this can be useful when browsing hierarchical "
"trees of comments (e.g. on reddit.com) to jump between first level comments "
"and skipping all the higher level comments."
msgstr ""
"Quando ci si sposta per allineamento, è possibile individuare facilmente i "
"paragrafi che sono allineati verticalmente nella pagina. In particolare, "
"premendo NVDA+Alt+Freccia giù o su si passa tra i paragrafi aventi lo stesso "
"rientro sinistro. Ciò può essere utile quando si sfogliano commenti "
"organizzati gerarchicamente (ad es. su reddit.com) per passare tra i "
"commenti di primo livello e saltare tutti i commenti di livello inferiore."

#. type: Bullet: '* '
msgid ""
"When navigating by font size, you can easily find paragraphs written in the "
"same font size, or smaller/greater font size."
msgstr ""
"Quando ci si sposta per dimensione del carattere, è possibile trovare "
"facilmente paragrafi scritti con la stessa dimensione del carattere o con "
"una dimensione del carattere più piccola / più grande."

#. type: Bullet: '* '
msgid ""
"You can also navigate by font size with the constraint of same font style."
msgstr ""
"Puoi anche navigare per dimensione del carattere tra paragrafi con lo stesso "
"tipo di font."

#. type: Plain text
msgid "BrowserNav works in any browser supported by NVDA."
msgstr "BrowserNav funziona con qualsiasi browser supportato da NVDA."

#. type: Plain text
msgid "Keystrokes:"
msgstr "Tasti di scelta rapida:"

#. type: Bullet: '* '
msgid ""
"NVDA+Alt+UpArrow or DownArrow: Jump to previous or next paragraph with the "
"same horizontal offset or font size."
msgstr ""
"NVDA+Alt+freccia su o giù: passa al paragrafo precedente o successivo con lo "
"stesso rientro sinistro o la stessa dimensione del carattere."

#. type: Bullet: '* '
msgid ""
"NVDA+alt+LeftArrow: Jump to previous paragraph with lesser offset or greater "
"font size."
msgstr ""
"NVDA+alt+freccia sinistra: Passa al paragrafo precedente con rientro "
"sinistro minore o dimensione carattere maggiore."

#. type: Bullet: '* '
msgid ""
"NVDA+Alt+RightArrow: Jump to next paragraph with greater offset or smaller "
"font size."
msgstr ""
"NVDA+alt+freccia destra: Passa al paragrafo successivo con rientro sinistro "
"maggiore o dimensione carattere minore."

#. type: Bullet: '* '
msgid ""
"NVDA+O: Switch rotor setting between horizontal offset, font size, font size "
"with font style."
msgstr ""
"NVDA+O: sceglie il criterio di navigazione tra rientro sinistro, dimensione "
"carattere e dimensione carattere con stesso tipo di carattere."

#. type: Bullet: '* '
msgid "P or Shift+P: Jump to next or previous paragraph."
msgstr "P o shift + P: passa al paragrafo successivo o precedente."

#. type: Bullet: '* '
msgid "Y or Shift+Y: Jump to next or previous tab."
msgstr "Y o shift + Y: passa alla scheda successiva o precedente."

#. type: Title ##
#, no-wrap
msgid "Source code"
msgstr "Codice sorgente"

#. type: Plain text
msgid ""
"Source code is available at <https://github.com/mltony/nvda-browser-nav>."
msgstr ""
"Il codice sorgente  è disponibile su <https://github.com/mltony/nvda-browser-"
"nav>."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=browsernav"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=browsernav"
