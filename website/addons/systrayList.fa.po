# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-03-03 04:06+1000\n"
"PO-Revision-Date: 2016-12-29 15:30-0800\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"systrayList\"]]\n"
msgstr "[[!meta title=\"فهرست تکه‌ها در هشدارگاه سامانه\"]]\n"

#. type: Bullet: '* '
#, fuzzy
#| msgid "Authors: Rui Fontes, Rui Batista, NVDA Community contributors"
msgid ""
"Authors: Rui Fontes, Rui Batista, Joseph Lee, NVDA Community contributors"
msgstr "سازندگان: Rui Fontes, Rui Batista, حامیان انجمن NVDA"

#. type: Bullet: '* '
#, fuzzy
#| msgid "Download: [version 2.0-dev][1]"
msgid "Download [stable version][1]"
msgstr "دانلود: [نسخه 2.0-dev][1]"

#. type: Bullet: '* '
#, fuzzy
#| msgid "Download: [version 2.0-dev][1]"
msgid "NVDA compatibility: 2019.3 and beyond"
msgstr "دانلود: [نسخه 2.0-dev][1]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "This plugin makes possible to read and activate icons on the system tray "
#| "or the task bar, within a list box, for easy access and interaction to "
#| "those items. With the plugin installed, press NVDA+f11 once for this "
#| "dialog to pop-up with system tray elements, and press twice to pop it up "
#| "with task bar elements, which are the currently running applications."
msgid ""
"This add-on allows you to read and activate icons on the system tray or the "
"task bar, within a list box, for easy access and interaction to those items. "
"With the add-on installed, press NVDA+f11 once for this dialog to pop-up "
"with system tray elements, and press twice to display task bar elements, "
"which are the currently running applications."
msgstr ""
"این پلاگین به کاربران این امکان را میدهد تا نشانها در هشدارگاه سامانه یا "
"نوار وظیفه را فعال کرده و داخل جعبه لیست بخوانند, برای دسترسی آسان NVDA+f11 "
"را یکبار فشار داده تا هشدارگاه سامانه باز شود و با دوبار فشار دادن میتوانید "
"نوار وظیفه را باز نمایید تا برنامه های در کار را مشاهده نمایید."

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Note that you can access the windows system tray in any screenreader "
#| "software using the windows+b shortcut key, and the task bar by pressing "
#| "tab from the start button. This plugin is only useful to make the "
#| "transition from JAWS for Windows smoother and to avoid some tooltips that "
#| "may pop-up when cycling through the system tray with the windows keyboard "
#| "means."
msgid ""
"Note that you can access the windows system tray in any screen reader "
"software using the Windows+B, and the task bar by pressing Windows+T. This "
"plugin is only useful to make the transition from JAWS for Windows smoother "
"and to avoid some tooltips that may pop-up when cycling through the system "
"tray with the windows keyboard means."
msgstr ""
"توجه داشته باشید که شما میتوانید به هشدارگاه سامانه ویندوز در همه صفحه "
"خوانها با کلید میانبر windows+b دسترسی داشته باشید, و با زدن کلید تب "
"میتوانید به نوار وظیفه بروید موقعی که روی دکمه شروع هستید. "

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 1.3 - 2012-05-25"
msgid "Changes for 4.0 2020-01-03"
msgstr "تغییرات داده شده در نسخه 1.3 - 2012-05-25"

#. type: Bullet: '* '
#, fuzzy
#| msgid "Requires NVDA 2012.3beta2 or greater."
msgid "Requires NVDA 2019.3 or later."
msgstr "به NVDA 2012.3beta2 یا بالاتر نیاز دارد"

#. type: Bullet: '* '
msgid ""
"No more support for Windows versions earlier than Windows 7 Service Pack 1."
msgstr ""

#. type: Bullet: '* '
msgid "Add-on has been renamed to \"SysTrayList\"."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Donation request dialog will no longer appear when installing or updating "
"the add-on."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 1.3 - 2012-05-25"
msgid "Changes for 3.2 2018-12-24"
msgstr "تغییرات داده شده در نسخه 1.3 - 2012-05-25"

#. type: Bullet: '* '
msgid "More code changes to support Python 3."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 1.3 - 2012-05-25"
msgid "Changes for 3.1 2018-11-24"
msgstr "تغییرات داده شده در نسخه 1.3 - 2012-05-25"

#. type: Bullet: '* '
msgid "Internal changes to support future NVDA releases."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 1.3 - 2012-05-25"
msgid "Changes for 3.0 2018-10-25"
msgstr "تغییرات داده شده در نسخه 1.3 - 2012-05-25"

#. type: Plain text
msgid ""
"Support for Windows releases earlier than Windows 7 Service Pack 1 will end "
"in 2019."
msgstr ""

#. type: Bullet: '* '
msgid "Add-on is Python 3 compatible."
msgstr ""

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 1.3 - 2012-05-25"
msgid "Changes for 2.0 2017-05-20"
msgstr "تغییرات داده شده در نسخه 1.3 - 2012-05-25"

#. type: Bullet: '* '
msgid "Systray/taskbar dialog is now centered on screen."
msgstr ""

#. type: Bullet: '* '
msgid "Fixed various user interface issues."
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid "French and Turkish Translations."
msgid "New and updated translations."
msgstr "ترجمه زبانهای فرانسوی و ترکی"

#. type: Title ##
#, fuzzy, no-wrap
#| msgid "Changes for 1.4 - 2013-01-19"
msgid "Changes for 1.5 2015-mm-dd"
msgstr "تغییرات داده شده در نسخه 1.4 - 2013-01-19"

#. type: Bullet: '* '
msgid "Corrected left/right click action."
msgstr "عمل چپ/راست کلیک اصلاح شد"

#. type: Bullet: '* '
msgid "Add-on help is now available from add-ons manger."
msgstr ""

#. type: Bullet: '* '
msgid "Translation updates."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Changes for 1.4 - 2013-01-19"
msgstr "تغییرات داده شده در نسخه 1.4 - 2013-01-19"

#. type: Bullet: '* '
msgid "Requires NVDA 2012.3beta2 or greater."
msgstr "به NVDA 2012.3beta2 یا بالاتر نیاز دارد"

#. type: Bullet: '* '
msgid "Add donation request to installation procedure"
msgstr "اضافه کردن درخواست کمک برای ادامه دادن به مراحل نصب"

#. type: Bullet: '* '
msgid "Implemented taskbar support"
msgstr "پشتیبانی از نوار وظیفه پیاده سازی"

#. type: Bullet: '* '
msgid ""
"New translations: Arabic, Bulgarian, Dutch, Finnish, Galician, Greek, "
"Hungarian, Italian, Japanese, Korean, Nepali, Norwegian, Polish, Brazilian "
"Portuguese, Russian, Slovak, Tamil, Traditional Chinese Hong Kong."
msgstr ""
"ترجمه های جدید: عربی، بلغاری، هلندی ، فنلاندی ، گالیسی، یونانی ، مجارستانی ، "
"ایتالیایی ، ژاپنی ، کره ای ، نپالی، نروژی، لهستانی، پرتغالی برزیل، روسیه ،"
"اسلواکی ، تامیل ، چینی سنتی هنگ کنگ ."

#. type: Title ##
#, no-wrap
msgid "Changes for 1.3 - 2012-05-25"
msgstr "تغییرات داده شده در نسخه 1.3 - 2012-05-25"

#. type: Bullet: '* '
msgid "Converted to an add-on package"
msgstr "به بسته افزونه ها تبدیل شده است"

#. type: Bullet: '* '
msgid "French and Turkish Translations."
msgstr "ترجمه زبانهای فرانسوی و ترکی"

#. type: Title ##
#, no-wrap
msgid "Changes for  1.2 - 2012-04.25"
msgstr "تغییرات داده شده در نسخه 1.2 - 2012-04.25"

#. type: Bullet: '* '
msgid "Spanish and German translations. Thanks to all that contributed."
msgstr "ترجمه زبانهای اسپانیایی و آلمانی. با تشکر از کسانی که کمک کردند."

#. type: Title ##
#, no-wrap
msgid "Changes for 1.1 - 2012-03-20"
msgstr "تغییرات داده شده در نسخه 1.1 - 2012-03-20"

#. type: Bullet: '* '
msgid ""
"Corrected a bug that was making it impossible to use the plugin when a "
"gettext translation is not available"
msgstr ""
"تصحیح اشکالی که استفاده از پلاگین را غیر ممکن میکرد موقعی که ترجمه gettext "
"دردسترس نبود"

#. type: Title ##
#, no-wrap
msgid "Changes for 1.0 - 2012-03-19"
msgstr "تغییرات داده شده در نسخه 1.0 - 2012-03-19"

#. type: Bullet: '* '
msgid "Initial Release"
msgstr "انتشار اولیه"

#. type: Plain text
#, no-wrap
msgid "[[!tag stable]]\n"
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=st"
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=st"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=st"

#, fuzzy
#~| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=st"
#~ msgid "[2]: https://addons.nvda-project.org/files/get.php?file=st-2019"
#~ msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=st"

#, fuzzy
#~| msgid "Download: [version 2.0-dev][1]"
#~ msgid "Download [development version][2]"
#~ msgstr "دانلود: [نسخه 2.0-dev][1]"
