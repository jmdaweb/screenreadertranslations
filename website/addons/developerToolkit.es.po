# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2020-05-13 01:16+1000\n"
"PO-Revision-Date: 2020-05-12 17:09+0200\n"
"Last-Translator: José Manuel Delicado <jmdaweb@hotmail.com>\n"
"Language-Team: \n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Developer Toolkit\"]]\n"
msgstr "[[!meta title=\"Developer Toolkit\"]]\n"

#. type: Bullet: '* '
msgid "Author: Andy Borka"
msgstr "Autor: Andy Borka"

#. type: Bullet: '* '
msgid "download [stable version][1]"
msgstr "descargar [versión estable][1]"

#. type: Bullet: '* '
msgid "download [development version][2]"
msgstr "descargar [versión de desarrollo][2]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2019.1 to 2020.1"
msgstr "Compatibilidad con NVDA: de 2019.1 a 2020.1"

#. type: Plain text
msgid ""
"Developer toolkit (DTK) is an NVDA add-on that helps blind and visually "
"impaired developers independently create visually appealing user interfaces "
"and web content. It provides gestures that enable you to navigate through "
"objects and obtain information about them, such as their size, position, and "
"characteristics. To begin using DTK, place focus on a control, then press "
"**ALT+WINDOWS+K**. To disable it, press **ALT+WINDOWS+K** again. When on the "
"web, press **NVDA+SPACE** to put NVDA in Focus Mode and press NVDA+SHIFT"
"+SPACE to disable Single Letter Navigation."
msgstr ""
"Developer Toolkit (DTK) es un complemento para NVDA que asiste a los "
"desarrolladores ciegos o con baja visión a la hora de crear interfaces de "
"usuario visualmente atractivas o contenido web de forma autónoma. El "
"complemento proporciona gestos que permiten navegar por objetos y obtener "
"información sobre ellos, como su tamaño, posición, y características. Para "
"comenzar a usar DTK, sitúa el foco en un control y pulsa **Alt+windows+k**. "
"Para desactivarlo, pulsa nuevamente **Alt+windows+k**. Cuando estés en la "
"web, pulsa **NVDA+espacio** para poner a NVDA en modo foco, y pulsa **NVDA"
"+shift+espacio** para desactivar la navegación con una sola letra."

#. type: Title ##
#, no-wrap
msgid "Gestures"
msgstr "Gestos"

#. type: Plain text
msgid "The following gestures are available when DTK is enabled."
msgstr ""
"Los siguientes gestos se encuentran disponibles cuando DTK está activado."

#. type: Bullet: '* '
msgid "**ALT+WINDOWS+K** - Enable or disable DTK features."
msgstr "**Alt+windows+k** - Activa o desactiva las funciones de DTK."

#. type: Bullet: '* '
msgid "**LEFT ARROW** - Move to previous sibling."
msgstr "**Flecha izquierda** - Moverse al elemento hermano anterior."

#. type: Bullet: '* '
msgid "**RIGHT ARROW** - Move to next sibling."
msgstr "**Flecha derecha** - Moverse al siguiente elemento hermano."

#. type: Bullet: '* '
msgid "**UP ARROW** - Move to parent."
msgstr "**Flecha arriba** - Moverse al elemento padre."

#. type: Bullet: '* '
msgid "**DOWN ARROW** - Move to first child."
msgstr "**Flecha abajo** - Moverse al primer elemento hijo."

#. type: Bullet: '* '
msgid "**CTRL+HOME** - Move to top-most parent."
msgstr "**Ctrl+inicio** - Moverse al elemento padre de más alto nivel."

#. type: Bullet: '* '
msgid "**HOME** - Move to the relative parent if one is assigned."
msgstr "**Inicio** - Se mueve al padre relativo si hay uno asignado."

#. type: Bullet: '* '
msgid ""
"**A** - In web content, speak HTML attributes. Press twice quickly to copy "
"to the clipboard."
msgstr ""
"**A** - En el contenido web, verbaliza los atributos HTML. Pulsa dos veces "
"rápidamente para copiarlos al portapapeles."

#. type: Bullet: '* '
msgid ""
"**B** - Speak the position of the object's bottom edge. Press twice quickly "
"to copy to the clipboard."
msgstr ""
"**B** - Verbalizar la posición del borde inferior del objeto. Pulsa dos "
"veces rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**SHIFT+B** - Speak the distance between the object's bottom edge and the "
"relative parent's bottom edge. Press twice quickly to copy to the clipboard."
msgstr ""
"**Shift+b** - Verbalizar la distancia entre el borde inferior del objeto y "
"el borde inferior del padre relativo. Pulsa dos veces rápidamente para "
"copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**C** - Speak the number of children contained inside the object. Press "
"twice quickly to copy to the clipboard."
msgstr ""
"**C** - Verbalizar la cantidad de hijos que contiene el objeto. Pulsa dos "
"veces rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid "**control+c** - Switch between RGB, Hex, and Name color values."
msgstr ""
"**Control+c** - Alternar entre valores de color RGB, hexadecimal o por "
"nombre."

#. type: Bullet: '* '
msgid "**CTRL+D** - Enable or disable detailed messages."
msgstr "**Ctrl+d** - Activar o desactivar mensajes detallados."

#. type: Bullet: '* '
msgid ""
"**F** - In web content, speaks the object's font and formatting information. "
"Press twice quickly to copy to the clipboard."
msgstr ""
"**F** - En el contenido web, verbaliza la información de fuente y formato "
"del objeto. Pulsa dos veces rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**H** - Speak the object's height. Press twice quickly to copy to the "
"clipboard."
msgstr ""
"**H** - Verbalizar la altura del objeto. Pulsa dos veces rápidamente para "
"copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**L** - Speak the position of the object's left edge. Press twice quickly to "
"copy to the clipboard."
msgstr ""
"**L** - Verbalizar la posición del borde izquierdo del objeto. Pulsa dos "
"veces rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**n** - Speak the object's name. Press twice quickly to copy to the "
"clipboard."
msgstr ""
"**N** - Verbalizar el nombre del objeto. Pulsa dos veces rápidamente para "
"copiarlo al portapapeles."

#. type: Bullet: '* '
msgid ""
"**CTRL+P** - Set the relative parent for obtaining size/location of objects."
msgstr ""
"**Control+p** - Configura el padre relativo para obtener el tamaño y "
"posición de los objetos."

#. type: Bullet: '* '
msgid ""
"**P** - Speak the relative parent's name. Press twice quickly to copy to the "
"clipboard."
msgstr ""
"**P** - Verbalizar nombre del padre relativo. Pulsa dos veces rápidamente "
"para copiarlo al portapapeles."

#. type: Bullet: '* '
msgid ""
"**R** - Speak the position of the object's right edge. Press twice quickly "
"to copy to the clipboard."
msgstr ""
"**R** - Verbalizar la posición del borde derecho del objeto. Pulsa dos veces "
"rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**SHIFT+R** - Speak the distance between the object's right edge and the "
"relative parent's right edge. Press twice quickly to copy to the clipboard."
msgstr ""
"**Shift+r** - Verbalizar la distancia entre el borde derecho del objeto y el "
"borde derecho del padre relativo. Pulsa dos veces rápidamente para copiarla "
"al portapapeles."

#. type: Bullet: '* '
msgid ""
"**ALT+R** - Speak the object's Role/control type. Press twice quickly to "
"copy it to the clipboard."
msgstr ""
"**Alt+r** - Verbalizar el rol o tipo de control del objeto. Pulsa dos veces "
"rápidamente para copiarlo al portapapeles."

#. type: Bullet: '* '
msgid ""
"**S** - Speak the number of siblings relative to the object. Press twice "
"quickly to copy to the clipboard."
msgstr ""
"**S** - Verbalizar la cantidad de hermanos relativos al objeto. Pulsa dos "
"veces rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**SHIFT+S** - Speak the object's control states. Press twice quickly to copy "
"it to the clipboard."
msgstr ""
"**Shift+s** - Verbalizar los estados de control del objeto. Pulsa dos veces "
"rápidamente para copiarlos al portapapeles."

#. type: Bullet: '* '
msgid ""
"**T** - Speak the position of the object's top edge. Press twice quickly to "
"copy to the clipboard."
msgstr ""
"**T** - Verbalizar la posición del borde superior del objeto. Pulsa dos "
"veces rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**V** - Speak Developer toolkit version. Press twice quickly to copy to the "
"clipboard."
msgstr ""
"**V** - Verbalizar la versión de Developer Toolkit. Pulsa dos veces "
"rápidamente para copiarla al portapapeles."

#. type: Bullet: '* '
msgid ""
"**W** - Speak the object's width. Press twice quickly to copy to the "
"clipboard."
msgstr ""
"**W** - Verbalizar la anchura del objeto. Pulsa dos veces rápidamente para "
"copiarla al portapapeles."

#. type: Title ##
#, no-wrap
msgid "Notes"
msgstr "Notas"

#. type: Bullet: '* '
msgid ""
"When using home or any modified version of the home key, using the numpad "
"home key fails because NVDA will send the numpad7 keypress instead of a "
"numpadHome keypress. Other keyboard add-ons that attempt to reassign numpad7 "
"to the home key will fail in this add-on."
msgstr ""
"Al usar inicio o cualquier versión modificada de esta tecla, la tecla Inicio "
"del teclado numérico falla porque NVDA envía la pulsación del 7 numérico en "
"lugar de la pulsación del Inicio numérico. Otros complementos que intentan "
"reasignar el 7 del teclado numérico a la tecla Inicio fallarán con este "
"complemento."

#. type: Bullet: '* '
msgid ""
"When using the relative parent feature, DTK will set the relative parent to "
"the desktop under the following conditions."
msgstr ""
"Al usar la función de padre relativo, DTK configurará el escritorio como "
"padre relativo si se dan las siguientes condiciones."

#. type: Bullet: '	* '
msgid "The focused object and the relative parent are the same."
msgstr "El objeto con el foco y el padre relativo son el mismo."

#. type: Bullet: '	* '
msgid "The relative parent is not a direct ancestor of the focused object."
msgstr "El padre relativo no es un antecesor directo dell objeto con el foco."

#. type: Bullet: '* '
msgid ""
"DTK cannot access information such as CSS rules, padding, borders, or z-"
"index. Doing so requires accessing them outside of the NVDA context, which "
"presents a security concern for users."
msgstr ""
"DTK no puede acceder a información como reglas CSS, relleno, bordes, o z-"
"index. Hacerlo requeriría acceder a todo esto desde fuera del contexto de "
"NVDA, lo que supone un riesgo de seguridad para los usuarios."

#. type: Title ##
#, no-wrap
msgid "Known issues"
msgstr "Problemas conocidos"

#. type: Bullet: '* '
msgid ""
"The customizable list of font attributes found in Developer toolkit settings "
"may be cumbersome to use. This is a limitation found in NVDA's user "
"interface library."
msgstr ""
"La lista personalizable de atributos de fuente disponible en las opciones de "
"Developer Toolkit puede ser algo engorrosa de utilizar. Esto se debe a una "
"limitación de la biblioteca de interfaz de usuario de NVDA."

#. type: Title ##
#, no-wrap
msgid "Version history"
msgstr "Historial de versiones"

#. type: Title ###
#, no-wrap
msgid "20.04"
msgstr "20.04"

#. type: Bullet: '* '
msgid ""
"Added a gesture (home) that gives the ability to move to the defined "
"relative parent."
msgstr ""
"Se ha añadido un gesto (Inicio) que da la capacidad de moverse al padre "
"relativo definido."

#. type: Title ###
#, no-wrap
msgid "20.03"
msgstr "20.03"

#. type: Bullet: '* '
msgid ""
"Added a gesture (ALT+R) that obtains the currently focused object's control "
"type. Examples include button, listitem, list, checkbox, and section."
msgstr ""
"Se ha añadido un gesto (Alt+r) que obtiene el tipo de control del objeto "
"actual con el foco. Algunos ejemplos son botón, elemento de lista, lista, "
"casilla de verificación y sección."

#. type: Bullet: '* '
msgid ""
"Added a gesture (SHIFT+S) that obtains the currently focused object's "
"control states. Examples include focused, focusable, selected, checked, "
"pressed, and readonly."
msgstr ""
"Se ha añadido un gesto (Shift+s) que obtiene los estados del control que "
"tiene el foco actualmente. Algunos ejemplos son enfocado, enfocable, "
"seleccionado, marcado, pulsado y sólo lectura."

#. type: Bullet: '* '
msgid ""
"Removed support for IAccessible's unique ID because it is not reliable in "
"most contexts."
msgstr ""
"Se ha eliminado el soporte para el identificador único de IAccessible, ya "
"que no es fiable en la mayoría de contextos."

#. type: Bullet: '* '
msgid ""
"Abbreviated the version number to xx.yy where xx is the last two digits of "
"the current year, and yy is the update number for the current year. Thus "
"20.03 is the third major update in 2020."
msgstr ""
"Se ha abreviado el número de versión a xx.yy, donde xx son los dos últimos "
"dígitos del año actual, e yy es el número de actualización del año actual. "
"Por tanto, 20.03 es la tercera actualización importante de 2020."

#. type: Title ###
#, no-wrap
msgid "2020.2"
msgstr "2020.2"

#. type: Bullet: '* '
msgid ""
"Starting DTK retains the cursor’s current position in web content. If the "
"object under the caret can’t gain focus, DTK will place focus on the first "
"available ancestor."
msgstr ""
"Al iniciarse, DTK retiene la posición actual del cursor en el contenido web. "
"Si el objeto bajo el cursor no puede recibir el foco, DTK lo situará en el "
"primer antecesor disponible."

#. type: Bullet: '* '
msgid "Removed legacy support for unicode strings."
msgstr "Se ha eliminado el soporte obsoleto para cadenas Unicode."

#. type: Bullet: '* '
msgid ""
"Removed the enable/disable Developer toolkit features setting from Developer "
"toolkit settings dialog found in NVDA menu>preferences>settings. Users can "
"still enable/disable features by pressing alt+windows+k."
msgstr ""
"Se ha eliminado el ajuste para activar o desactivar las funciones de "
"Developer Toolkit desde el diálogo de opciones de Developer Toolkit situado "
"en el menú NVDA > Preferencias > Opciones. Los usuarios todavía pueden "
"activar o desactivar estas funciones pulsando alt+windows+k."

#. type: Bullet: '* '
msgid ""
"The formatting attributes obtained by pressing 'f' when Developer toolkit is "
"enabled is now configurable in preferences>settings>Developer toolkit."
msgstr ""
"Ahora se pueden configurar los atributos de formato que se obtienen al "
"pulsar 'f' cuando Developer Toolkit está activado en Preferencias > Opciones "
"> Developer Toolkit."

#. type: Bullet: '	* '
msgid ""
"Add new formatting attributes by pressing the 'new attribute' button. Type "
"in a name of a valid formatting attribute, press tab to put it in the list, "
"then press escape to leave edit mode."
msgstr ""
"Añade nuevos atributos de formato pulsando el botón 'Nuevo atributo'. Teclea "
"el nombre de un atributo de formato válido, pulsa tab para ponerlo en la "
"lista y pulsa escape para abandonar el modo de edición."

#. type: Bullet: '	* '
msgid ""
"Remove an attribute by selecting it in the list, then pressing the 'delete "
"attribute' button."
msgstr ""
"Elimina un atributo seleccionándolo en la lista y pulsando el botón "
"'Eliminar atributo'."

#. type: Bullet: '	* '
msgid ""
"Rename an attribute by pressing the 'Rename attribute' button, typing a new "
"name, pressing tab to put it in the list, then pressing escape to leave edit "
"mode."
msgstr ""
"Renombra un atributo pulsando el botón 'Renombrar atributo', escribiendo un "
"nuevo nombre, pulsando tab para ponerlo en la lista y escape para salir del "
"modo de edición."

#. type: Bullet: '	* '
msgid ""
"Move an attribute up in the list by selecting it and pressing the 'Move up' "
"button. Repeat this until it reaches the desired location in the list."
msgstr ""
"Sube un atributo en la lista seleccionándolo y pulsando el botón 'Subir'. "
"Repite estos pasos hasta que alcance la posición deseada en la lista."

#. type: Bullet: '	* '
msgid ""
"Move an attribute down in the list by selecting it and pressing the 'Move "
"down' button. Repeat this until it reaches the desired location in the list."
msgstr ""
"Baja un atributo en la lista seleccionándolo y pulsando el botón 'Bajar'. "
"Repite estos pasos hasta que alcance la posición deseada en la lista."

#. type: Bullet: '* '
msgid ""
"Users now have the ability to change the way color values are displayed "
"while obtaining formatting information. The setting is found in "
"preferences>settings>Developer toolkit, then choosing a color display "
"format. The color display format can also be changed by pressing control+c "
"while Developer toolkit features are enabled."
msgstr ""
"Los usuarios ahora disponen de la capacidad de cambiar la forma de mostrar "
"los colores al obtener información de formato. Este ajuste se encuentra "
"disponible en Preferencias > Opciones > Developer Toolkit, y eligiendo un "
"formato para mostrar el color. También se puede cambiar el formato para "
"mostrar el color pulsando Control+c mientras las funciones de Developer "
"Toolkit están activadas."

#. type: Bullet: '	* '
msgid ""
"**RGB** - Red/Green/Blue value. Defines a color through a red, green, blue "
"color combination. Good for graphic art, especially non-web safe colors."
msgstr ""
"**RGB** - Valor rojo/verde/azul. Define un color como una combinación de los "
"colores rojo, verde y azul. Bueno para artes gráficas, especialmente colores "
"seguros fuera de la web."

#. type: Bullet: '	* '
msgid ""
"**Hex** - string starting with a '#' and followed by 6 characters consisting "
"of 1-0 and A-F. Usually good for web content."
msgstr ""
"**Hexadecimal** - Cadena que comienza con un '#', seguida de 6 caracteres "
"que comprenden del 0 al 9 y de la A a la F. Normalmente útil en contenido "
"web."

#. type: Bullet: '	* '
msgid ""
"**Name** - The human readable value for the color. Good for general purpose "
"usage and verifying other color value formats."
msgstr ""
"**Nombre** - El valor del color legible para las personas. Bueno para uso "
"general y verificar otros formatos de valor de color."

#. type: Title ###
#, no-wrap
msgid "2020.1.1"
msgstr "2020.1.1"

#. type: Bullet: '* '
msgid "Improved unicode support."
msgstr "Se ha mejorado el soporte Unicode."

#. type: Title ###
#, no-wrap
msgid "2020.1.0"
msgstr "2020.1.0"

#. type: Bullet: '* '
msgid ""
"Developers now have the ability to focus on smaller areas of their user "
"interfaces by pressing CTRL+p to set a relative parent. Use a relative "
"parent as a reference point when obtaining size and location information. To "
"use this feature, enable DTK features, navigate to the object to use as a "
"relative parent, then press CTRL+p. Then, return to your work as usual."
msgstr ""
"Los desarrolladores ahora tienen la posibilidad de centrarse en áreas más "
"pequeñas de sus interfaces de usuario pulsando ctrl+p para configurar un "
"padre relativo. Usa el padre relativo como punto de referencia al obtener "
"información de tamaño y posición. Para usar esta característica, activa las "
"funciones de DTK, navega al objeto que usarás como padre relativo y pulsa "
"ctrl+p. A continuación, sigue trabajando como siempre."

#. type: Bullet: '* '
msgid ""
"Press the letter p while working in DTK to obtain the relative parent's "
"name. Press twice quickly to copy to the clipboard."
msgstr ""
"Pulsa la letra p mientras trabajas con DTK para obtener el nombre del padre "
"relativo. Pulsa dos veces rápidamente para copiarlo al portapapeles."

#. type: Bullet: '* '
msgid ""
"Use SHIFT+b to obtain the distance between the focused object's bottom edge "
"and the relative paren'ts bottom edge. DTK features must be enable to use "
"this feature."
msgstr ""
"Usa shift+b para obtener la distancia entre el borde inferior del objeto con "
"el foco y el borde inferior del padre relativo. Las funciones de DTK deben "
"estar activadas para hacer esto."

#. type: Bullet: '* '
msgid ""
"Use SHIFT+r to obtain the distance between the focused object's right edge "
"and the relative paren'ts right edge. DTK features must be enable to use "
"this feature."
msgstr ""
"Usa shift+r para obtener la distancia entre el borde derecho del objeto con "
"el foco y el borde derecho del padre relativo. Las funciones de DTK deben "
"estar activadas para hacer esto."

#. type: Bullet: '* '
msgid "DTK now gracefully handles configuration profile switches."
msgstr "Ahora DTK procesa correctamente los cambios de perfil."

#. type: Bullet: '* '
msgid ""
"Removed '-preview' from the version number to avoid version number problems "
"with add-on updater."
msgstr ""
"Se ha eliminado '-preview' del número de versión para evitar problemas de "
"número de versión con Add-on Updater."

#. type: Title ###
#, no-wrap
msgid "2020.0 preview"
msgstr "2020.0 preview"

#. type: Bullet: '* '
msgid ""
"Changed version number to 2020.0 preview to reflect the impending switch to "
"Python 3."
msgstr ""
"Se ha cambiado el número de versión a 2020.0 preview para reflejar el paso a "
"Python 3."

#. type: Bullet: '* '
msgid "Added Python 3 compatibility."
msgstr "Se ha añadido compatibilidad con Python 3."

#. type: Bullet: '* '
msgid ""
"Added a new gesture, \"n\" that speaks the object's name. If one is not "
"assigned, speaks the word 'None' as the object's name."
msgstr ""
"Se ha añadido un nuevo gesto, \"n\", que verbaliza el nombre del objeto. Si "
"no hay ninguno asignado, se verbaliza la palabra \"None\" como nombre del "
"objeto."

#. type: Bullet: '* '
msgid ""
"DTK no longer adds duplicate settings panels in the NVDA settings window "
"when reloading add-ons."
msgstr ""
"DTK ya no añade paneles de opciones duplicados en la ventana de opciones de "
"NVDA cuando se recargan los complementos."

#. type: Title ###
#, no-wrap
msgid "2019.1.2"
msgstr "2019.1.2"

#. type: Bullet: '* '
msgid "DTK will now report size and position values if they are 0."
msgstr "DTK ahora anuncia valores de posición y tamaño si estos son 0."

#. type: Bullet: '* '
msgid "Navigation now honors the detailed messages setting."
msgstr "La navegación ahora respeta el ajuste de mensajes detallados."

#. type: Bullet: '* '
msgid "Made reporting of size/position information more concise."
msgstr "El anuncio de información de tamaño y posición ahora es más conciso."

#. type: Bullet: '* '
msgid "Stability improvements."
msgstr "Mejoras de estabilidad."

#. type: Title ###
#, no-wrap
msgid "2019.1.1"
msgstr "2019.1.1"

#. type: Bullet: '* '
msgid ""
"DTK will no longer attempt to load itself multiple times when announcing the "
"version number."
msgstr ""
"DTK ya no intentará cargarse varias veces a sí mismo al anunciar el número "
"de versión."

#. type: Bullet: '* '
msgid ""
"DTK features will be disabled on install. Previously, DTK features were "
"enabled on install. This is different than enabling or disabling the add-on "
"in the NVDA toolls>manage add-ons window."
msgstr ""
"Las funciones de DTK vendrán desactivadas al instalar. Anteriormente, las "
"funciones de DTK se activaban al instalar. Esto es distinto a activar o "
"desactivar el complemento desde las herramientas de NVDA > ventana Gestionar "
"complementos."

#. type: Bullet: '* '
msgid "Messages presented to the user can now contain non-ascii characters."
msgstr ""
"Los mensajes presentados al usuario ahora pueden contener caracteres no "
"ASCII."

#. type: Bullet: '* '
msgid ""
"Pressing gestures such as a, b, c, f, h, l, r, s, t, v, and w no longer "
"interupt NVDA speech when copying to the clipboard."
msgstr ""
"Al pulsar gestos como a, b, c, f, h, l, r, s, t, v y w ya no se interrumpe "
"la voz de NVDA si se copia información al portapapeles."

#. type: Title ###
#, no-wrap
msgid "2019.1"
msgstr "2019.1"

#. type: Bullet: '* '
msgid ""
"Fixed a compatibility problem where DTK declared a minimum NVDA version that "
"hasn't been released yet."
msgstr ""
"Corregido un problema de compatibilidad por el que DTK declaraba una versión "
"mínima de NVDA que todavía no había sido publicada."

#. type: Title ###
#, no-wrap
msgid "2019.0 (initial stable release)"
msgstr "2019.0 (versión estable inicial)"

#. type: Bullet: '* '
msgid "Initial build with basic navigation."
msgstr "Compilación inicial con navegación básica."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=devtoolkit"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=devtoolkit"

#. type: Plain text
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=devtoolkit-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=devtoolkit-dev"

#~ msgid ""
#~ "DTK helps developers create user interfaces. This add-on is not an "
#~ "analytical tool for third-party user interfaces."
#~ msgstr ""
#~ "DTK ayuda a los desarrolladores a crear interfaces de usuario. Este "
#~ "complemento no es una herramienta de análisis para interfaces de usuario "
#~ "de terceros."

#~ msgid ""
#~ "When using Chrome, not all web elements will appear in the accessibility "
#~ "tree. To force an element to appear in the accessibility tree, give it a "
#~ "title attribute."
#~ msgstr ""
#~ "Al usar Chrome, no todos los elementos web aparecerán en el árbol de "
#~ "accesibilidad. Para forzar que un elemento aparezca en el árbol de "
#~ "accesibilidad, dale un atributo title."

#~ msgid ""
#~ "When using Firefox, phantom elements may appear in the accessibility "
#~ "tree. For example, a text frame may appear as a text block’s container. "
#~ "These phantom elements are a part of Mozilla’s implementation of the "
#~ "accessibility tree."
#~ msgstr ""
#~ "Al usar Firefox, podrían aparecer elementos fantasma en el árbol de "
#~ "accesibilidad. Por ejemplo, un marco de texto puede aparecer como un "
#~ "contenedor de bloques de texto. Estos elementos fantasma forman parte de "
#~ "la implementación del árbol de accesibilidad de Mozilla."

#~ msgid ""
#~ "Edge has not been completely tested. Therefore, anything reported by the "
#~ "add-on should be considered with care."
#~ msgstr ""
#~ "Edge no se ha probado completamente. Por lo tanto, cualquier cosa que "
#~ "anuncie el complemento debería tratarse con cuidado."

#~ msgid ""
#~ "In web content, everything except a text block is a container. For "
#~ "instance, a paragraph (p tag) may have multiple elements inside."
#~ msgstr ""
#~ "En el contenido web, todo a excepción de un bloque de texto es un "
#~ "contenedor. Por ejemplo, un párrafo (etiqueta p) puede contener varios "
#~ "elementos dentro."

#~ msgid "div tags are reported as a section in HTML5."
#~ msgstr "las etiquetas div son anunciadas como secciones en HTML5."

#~ msgid ""
#~ "To avoid names of web elements appearing as \"None\", always give "
#~ "elements a title attribute."
#~ msgstr ""
#~ "Para evitar que los nombres de los elementos web aparezcan como \"None\", "
#~ "da siempre a los elementos un atributo title."

#~ msgid ""
#~ "Font information is only available in web content. This should be fixed "
#~ "in a future version."
#~ msgstr ""
#~ "La información de fuente sólo se encuentra disponible en el contenido "
#~ "web. Esto debería corregirse en una futura versión."

#~ msgid ""
#~ "The add-on does not teach a user proper user interface/web content design "
#~ "concepts."
#~ msgstr ""
#~ "El complemento no enseña al usuario conceptos adecuados de diseño de "
#~ "contenido web o interfaces de usuario."

#~ msgid ""
#~ "Users are not automatically notified of the enabled/disabled state of the "
#~ "add-on's features when switching between windows."
#~ msgstr ""
#~ "Los usuarios no son notificados automáticamente del estado de activación "
#~ "o desactivación de las funciones de DTK al cambiar de ventana."

#~ msgid ""
#~ "The margins of a control are only available in web content, and are "
#~ "browser dependent."
#~ msgstr ""
#~ "Los márgenes de un control sólo están disponibles en el contenido web, y "
#~ "dependen del navegador."

#~ msgid ""
#~ "The border and padding attributes are not available. This is a long-"
#~ "standing issue."
#~ msgstr ""
#~ "Los atributos de borde y relleno no están disponibles. Este es un "
#~ "problema de larga duración."

#~ msgid ""
#~ "There is no way to restrict DTK to a specific content type or application "
#~ "window."
#~ msgstr ""
#~ "No hay forma de restringir DTK a un tipo de contenido concreto o ventana "
#~ "de aplicación."

#~ msgid ""
#~ "The font information when pressing F is messy, and will get fixed in a "
#~ "future version."
#~ msgstr ""
#~ "La información de fuente que se obtiene al pulsar la f está desordenada, "
#~ "pero esto se corregirá en próximas versiones."

#~ msgid "Contact information"
#~ msgstr "Información de contacto"

#~ msgid ""
#~ "I am available on most blindness related programming lists such as "
#~ "program-l, nvda addon developers list, the nvda-dev list, and the NFB cs "
#~ "list. In the event I am not available there, you can reach me at ajborka "
#~ "(at) gmail (dot) com."
#~ msgstr ""
#~ "El autor está disponible en la mayoría de listas inglesas relacionadas "
#~ "con ceguera y programación como program-l, la lista de desarrolladores de "
#~ "complementos de NVDA, la lista nvda-devel, y la lista NFB cs. En caso de "
#~ "que no se encuentre disponible, puedes contactar con él escribiendo a "
#~ "ajborka@gmail.com."

#~ msgid "Documentation"
#~ msgstr "Documentación"

#~ msgid ""
#~ "The NVDA developer toolkit assists blind and visually impaired developers "
#~ "with the difficult task of creating appealing visual layouts for any "
#~ "software or web development project. It does this by providing the "
#~ "following about an object or control:"
#~ msgstr ""
#~ "El kit del desarrollador de NVDA asiste a los desarrolladores ciegos o "
#~ "deficientes visuales en la difícil tarea de crear diseños visualmente "
#~ "atractivos en cualquier proyecto de desarrollo software o web. Esto lo "
#~ "hace proporcionando lo siguiente sobre un objeto o control:"

#~ msgid "Object location (in pixels)"
#~ msgstr "Posición del objeto (en píxeles)"

#~ msgid "left edge"
#~ msgstr "borde izquierdo"

#~ msgid "top edge"
#~ msgstr "borde superior"

#~ msgid "right edge"
#~ msgstr "borde derecho"

#~ msgid "bottom edge"
#~ msgstr "borde inferior"

#~ msgid "top-left corner"
#~ msgstr "esquina superior izquierda"

#~ msgid "top-right corner"
#~ msgstr "esquina superior derecha"

#~ msgid "bottom-left corner"
#~ msgstr "esquina inferior izquierda"

#~ msgid "bottom-right corner"
#~ msgstr "esquina inferior derecha"

#~ msgid "absolute center"
#~ msgstr "centro absoluto"

#~ msgid "height"
#~ msgstr "altura"

#~ msgid "width"
#~ msgstr "anchura"

#~ msgid ""
#~ "Developer tools will work with Firefox, Chrome, and Internet Explorer. "
#~ "However, to make developer tools work well with Chrome and Internet "
#~ "Explorer, you must add the following at the top of all CSS files: * {box-"
#~ "sizing: box-border; }. This allows Chrome and Internet Explorer to "
#~ "include borders, padding, and element content as part of the element's "
#~ "total size."
#~ msgstr ""
#~ "Las herramientas del desarrollador funcionarán con Firefox, Chrome e "
#~ "Internet Explorer. Sin embargo, para conseguir que funcionen bien con "
#~ "Chrome e Internet Explorer, debes añadir lo siguiente al principio de "
#~ "todos los archivos css: `* {box-sizing: box-border; }`. Esto permite a "
#~ "Chrome e Internet Explorer incluir bordes, relleno y contenido del "
#~ "elemento como parte del tamaño total del elemento."

#~ msgid "Keyboard shortcuts"
#~ msgstr "Atajos de teclado"

#~ msgid ""
#~ "The developer toolkit makes use of the keyboard combination CONTROL"
#~ "+WINDOWS as its modifier key. Any shortcuts listed must include the "
#~ "toolkit modifier key to work properly"
#~ msgstr ""
#~ "El kit del desarrollador hace uso de la combinación de teclado ctrl"
#~ "+windows como tecla modificadora. Cualquier atajo de los que se listan "
#~ "debe incluir la tecla modificadora del kit de herramientas para funcionar "
#~ "adecuadamente"

#~ msgid "Desktop shortcuts (see below for laptop shortcuts)"
#~ msgstr ""
#~ "Atajos de teclado de escritorio (mira más abajo para ver los atajos de "
#~ "teclado portátiles)"

#~ msgid "numpad1: Obtain the location of the bottom-left corner."
#~ msgstr ""
#~ "1 del teclado numérico: Obtener la ubicación de la esquina inferior "
#~ "izquierda."

#~ msgid "numpad2: Obtain the location of the bottom edge"
#~ msgstr "2 del teclado numérico: Obtener la ubicación del borde inferior"

#~ msgid "numpad3: Obtain the location of the bottom-right corner."
#~ msgstr ""
#~ "3 del teclado numérico: Obtener la posición de la esquina inferior "
#~ "derecha."

#~ msgid "numpad4: Obtain the location of the left edge."
#~ msgstr "4 del teclado numérico: Obtener la ubicación del borde izquierdo."

#~ msgid "numpad5: Obtain the location of the absolute center."
#~ msgstr "5 del teclado numérico: Obtener la ubicación del centro absoluto."

#~ msgid "numpad6: Obtain the location of the right edge."
#~ msgstr "6 del teclado numérico: Obtener la ubicación del borde derecho."

#~ msgid "numpad7: Obtain the location of the top-left corner."
#~ msgstr ""
#~ "7 del teclado numérico: Obtener la ubicación de la esquina superior "
#~ "izquierda."

#~ msgid "numpad8: Obtain the location of the top edge"
#~ msgstr "8 del teclado numérico: Obtener la ubicación del borde superior"

#~ msgid "numpad9: Obtain the location of the top-right corner."
#~ msgstr ""
#~ "9 del teclado numérico: Obtener la ubicación de la esquina superior "
#~ "derecha."

#~ msgid "numpad plus: Obtain the height."
#~ msgstr "más del telcado numérico: Obtener la altura."

#~ msgid "numpad enter: Obtain the width."
#~ msgstr "intro del teclado numérico: Obtener la anchura."

#~ msgid "Laptop shortcuts"
#~ msgstr "Atajos de teclado portátil"

#~ msgid "m: Obtain the location of the bottom-left corner."
#~ msgstr "m: Obtener la ubicación de la esquina inferior izquierda."

#~ msgid "comma (,): Obtain the location of the bottom edge."
#~ msgstr "coma (,): Obtener la ubicación del borde inferior."

#~ msgid "period (.): Obtain the location of the bottom-right corner."
#~ msgstr "punto (.): Obtener la ubicación de la esquina inferior derecha."

#~ msgid "j: Obtain the location of the left edge."
#~ msgstr "j: Obtener la ubicación del borde izquierdo."

#~ msgid "k: Obtain the location of the absolute center."
#~ msgstr "k: Obtener ubicación del centro absoluto."

#~ msgid "l: Obtain the location of the left edge."
#~ msgstr "l: Obtener la ubicación del borde derecho."

#~ msgid "u: Obtain the location of the top-left corner."
#~ msgstr "u: Obtener la ubicación de la esquina superior izquierda."

#~ msgid "i: Obtain the location of the top edge."
#~ msgstr "i: Obtener la ubicación del borde superior."

#~ msgid "o: Obtain the location of the top-right corner."
#~ msgstr "o: Obtener la ubicación de la esquina superior derecha."

#~ msgid "[: Obtain the height."
#~ msgstr "[: Obtener la altura."

#~ msgid "]: Obtain the width."
#~ msgstr "]: Obtener la anchura."

#~ msgid ""
#~ "The NVDA developer toolkit assists blind and visually impaired developers "
#~ "with the difficult task of creating appealing visual layouts for any "
#~ "software or web development project. Currently, it provides the location "
#~ "of objects on the visible portion of the screen. The addon provides the "
#~ "location in pixels. The anchor for each object is the top left corner in "
#~ "relation to the top left corner of the window or screen. For example, if "
#~ "the addon reports left: 15, top: 100, this means that the object with "
#~ "focus is 15 pixels from the left edge of the screen and 100 pixels from "
#~ "the top edge of the screen. The width and height of the control is also "
#~ "provided in pixels. This accounts for the content, padding, and margins "
#~ "of each control involved."
#~ msgstr ""
#~ "El kit del desarrollador de NVDA asiste a los desarrolladores ciegos o "
#~ "deficientes visuales en la difícil tarea de crear diseños visualmente "
#~ "atractivos de cualquier proyecto de desarrollo web o software. "
#~ "Actualmente, proporciona la ubicación de los objetos visibles en "
#~ "pantalla. El complemento proporciona esta ubicación en píxeles. El punto "
#~ "de inicio de cada objeto es la esquina superior izquierda en relación con "
#~ "la esquina superior izquierda de la ventana o pantalla. Por ejemplo, si "
#~ "el complemento indica izquierda: 15, arriba: 100, significa que el objeto "
#~ "con el foco está a 15 píxeles del borde izquierdo de la pantalla y a 100 "
#~ "del borde superior. La anchura y altura del control también se "
#~ "proporcionan en píxeles. Esto cuenta el contenido, relleno y márgenes de "
#~ "cada control involucrado."

#~ msgid ""
#~ "NVDA+SHIFT+NUMPAD DELETE: Obtain size/location information of focused "
#~ "item or current navigator object."
#~ msgstr ""
#~ "NVDA+shift+suprimir del teclado numérico: obtener información de tamaño y "
#~ "posición del elemento con el foco o el objeto bajo el navegador de "
#~ "objetos."
