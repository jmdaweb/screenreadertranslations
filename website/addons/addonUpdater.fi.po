# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Add-on Updater\n"
"POT-Creation-Date: 2021-12-04 20:14+0000\n"
"PO-Revision-Date: 2021-12-05 12:38+0200\n"
"Last-Translator: Jani Kinnunen <janikinnunen340@gmail.com>\n"
"Language-Team: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Add-on Updater\"]]\n"
msgstr "[[!meta title=\"Lisäosien päivittäjä\"]]\n"

#. type: Bullet: '* '
msgid "Author: Joseph Lee"
msgstr "Tekijä: Joseph Lee"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "Lataa [vakaa versio][1]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2021.2 and later"
msgstr "Yhteensopivuus: NVDA 2021.2 ja uudemmat"

#. type: Plain text
msgid ""
"This add-on brings NVDA Core issue 3208 to life: ability to check for, "
"download, and apply add-on updates."
msgstr ""
"Tämä lisäosa tuo NVDA:han  mahdollisuuden tarkistaa, ladata ja asentaa "
"lisäosien päivityksiä."

#. type: Plain text
msgid ""
"To check for updates after installing this add-on, go to NVDA menu/Tools/"
"Check for add-on updates. If updates are available, a list of add-on updates "
"will be shown, with each entry consisting of description, current version, "
"and the new version. Select Update, and NVDA will download and apply updates "
"in sequence, with a prompt to restart your NVDA shown afterwards."
msgstr ""
"Päivitykset tarkistetaan tämän lisäosan asentamisen jälkeen valitsemalla "
"NVDA-valikosta Työkalut/Tarkista lisäosien päivitykset. Jos päivityksiä on "
"saatavilla, niistä näytetään luettelo. Luettelon kohdat koostuvat "
"kuvauksesta sekä nykyisestä ja uudesta versiosta. NVDA lataa ja asentaa "
"päivitykset järjestyksessä, kun painetaan Päivitä-painiketta, ja asennuksen "
"jälkeen näytetään kehote uudelleenkäynnistämisestä."

#. type: Plain text
msgid ""
"The following add-ons provide built-in update feature and thus updates will "
"not be checked via this add-on:"
msgstr ""
"Seuraavissa lisäosissa on sisäänrakennettu päivitystoiminto, joten tämä "
"lisäosa ei tarkista niiden päivityksiä:"

#. type: Bullet: '* '
msgid "Braille Extender"
msgstr "Braille Extender"

#. type: Bullet: '* '
msgid "WeatherPlus"
msgstr "WeatherPlus"

#. type: Plain text
msgid "IMPORTANT NOTES:"
msgstr "TÄRKEITÄ HUOMAUTUKSIA:"

#. type: Bullet: '* '
msgid ""
"This is a proof of concept add-on. Once the [relevant feature is included in "
"NVDA][2], this add-on will be discontinued."
msgstr ""
"Tämä on konseptilisäosa. Sen kehitys lopetetaan, kun [kyseinen ominaisuus]"
"[2] on lisätty NVDA:han."

#. type: Bullet: '* '
msgid ""
"If the new add-on updates specify a compatibility range (minimum and last "
"tested NVDA versions) and if the NVDA version you are running does not fall "
"within the compatibility range according to NVDA, add-on updating will not "
"proceed."
msgstr ""
"Mikäli uusissa lisäosapäivityksissä on määritetty yhteensopivuusalue (NVDA:n "
"vähimmäis- ja viimeisin testattu versio) eikä käyttämäsi NVDA:n versio kuulu "
"siihen, päivitystä ei jatketa."

#. type: Bullet: '* '
msgid ""
"Not all add-ons come with development releases. If you are not getting "
"updates after choosing to install development versions of an add-on, switch "
"to stable channel for affected add-ons."
msgstr ""
"Kaikista lisäosista ei julkaista kehitysversioita. Mikäli et saa päivityksiä "
"valittuasi asennettavaksi lisäosan kehitysversion, vaihda takaisin vakaaseen "
"päivityskanavaan."

#. type: Bullet: '* '
msgid ""
"On some systems (particularly computers joined to a corporate domain), add-"
"on update check functionality may not work properly, therefore add-on "
"updates must be downloaded manually."
msgstr ""
"Joissakin järjestelmissä (etenkin yrityksen toimialueeseen liitetyissä) "
"Lisäosien päivitystarkistustoiminto ei välttämättä toimi kunnolla, joten "
"lisäosien päivitykset on ladattava manuaalisesti."

#. type: Title ##
#, no-wrap
msgid "Version 22.01"
msgstr "Versio 22.01"

#. type: Bullet: '* '
msgid "NVDA 2021.2 or later is required."
msgstr "Edellyttää NVDA 2021.2:ta tai uudempaa."

#. type: Bullet: '* '
msgid ""
"On server systems running Windows Server 2016 and later, add-on updates will "
"be presented in a dialog instead of using toast notifications."
msgstr ""
"Palvelinjärjestelmissä, joissa on Windows Server 2016 tai uudempi, "
"lisäosapäivitykset näytetään valintaikkunoina ilmoitusruutujen asemesta."

#. type: Title ##
#, no-wrap
msgid "Version 21.10"
msgstr "Versio 21.10"

#. type: Bullet: '* '
msgid ""
"It is again possible to check for add-on updates on some systems, notably "
"after a clean Windows installation."
msgstr ""
"Lisäosapäivitysten tarkistus on taas mahdollista tietyissä järjestelmissä, "
"etenkin puhtaan Windows-asennuksen jälkeen."

#. type: Title ##
#, no-wrap
msgid "Version 21.09"
msgstr "Versio 21.09"

#. type: Bullet: '* '
msgid "NVDA 2021.1 or later is required."
msgstr "Edellyttää NVDA 2021.1:tä tai uudempaa."

#. type: Bullet: '* '
msgid ""
"on Windows 10 and later, it is possible to select add-on update notification "
"between a toast message and an update dialog. This can be configured from "
"Add-on Updater settings found in NVDA Settings screen."
msgstr ""
"Windows 10:ssä ja uudemmissa lisäosapäivityksen ilmoitus on mahdollista "
"valita ilmoitusruudun ja valintaikkunan väliltä. Tämä voidaan määrittää "
"Lisäosien päivittäjän asetuksista, jotka löytyvät NVDA:n asetusruudusta."

#. type: Bullet: '* '
msgid ""
"Add-on Updater will no longer check minimum Windows release information for "
"add-ons as add-ons such as Windows App Essentials provide better Windows "
"compatibility information."
msgstr ""
"Parempien yhteensopivuustietojen tarjoamisen vuoksi Lisäosien päivittäjä ei "
"enää tarkista Windows-version vähimmäisvaatimusta sellaisilta lisäosilta "
"kuin Windows App Essentials."

#. type: Title ##
#, no-wrap
msgid "Version 21.07"
msgstr "Versio 21.07"

#. type: Bullet: '* '
msgid ""
"On Windows 10 and later, a toast notification will be shown when add-on "
"updates are available. Note that you cannot click this notification - you "
"must open NvDA menu/Tools/Check for add-on updates to review updates."
msgstr ""
"Windows 10:ssä ja uudemmissa näytetään ilmoitusruutu, kun lisäosiin on "
"saatavilla päivityksiä. Huomaa, että tätä ilmoitusta ei voi napsauttaa. "
"Sinun on avattava NVDA-valikosta Työkalut-alivalikko ja valittava sitten "
"Tarkista lisäosien päivitykset tarkastellaksesi päivityksiä."

#. type: Bullet: '* '
msgid ""
"When legacy add-ons dialog is shown at startup, you can now review legacy "
"add-ons and reasons just like add-on updates."
msgstr ""
"Kun vanhentuneiden lisäosien valintaikkuna näytetään käynnistettäessä, voit "
"nyt tarkastella niitä ja vanhentumisen syitä aivan kuten päivityksiäkin."

#. type: Bullet: '* '
msgid ""
"Improved add-on update check internals, including use of add-on metadata "
"collection provided by the community to validate add-on compatibility. Among "
"other things, this eliminates add-on releases for adding update checks for "
"new add-ons."
msgstr ""
"Lisäosien päivitystarkistuksen toimintaa paranneltu, mukaan lukien yhteisön "
"tarjoaman lisäosien metatietokokoelman käyttö yhteensopivuuden "
"varmistamiseksi. Tämä muun muassa karsii lisäosat, joissa on oma "
"päivitystentarkistustoiminto."

#. type: Title ##
#, no-wrap
msgid "Version 21.05"
msgstr "Versio 21.05"

#. type: Bullet: '* '
msgid ""
"NvDA will no longer play error tones if trying to check updates while using "
"NVDA 2021.1 alpha snapshots, caused by changes to wxPython GUI toolkit."
msgstr ""
"NVDA ei enää toista virheääniä yritettäessä tarkistaa päivityksiä NVDA "
"2021.1:n alfa-kehitysversioita käytettäessä, jonka aiheuttivat wxPython-"
"käyttöliittymätyökalupakkiin tehdyt muutokset."

#. type: Title ##
#, no-wrap
msgid "Version 21.03"
msgstr "Versio 21.03"

#. type: Bullet: '* '
msgid "NVDA 2020.4 or later is required."
msgstr "Edellyttää NVDA 2020.4:ää tai uudempaa."

#. type: Bullet: '* '
msgid ""
"NVDA will present an error dialog if errors occur while checking for add-on "
"updates such as loss of Internet connection."
msgstr ""
"NVDA näyttää virheilmoituksen, mikäli lisäosapäivityksiä tarkistettaessa "
"tapahtui virheitä, kuten internet-yhteyden katkeaminen."

#. type: Title ##
#, no-wrap
msgid "Version 20.11"
msgstr "Versio 20.11"

#. type: Bullet: '* '
msgid "NVDA 2020.3 or later is required."
msgstr "Edellyttää NVDA 2020.3:a tai uudempaa."

#. type: Bullet: '* '
msgid "Resolved more coding style issues and potential bugs with Flake8."
msgstr ""
"Ratkaistu lisää koodaustyylin ongelmia sekä mahdollisia bugeja Flake8:n "
"kanssa."

#. type: Bullet: '* '
msgid ""
"NVDA will no longer play error tones or appear to do nothing when using the "
"add-on while NVDA is running from source code. A message about this fact "
"will be recorded in the log instead."
msgstr ""
"NVDA ei enää toista virheääniä eikä näytä tekevän mitään, kun lisäosaa "
"käytetään NVDA:n lähdekoodiversiossa. Tieto tästä tallennetaan lokiin."

#. type: Title ##
#, no-wrap
msgid "Version 20.07"
msgstr "Versio 20.07"

#. type: Bullet: '* '
msgid "NVDA 2020.1 or later is required."
msgstr "Edellyttää NVDA 2020.1:tä tai uudempaa."

#. type: Bullet: '* '
msgid ""
"If one or more legacy add-ons (such as Screen Curtain) are installed, Add-on "
"Updater will present a message asking you to disable or uninstall the add-"
"ons listed."
msgstr ""
"Jos yksi tai useampi vanhentunut lisäosa (kuten Näyttöverho) on asennettuna, "
"Lisäosien päivittäjä näyttää ilmoituksen, jossa sinua pyydetään poistamaan "
"luettelossa näkyvät lisäosat käytöstä."

#. type: Bullet: '* '
msgid ""
"You can now save, reload, or reset Add-on Updater settings by pressing "
"Control+NVDA+C, Control+NVDA+R once, or Control+NVDA+R three times, "
"respectively."
msgstr ""
"Voit nyt tallentaa, ladata uudelleen tai nollata Lisäosan päivittäjän "
"asetukset painamalla Ctrl+NVDA+C, Ctrl+NVDA+R kerran tai Ctrl+NVDA+R "
"kolmesti."

#. type: Title ##
#, no-wrap
msgid "Version 20.06"
msgstr "Versio 20.06"

#. type: Bullet: '* '
msgid "Resolved many coding style issues and potential bugs with Flake8."
msgstr ""
"Ratkaistu useita koodaustyylin ongelmia sekä mahdollisia bugeja Flake8:n "
"kanssa."

#. type: Title ##
#, no-wrap
msgid "Version 20.04"
msgstr "Versio 20.04"

#. type: Bullet: '* '
msgid ""
"NVDA will no longer appear to do nothing or play error tones when trying to "
"update add-ons through Add-on Updater."
msgstr ""
"NVDA ei näytä enää tekevän mitään tai toistavan virheääniä, kun lisäosia "
"yritetään päivittää Lisäosien päivittäjän kautta."

#. type: Bullet: '* '
msgid ""
"Resolved an issue where \"check for add-on updates\" item wasn't present in "
"NVDA Tools menu."
msgstr ""
"Ratkaistu ongelma, joka aiheutti sen, että \"Tarkista lisäosien päivitykset"
"\" -vaihtoehtoa ei ollut NVDA:n Työkalut-valikossa."

#. type: Title ##
#, no-wrap
msgid "Version 20.03"
msgstr "Versio 20.03"

#. type: Bullet: '* '
msgid "NVDA 2019.3 or later is required."
msgstr "Edellyttää NVDA 2019.3:a tai uudempaa."

#. type: Bullet: '* '
msgid ""
"When installing add-on updates, Add-on Updater will no longer check for "
"compatibility range. NVDA itself will check add-on compatibility."
msgstr ""
"Lisäosien päivittäjä ei enää tarkista yhteensopivuusaluetta lisäosien "
"päivityksiä asennettaessa. NVDA tarkistaa itse yhteensopivuuden."

#. type: Title ##
#, no-wrap
msgid "Version 19.11"
msgstr "Versio 19.11"

#. type: Bullet: '* '
msgid ""
"When add-on updates are available, NVDA will announce how many updates are "
"available."
msgstr "Kun lisäosien päivityksiä on saatavilla, NVDA ilmoittaa niiden määrän."

#. type: Title ##
#, no-wrap
msgid "Version 19.09"
msgstr "Versio 19.09"

#. type: Bullet: '* '
msgid "Requires NVDA 2019.2 or later."
msgstr "Edellyttää NVDA 2019.2:ta tai uudempaa."

#. type: Bullet: '* '
msgid ""
"Timeout errors seen when attempting to download some add-on updates (notably "
"add-on files hosted on GitHub) has been resolved."
msgstr ""
"Ratkaistu ongelma, joka aiheutti aikakatkaisuvirheitä yritettäessä ladata "
"joidenkin lisäosien (varsinkin GitHubissa ylläpidettävien) päivityksiä."

#. type: Title ##
#, no-wrap
msgid "Version 19.04"
msgstr "Versio 19.04"

#. type: Bullet: '* '
msgid "Requires NVDA 2019.1 or later."
msgstr "Edellyttää NVDA 2019.1:tä tai uudempaa."

#. type: Bullet: '* '
msgid ""
"When installing add-on updates, both minimum and last tested versions will "
"be checked."
msgstr ""
"NVDA:n vähimmäis- ja viimeisin testattu versio tarkistetaan lisäosia "
"asennettaessa."

#. type: Title ##
#, no-wrap
msgid "Version 19.01"
msgstr "Versio 19.01"

#. type: Bullet: '* '
msgid "Requires NVDA 2018.4 or later."
msgstr "Edellyttää NVDA 2018.4:ää tai uudempaa."

#. type: Bullet: '* '
msgid "Improved responsiveness when checking for add-on updates."
msgstr "Herkkyyttä parannettu lisäosia päivitettäessä."

#. type: Bullet: '* '
msgid "Made the add-on more compatible with Python 3."
msgstr "Lisäosasta tehty yhteensopivampi Python 3:n kanssa."

#. type: Title ##
#, no-wrap
msgid "Version 18.12.2"
msgstr "Versio 18.12.2"

#. type: Bullet: '* '
msgid "Python 3 ready."
msgstr "Python 3 -yhteensopiva."

#. type: Bullet: '* '
msgid ""
"Fixed compatibility with recent NVDA alpha snapshots where add-on updates "
"would not download."
msgstr ""
"Korjattu yhteensopivuus viimeisimpien NVDA:n alfa-kehitysversioiden kanssa, "
"joissa lisäosien päivitykset eivät latautuneet."

#. type: Title ##
#, no-wrap
msgid "Version 18.12.1"
msgstr "Versio 18.12.1"

#. type: Bullet: '* '
msgid "Added localizations."
msgstr "Lokalisointeja lisätty."

#. type: Title ##
#, no-wrap
msgid "Version 18.12"
msgstr "Versio 18.12"

#. type: Bullet: '* '
msgid ""
"Updates for disabled add-ons can be checked. They will stay disabled after "
"updating them."
msgstr ""
"Käytöstä poistettujen lisäosien päivitykset voidaan tarkistaa. Lisäosat "
"pysyvät poissa käytöstä päivittämisen jälkeen."

#. type: Bullet: '* '
msgid ""
"During updates, if an add-on requires specific NVDA version and/or Windows "
"release, these will be checked, and if one of these does not match, an error "
"message will be shown and update will be aborted, resulting in no changes to "
"already installed add-on version."
msgstr ""
"Jos lisäosa vaatii päivitysten aikana tiettyä NVDA:n ja/tai Windowsin "
"versiota, ne tarkistetaan, ja mikäli jokin niistä ei täsmää, virheilmoitus "
"näytetään ja päivitys keskeytetään, jolloin asennettuun lisäosaversioon ei "
"tehdä muutoksia."

#. type: Bullet: '* '
msgid ""
"When automatic update check is enabled and when updates are ready, NVDA will "
"present the updates list instead of asking if you wish to review updates."
msgstr ""
"Kun automaattinen päivitystarkistus on käytössä ja päivityksiä saatavilla, "
"NVDA näyttää päivitysluettelon sen sijaan, että kysyisi, halutaanko "
"päivityksiä tarkastella."

#. type: Title ##
#, no-wrap
msgid "Version 18.10"
msgstr "Versio 18.10"

#. type: Bullet: '* '
msgid "Initial stable release (still marked as proof of concept)."
msgstr "Ensimmäinen vakaa versio (edelleen konseptiasteella)."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=nvda3208"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=nvda3208"

#. type: Plain text
msgid "[2]: https://github.com/nvaccess/nvda/issues/3208"
msgstr "[2]: https://github.com/nvaccess/nvda/issues/3208"
