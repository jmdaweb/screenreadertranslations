# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-04-28 11:11+0000\n"
"PO-Revision-Date: 2021-05-15 02:08+0200\n"
"Last-Translator: Bernd Dorer <mail@bdorer.de>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tony's enhancements\"]]\n"
msgstr "[[!meta title=\"Tony's Verbesserungen\"]]\n"

#. type: Bullet: '* '
msgid "Authors: Tony Malykh"
msgstr "Autoren: Tony Malykh"

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "[Stabile Version herunterladen][1]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2019.3"
msgstr "NVDA-Kompatibilität: 2019.3"

#. type: Plain text
msgid ""
"This add-on contains a number of small improvements to NVDA screen reader, "
"each of them too small to deserve a separate add-on."
msgstr ""
"Diese Erweiterung enthält mehrere kleine Verbesserungen für NVDA, die für "
"sich alleine zu klein für eine eigene Erweiterung sind."

#. type: Plain text
msgid "This add-on is only compatible with NVDA versions 2019.3 and above."
msgstr "Diese Erweiterung ist nur mit NVDA 2019.3 und neuer kompatibel."

#. type: Title ##
#, no-wrap
msgid "Enhanced table navigation commands"
msgstr "Erweiterte Tabellen-Navigationsbefehle"

#. type: Bullet: '* '
msgid "Control+Alt+Home/End - jump to the first/last column in the table."
msgstr ""
"Steuerung+Alt+Pos1/Ende - springe zur ersten bzw. letzten Spalte in einer "
"Tabelle."

#. type: Bullet: '* '
msgid "Control+Alt+PageUp/PageDown - jump to the first/last row in the table."
msgstr ""
"Steuerung+Alt+Seite rauf/runter - springe zur ersten bzw. letzten Reihe in "
"einer Tabelle."

#. type: Bullet: '* '
msgid "NVDA+Control+digit - jump to 1st/2nd/3rd/... 10th column in the table."
msgstr ""
"NVDA+Steuerung+Ziffer - springe zu Spalte 1, 2, 3 bis 10 in einer Tabelle."

#. type: Bullet: '* '
msgid "NVDA+Alt+digit - jump to 1st/2nd/3rd/... 10th row in the table."
msgstr "NVDA+Alt+Ziffer - springe zu Reihe 1, 2, 3 bis 10 in einer Tabelle."

#. type: Bullet: '* '
msgid ""
"NVDA+Shift+DownArrow - read current column in the table starting from "
"current cell down."
msgstr ""
"NVDA+Umschalt+Pfeil runter - Liest die aktuelle Spalte der Tabelle beginnend "
"bei der aktuellen Zelle."

#. type: Title ##
#, no-wrap
msgid "Dynamic keystrokes"
msgstr "Dynamische Tastenkombinationen"

#. type: Plain text
msgid ""
"You can assign certain keystrokes to be dynamic. After issuing such a "
"keystroke, NVDA will be checking currently focused window for any updates "
"and if the line is updated, NVDA will speak it automatically. For example, "
"certain keystrokes in text editors should be marked dynamic, such as Jump to "
"bookmark, jump to another line and debugging keystrokes,such as step into/"
"step over."
msgstr ""
"Sie können bestimmte Tastenkombinationen dynamisch machen. Dies bedeutet, "
"dass NVDA nach solch einer Tastenkombination das Fenster der aktuellen "
"Anwendung auf Aktualisierungen prüft und die aktuelle Zeile vorliest, sofern "
"sich diese Geändert hat. Dies kann z. B. die Tastenkombination springe zu "
"Lesezeichen, springe zu einer bestimmten Zeile  in einem Editor sein."

#. type: Plain text
msgid ""
"The format of dynamic keystrokes table is simple: every line contains a rule "
"in the following format:"
msgstr ""
"Das Format der dynamischen Tastenkürzel ist einfach. Jede Zeile enthält eine "
"Regel in folgendem Format:"

#. type: Plain text
#, no-wrap
msgid ""
"\n"
"appName keystroke\n"
"\n"
msgstr ""
"\n"
" Anwendungsname Tastenkürzel\n"

#. type: Plain text
msgid ""
"where `appName` is the name of the application where this keystroke is "
"marked dynamic (or `*` to b marked dynamic in all applications), "
"and`keystroke` is a keystroke in NVDA format, for example, `control+alt+shift"
"+pagedown`."
msgstr ""
"Hier ist mit  \"Anwendungsname\" der Name der Anwendung gemeint, in welcher "
"die Tastenkombination dynamisch gemacht werden soll. Mit * wird die "
"Tastenkombination in allen Anwendungen dynamisch gemacht. Die "
"\"Tastenkombination\" muss im NVDA-Format geschrieben werden. Beispiel: "
"\"control+alt+shift+pagedown\" für Alt+Steuerung++UmschaltPfeil runter.."

#. type: Title ##
#, no-wrap
msgid "Real-time console output"
msgstr "Echtzeit-Konsolenausgabe"

#. type: Plain text
msgid "This option is disabled by default and must be enabled in the settings."
msgstr ""
"Diese Option ist standardmäßig ausgeshaltet und muss manuell in den "
"Einstellungen aktiviert werden."

#. type: Plain text
msgid ""
"This option makes NVDA to speak new lines immediately as they appear in "
"console output, instead of queueing new speech utterances."
msgstr ""
"Diese Option bringt NVDA dazu, sofort neue Zeilen zu sprechen, sobald diese "
"in der Kommandozeile erscheinen, anstatt neue Zeilen nacheinander vorzulesen."

#. type: Plain text
msgid ""
"There is also an option to beep on command line updates - this would give "
"you a better idea when new lines are printed in the console."
msgstr ""
"Ebenso gibt es eine Option, die signalisiert, sobald die Kommandozeile "
"aktualisiert wird, sodass Sie dies gleich mitbekommen, auch wenn die "
"Sprachausgabe noch nicht ausgesprochen hat."

#. type: Title ##
#, no-wrap
msgid "Beep when NVDA is busy"
msgstr "Signalisiere, wenn NVDA ausgelastet ist"

#. type: Plain text
msgid ""
"Check this option for NVDA to provide audio feedback when NVDA is busy. NVDA "
"being busy does not necessarily indicate a problem with NVDA, but rather "
"this is a signal to the user that any NVDA commands will not be processed "
"immediately."
msgstr ""
"Aktivieren Sie diese Option, wenn NVDA signalisieren soll, dass es "
"beschäftigt ist. In diesem Fall gibt es nicht unbedingt ein Problem mit "
"NVDA, zeigt dem Nutzer jedoch an, dass der nächste NVDA-Befehl nicht sofort "
"ausgeführt wird."

#. type: Title ##
#, no-wrap
msgid "NVDA volume"
msgstr "NVDA-Lautstärke"

#. type: Bullet: '* '
msgid "NVDA+Control+PageUp/PageDown - adjust NVDA volume."
msgstr "NVDA+Steuerung+Seite rauf/runter - NVDA-Lautstärke anpassen."

#. type: Plain text
msgid ""
"This option controls the volume of NVDA speech as well as all the other "
"sounds and beeps produced by NVDA. The advantage of this option compared to "
"adjusting volume of a speech synthesizer, is that it affects the volume of "
"all the beeps proportionally."
msgstr ""
"Diese Option kontrolliert die Lautstärke der Sprache, Signaltöne und Sounds, "
"die von NVDA  ausgegeben werden. Der Vorteil dieser  Option im Vergleich zur "
"Lautstärkenänderung in den Stimmeinstellungen ist, dass die Signaltöne und "
"Sounds mit angepasst werden."

#. type: Title ##
#, no-wrap
msgid "Blocking double insert keystroke"
msgstr "Zweimaligen Druck der Taste einfügen blockieren"

#. type: Plain text
msgid ""
"In NVDA pressing Insert key twice in a row toggles insert mode in "
"applications. However, sometimes it happens accidentally and it triggers "
"insert mode. Since this is a special keystroke, it cannot be disabled in the "
"settings. This add-on provides a way to block this keyboard shortcut. When "
"double insert is blocked, insert mode can stil be toggled by pressing NVDA"
"+F2 and then Insert."
msgstr ""
"Während der Nutzung von NVDA bewirkt das zweimalige Drücken der Einfügen-"
"Taste hintereinander den Wechsel zwischen Einfügen und Überschreiben, sofern "
"die aktuelle Anwendung dies unterstützt. Manchmal geschieht dies jedoch "
"versehentlich und löst den Überschreibmodus aus. Da es sich um einen "
"speziellen Tastenanschlag handelt, kann er in den Einstellungen nicht "
"deaktiviert werden. Diese Erweiterung bietet eine Möglichkeit, diese "
"Tastenkombination zu blockieren. Wenn der Doppeldruck blockiert ist, kann "
"der Einfügemodus durch Drücken von NVDA+F2 und betätigen der Einfügen-Taste "
"umgeschaltet werden."

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=tony"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=tony"

#~ msgid "```"
#~ msgstr "```"
