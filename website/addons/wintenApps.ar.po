# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2022-01-21 05:10+0000\n"
"PO-Revision-Date: 2021-07-01 19:15-0700\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.9\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Windows App Essentials\"]]\n"
msgstr "[[!meta title=\"Windows App Essentials\"]]\n"

#. type: Bullet: '* '
msgid "Authors: Joseph Lee, Derek Riemer and others"
msgstr ""

#. type: Bullet: '* '
msgid "Download [stable version][1]"
msgstr "تحميل [الإصدار النهائي][2]"

#. type: Bullet: '* '
msgid "Download [development version][2]"
msgstr "تحميل [الإصدار التجريبي][2]"

#. type: Bullet: '* '
msgid "NVDA compatibility: 2021.2 and later"
msgstr ""

#. type: Plain text
msgid ""
"Note: Originally called Windows 10 App Essentials, it was renamed to Windows "
"App Essentials in 2021 to support Windows 10 and future releases such as "
"Windows 11. Parts of this add-on will still refer to the original add-on "
"name."
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid ""
#| "This add-on is a collection of app modules for various Windows 10 apps, "
#| "as well as fixes for certain windows 10 controls."
msgid ""
"This add-on is a collection of app modules for various modern Windows apps, "
"as well as enhancements and fixes for certain controls found in Windows 10 "
"and later."
msgstr ""
"هذه الإضافة هي عبارة عن مجموعة من الملحقات البرمجية لتطبيقات متنوعة بويندوز "
"10 فضلا عن معالجة لبعض الأخطاء البرمجية لبعض البرامج"

#. type: Plain text
msgid ""
"The following app modules or support modules for some apps are included (see "
"each app section for details on what is included):"
msgstr ""
"تحتوي الإضافة على الملحقات البرمجية التالية (يرجى النظر في كل قسم تطبيق معين "
"للمزيد من التفاصيل حول ما تحتويه الإضافة من ملحقات عن هذا التطبيق):"

#. type: Title ##
#, no-wrap
msgid "Calculator"
msgstr "حاسبة"

#. type: Title ##
#, no-wrap
msgid "Cortana"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Mail"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Maps"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Microsoft Solitaire Collection"
msgstr ""

#. type: Bullet: '* '
msgid ""
"Modern keyboard (emoji panel/dictation/voice typing/hardware input "
"suggestions/clipboard history/modern input method editors)"
msgstr ""

#. type: Bullet: '* '
msgid "Notepad (Windows 11)"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "People"
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid "Settings (system settings, Windows+I)."
msgid "Settings (system settings, Windows+I)"
msgstr "إعدادات, (إعدادات النظام Windows+I)."

#. type: Title ##
#, no-wrap
msgid "Weather"
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid "Miscellaneous modules for controls such as Start Menu tiles."
msgid "Miscellaneous modules for controls such as Start Menu tiles"
msgstr "وحدات متنوعة للكائنات كعناصر قائمة ابدأ."

#. type: Plain text
msgid "Notes:"
msgstr ""

#. type: Bullet: '* '
msgid ""
"This add-on requires Windows 10 21H1 (build 19043) or later and is "
"compatible with Windows 11."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Although installation is possible, this add-on does not support Windows "
"Enterprise LTSC (Long-Term Servicing Channel) and Windows Server releases."
msgstr ""

#. type: Bullet: '* '
msgid "Some add-on features are or will be part of NVDA screen reader."
msgstr ""

#. type: Bullet: '* '
msgid ""
"For entries not listed below, you can assume that features are part of NVDA, "
"no longer applicable as the add-on does not support unsupported Windows "
"releases such as old Windows 10 versions, or changes were made to Windows "
"and apps that makes entries no longer applicable."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Some apps support compact overlay mode (always on top in Calculator, for "
"example), and this mode will not work properly with portable version of NVDA."
msgstr ""

#. type: Plain text
msgid ""
"For a list of changes made between each add-on releases, refer to "
"[changelogs for add-on releases][3] document."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "General"
msgstr "عام"

#. type: Bullet: '* '
msgid ""
"NVDA can announce suggestion count when performing a search in majority of "
"cases, including when suggestion count changes as search progresses. This is "
"now part of NVDA 2021.3."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In addition to UIA event handlers provided by NVDA, the following UIA events "
"are recognized: drag complete, drop target dropped, layout invalidated. With "
"NVDA's log level set to debug, these events will be tracked, and for UIA "
"notification event, a debug tone will be heard if notifications come from "
"somewhere other than the currently active app. Events built into NVDA such "
"as name change and controller for events are tracked from an add-on called "
"Event Tracker."
msgstr ""

#. type: Bullet: '* '
msgid ""
"When opening, closing, reordering (Windows 11), or switching between virtual "
"desktops, NVDA will announce active virtual desktop name (desktop 2, for "
"example)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will no longer announce Start menu size text when changing screen "
"resolutions or orientation."
msgstr ""

#. type: Bullet: '* '
msgid ""
"When arranging Start menu tiles or Action Center quick actions with Alt+Shift"
"+arrow keys, NVDA will announce information on dragged items or new position "
"of the dragged item."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Announcements such as volume/brightness changes in File Explorer and app "
"update notifications from Microsoft Store can be suppressed by turning off "
"Report Notifications in NVDA's object presentation settings."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA will no longer announce graphing calculator screen message twice."
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "In context menus for Start Menu tiles, submenus are properly recognized."
msgid "In Windows 10, history and memory list items are properly labeled."
msgstr "التعرف على القوائم الفرعية لقوائم سياق عناصر قائمة ابدأ بشكل صحيح."

#. type: Bullet: '* '
msgid "Textual responses from Cortana are announced in most situations."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA will be silent when talking to Cortana via voice."
msgstr ""

#. type: Bullet: '* '
msgid ""
"When reviewing items in messages list, you can now use table navigation "
"commands to review message headers. Note that navigating between rows "
"(messages) is not supported."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA plays location beep for map locations."
msgstr ""

#. type: Bullet: '* '
msgid ""
"When using street side view and if \"use keyboard\" option is enabled, NVDA "
"will announce street addresses as you use arrow keys to navigate the map."
msgstr ""

#. type: Bullet: '* '
msgid "NVDA will announce names of cards and card decks."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Modern keyboard"
msgstr ""

#. type: Plain text
msgid ""
"This includes emoji panel, clipboard history, dictation/voice typing, "
"hardware input suggestions, and modern input method editors for certain "
"languages. When viewing emojis, for best experience, enable Unicode "
"Consortium setting from NVDA's speech settings and set symbol level to \"some"
"\" or higher. When pasting from clipboard history in Windows 10, press Space "
"key instead of Enter key to paste the selected item. NVDA also supports "
"updated input experience panel in Windows 11."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In Windows 10, when an emoji group (including kaomoji and symbols group) is "
"selected, NVDA will no longer move navigator object to certain emojis."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Added support for updated input experience panel (combined emoji panel and "
"clipboard history) in Windows 11."
msgstr ""

#. type: Bullet: '* '
msgid ""
"In Windows 11, it is again possible to use the arrow keys to review emojis "
"when emoji panel opens."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Notepad"
msgstr ""

#. type: Plain text
msgid "This refers to Windows 11 Notepad version 11 or later."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will announce status items such as line and column information when "
"report status bar command (NVDA+End in desktop layout, NvDA+Shift+End in "
"laptop layout) is performed."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will no longer announce entered text when pressing Enter key from the "
"document."
msgstr ""

#. type: Bullet: '* '
msgid ""
"When searching for contacts, first suggestion will be announced, "
"particularly if using recent app releases."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Settings"
msgstr "إعدادات"

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "Certain information such as Windows Update progress is now reported "
#| "automatically."
msgid ""
"Certain information such as Windows Update progress is reported "
"automatically, including Storage sense/disk cleanup widget and errors from "
"Windows Update."
msgstr "سيتم الإعلان عن بعض المعلومات بشكل آلي كتحديث الويندوز."

#. type: Bullet: '* '
msgid ""
"Progress bar values and other information are no longer announced twice."
msgstr ""

#. type: Bullet: '* '
msgid ""
"Odd control labels seen in certain Windows installations has been corrected."
msgstr ""

#. type: Bullet: '* '
msgid ""
"NVDA will announce the name of the optional quality update control if "
"present (download and install now link in Windows 10, download button in "
"Windows 11)."
msgstr ""

#. type: Bullet: '* '
#, fuzzy
#| msgid ""
#| "In context menus for Start Menu tiles, submenus are properly recognized."
msgid "In Windows 11, breadcrumb bar items are properly recognized."
msgstr "التعرف على القوائم الفرعية لقوائم سياق عناصر قائمة ابدأ بشكل صحيح."

#. type: Bullet: '* '
msgid ""
"Tabs such as \"forecast\" and \"maps\" are recognized as proper tabs (patch "
"by Derek Riemer)."
msgstr ""

#. type: Bullet: '* '
msgid ""
"When reading a forecast, use the left and right arrows to move between "
"items. Use the up and down arrows to read the individual items. For example, "
"pressing the right arrow might report \"Monday: 79 degrees, partly "
"cloudy, ...\" pressing the down arrow will say \"Monday\" Then pressing it "
"again will read the next item (Like the temperature). This currently works "
"for daily and hourly forecasts."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag dev stable]]\n"
msgstr "[[!tag dev stable]]\n"

#. type: Plain text
#, fuzzy
#| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=w10"
msgid "[1]: https://addons.nvda-project.org/files/get.php?file=w10"
msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=w10"

#. type: Plain text
#, fuzzy
#| msgid "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"
msgid "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"
msgstr "[2]: https://addons.nvda-project.org/files/get.php?file=w10-dev"

#. type: Plain text
msgid "[3]: https://github.com/josephsl/wintenapps/wiki/w10changelog"
msgstr ""

#, fuzzy
#~| msgid "Calculator (modern)."
#~ msgid "Calculator (modern)"
#~ msgstr "الحاسبة (حديثة modern)."

#, fuzzy
#~| msgid "When ENTER is pressed, NVDA announces calculation results."
#~ msgid ""
#~ "When ENTER or Escape is pressed, NVDA will announce calculation results."
#~ msgstr "عند ضغط مفتاح الإدخال, سيعلن NVDA عن النتائج."

#, fuzzy
#~| msgid "[1]: https://addons.nvda-project.org/files/get.php?file=w10"
#~ msgid "[4]: https://addons.nvda-project.org/files/get.php?file=w10-2019"
#~ msgstr "[1]: https://addons.nvda-project.org/files/get.php?file=w10"

#~ msgid "Alarms and Clock."
#~ msgstr "Alarms and Clock."

#~ msgid "Alarms and clock"
#~ msgstr "Alarms and clock"

#, fuzzy
#~| msgid ""
#~| "Time picker values are now announced. This also affects the control used "
#~| "to select when to restart to finish installing Windows updates."
#~ msgid ""
#~ "Time picker values are now announced, noticeable when moving focus to "
#~ "picker controls. This also affects the control used to select when to "
#~ "restart to finish installing Windows updates. This is now part of NVDA "
#~ "2019.1."
#~ msgstr ""
#~ "الإعلان عن قيم الوقت. هذا يعالج أيضا الأداة التي تتيح اختيار متى تعيد "
#~ "تشغيل أو تنهي تثبيت تحديثات الويندوز."

#, fuzzy
#~| msgid "Button labels are now announced."
#~ msgid ""
#~ "Notifications such as file downloads and various webpage alerts, as well "
#~ "as availability of Reading View (if using Version 1709 and later) are "
#~ "announced."
#~ msgstr "الآن ستعلن أسماء الأزرار"
