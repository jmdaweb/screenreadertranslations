# Brazilian Portuguese translation of sidebar.pt_BR.po
# Copyright (C) 2013-2020 NVDA Contributors.
# This file is distributed under the same license as the NVDA package.
# Cleverson Casarin Uliana <clever97@gmail.com>, 2013, 2014, 2017.
# Tiago Melo Casal <tcasal@intervox.nce.ufrj.br>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Barra lateral do sítio\n"
"POT-Creation-Date: 2019-11-27 17:18+1000\n"
"PO-Revision-Date: 2020-02-12 21:20-0300\n"
"Last-Translator: Tiago Melo Casal <tcasal@intervox.nce.ufrj.br>\n"
"Language-Team: Equipe de tradução do NVDA para Português do Brasil "
"<clever97@gmail.com>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"
"X-Poedit-SourceCharset: UTF-8\n"

#. type: Bullet: '* '
msgid "[[Stable add-ons|/index]]"
msgstr "[[Complementos estáveis|/index]]"

#. type: Bullet: '* '
msgid "[[Add-ons under development|dev]]"
msgstr "[[Complementos em desenvolvimento|dev]]"

#. type: Bullet: '* '
msgid "[[Legacy add-ons|legacy]]"
msgstr "[[Complementos legados|legacy]]"

#. type: Bullet: '* '
msgid "[[Community announcements|announcements]]"
msgstr "[[Anúncios da comunidade|announcements]]"

#. type: Bullet: '* '
msgid "[Translation status](https://addons.nvda-project.org/poStatus.html)"
msgstr "[Status das traduções](https://addons.nvda-project.org/poStatus.html)"
