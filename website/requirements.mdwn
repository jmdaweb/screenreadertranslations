[[!meta title="Requirements for add-on submissions"]]

To submit a new add-on, or a new version of an add-on included on the website:

* Host it (ideally on GitHub, the [add-on template][1] makes this easy, and create a pull request against [addonFiles][2].
* Edit the get.php file. You need to make sure to have a unique key (add-on ID) in get.php for your add-on.
* Check that the add-on URL is correct.
* Ensure that the manifest is correct. Including API versions: only once the first NVDA Beta is released will the API be considered frozen, therefore 'lastTested' should not be set to that version until after the beta.

NV Access plans to make available an [add-on store][3], where reviews of metadata will be automated. In the meantime, the mentioned pull requests will be reviewed, approved and merged manually, so the inclusion of your add-onn may take some time.

[1]: https://github.com/nvdaaddons/addonTemplate

[2]: https://github.com/nvdaaddons/addonFiles

[3]: https://github.com/nvaccess/addon-store-submission
