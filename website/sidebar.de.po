# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-04-28 11:11+0000\n"
"PO-Revision-Date: 2021-10-04 20:46+0200\n"
"Last-Translator: René Linke <rene.linke@hamburg.de>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"

#. type: Bullet: '* '
msgid "[[Stable add-ons|/index]]"
msgstr "[[Stabile Erweiterungen|/index]]"

#. type: Bullet: '* '
msgid "[[Add-ons under development|dev]]"
msgstr "[[Entwicklerversion der Erweiterung|dev]]"

#. type: Bullet: '* '
msgid "[[Legacy add-ons|legacy]]"
msgstr "[[Veraltete Erweiterungen|legacy]]"

#. type: Bullet: '* '
msgid "[[Community announcements|announcements]]"
msgstr "[[Community-Ankündigungen|announcements]]"

#. type: Bullet: '* '
msgid "[Translation status](https://addons.nvda-project.org/poStatus.html)"
msgstr "[Übersetzungsstatus](https://addons.nvda-project.org/poStatus.html)"
