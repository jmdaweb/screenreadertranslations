# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the resourceMonitor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: resourceMonitor 19.07\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2019-08-16 18:10+1000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_HK\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Message reported when there is no battery on the system,
#. mostly laptops with battery pack removed and running on AC power.
msgid "This computer does not have a battery connected."
msgstr ""

#. Translators: message presented when AC is connected and battery is charging,
#. also show current battery percentage.
#, python-brace-format
msgid "{percent}%, battery charging."
msgstr ""

#. Translators: message presented when computer is running on battery power,
#. showing percentage remaining yet battery time is unknown.
#, python-brace-format
msgid "{percent}% battery remaining, battery time unknown."
msgstr ""

#. Translators: battery and system uptime in hours.
#. Translators: system uptime in hours.
msgid "1 hour"
msgstr ""

#. Translators: battery and system uptime in hours.
#. Translators: system uptime in hours.
#, python-brace-format
msgid "{0} hours"
msgstr ""

#. Translators: battery and system uptime in minutes.
#. Translators: system uptime in minutes.
msgid "1 minute"
msgstr ""

#. Translators: battery and system uptime in minutes.
#. Translators: system uptime in minutes.
#, python-brace-format
msgid "{0} minutes"
msgstr ""

#. Translators: message presented when computer is running on battery power,
#. showing percentage remaining and estimated remaining time.
#, python-brace-format
msgid "{percent}% battery remaining, about {time}."
msgstr ""

#. Translators: Message reported when battery level is low.
msgid " Warning: low battery."
msgstr ""

#. Translators: Message reported when battery level is critical.
msgid " Warning: critically low battery."
msgstr ""

#. Translators: Presents Windows version
#. (example output: "Windows 8.1 (32-bit)").
#, python-brace-format
msgid "{winVersion} ({cpuBit})"
msgstr ""

#. Translators: Presents Windows version and service pack level
#. (example output: "Windows 7 service pack 1 (64-bit)").
#, python-brace-format
msgid "{winVersion} {servicePackLevel} ({cpuBit})"
msgstr ""

#. Translators: The gestures category for this add-on in input gestures dialog (2013.3 or later).
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on
#. to be shown on installation and add-on information.
msgid "Resource Monitor"
msgstr ""

#. Translators: Input help message about battery info command in Resource Monitor.
msgid ""
"Presents battery percentage, charging status, remaining time (if not "
"charging), and a warning if the battery is low or critical."
msgstr ""

#. Translators: Input help message about drive info command in Resource Monitor.
msgid ""
"Presents the used and total space of the static and removable drives on this "
"computer."
msgstr ""

#. Translators: Shows drive letter, type of drive (fixed or removable),
#. used capacity and total capacity of a drive
#. (example: C drive, ntfs; 40 GB of 100 GB used (40%).
#, python-brace-format
msgid ""
"{driveName} ({driveType} drive): {usedSpace} of {totalSpace} used {percent}%."
msgstr ""

#. Translators: Input help mode message about processor info command in Resource Monitor.
msgid "Presents the average processor load and the load of each core."
msgstr ""

#. Translators: Shows average load of CPU cores (example: core 1, 50%).
#, python-brace-format
msgid "Core {coreNumber}: {corePercent}%"
msgstr ""

#. Translators: Shows average load of the processor and the load for each core.
#, python-brace-format
msgid "Average CPU load {avgLoad}%, {cores}."
msgstr ""

#. Translators: Input help mode message about memory info command in Resource Monitor.
msgid "Presents the used and total space for both physical and virtual ram."
msgstr ""

#. Translators: Shows RAM (physical memory) usage.
#, python-brace-format
msgid "Physical: {physicalUsed} of {physicalTotal} used ({physicalPercent}%). "
msgstr ""

#. Translators: Shows virtual memory usage.
#, python-brace-format
msgid "Virtual: {virtualUsed} of {virtualTotal} used ({virtualPercent}%)."
msgstr ""

#. Translators: Input help mode message about Windows version command in Resource Monitor.
msgid "Announces the version of Windows you are using."
msgstr ""

#. Translators: system uptime in days.
msgid "1 day"
msgstr ""

#. Translators: system uptime in days.
#, python-brace-format
msgid "{0} days"
msgstr ""

#. Translators: system uptime in seconds.
msgid "1 second"
msgstr ""

#. Translators: system uptime in seconds.
#, python-brace-format
msgid "{0} seconds"
msgstr ""

#. Translators: Input help mode message about obtaining the system's uptime
msgid "Announces the system's uptime."
msgstr ""

#. Translators: Obtaining uptime failed
msgid "Failed to get the system's uptime."
msgstr ""

#. Translators: Input help mode message about overall system resource info command in Resource Monitor
msgid ""
"Presents used ram, average processor load, and battery info if available."
msgstr ""

#, python-brace-format
msgid "{ramPercent}% RAM used, CPU at {cpuPercent}%."
msgstr ""

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid ""
"A handy resource monitor to report CPU load, memory usage, battery, disk "
"usage status and more."
msgstr ""
