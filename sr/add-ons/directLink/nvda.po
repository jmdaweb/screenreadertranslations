# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the directLink package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: directLink 1.0\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-07-22 05:38+0000\n"
"PO-Revision-Date: 2021-12-26 05:18+0100\n"
"Last-Translator: Nikola Jović <wwenikola123@gmail.com>\n"
"Language-Team: Serbian NVDA team <wwenikola123@gmail.com>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. translators: appears in the NVDA input help.
msgid "Converts the given link to a direct link."
msgstr "Pretvara zahtevan link u direktan link."

msgid ""
"The link has been converted previously, press NVDA+alt+shift+l to open it in "
"browser."
msgstr ""
"Link je već pretvoren, pritisnite NVDA+alt+šift+l da ga otvorite u "
"pretraživaču."

#. translators: the message will be announced when a user converts a dropbox link.
msgid ""
"The Dropbox link has been converted and copied to the clipboard, press NVDA"
"+alt+shift+l to open it in browser."
msgstr ""
"Dropbox link je pretvoren i kopiran u privremenu memoriju, pritisnite NVDA"
"+alt+šift+l da ga otvorite u pretraživaču."

#. translaters: the message will be announced when a user converts a Google Drive link.
msgid ""
"The Google Drive link has been converted and copied to the clipboard, press "
"nvda+alt+shift+l to open it in browser."
msgstr ""
"Google Drive link je pretvoren i kopiran u privremenu memoriju, pritisnite "
"nvda+alt+šift+l da ga otvorite u pretraživaču."

#. translators: the message will be announced when a user converts a oneDrive link.
msgid ""
"The oneDrive link has been converted and copied to the clipboard, press nvda"
"+alt+shift+l to open it in browser."
msgstr ""
"oneDrive link je pretvoren i kopiran u privremenu memoriju, pritisnite nvda"
"+alt+šift+l da ga otvorite u pretraživaču."

#. translators: same as oneDrive message but for oneDrive business links.
msgid ""
"the oneDrive link has been converted and copied to the clipboard, press alt"
"+nvda+shift+l to open it in browser"
msgstr ""
"oneDrive link je pretvoren i kopiran u privremenu memoriju, pritisnite alt"
"+nvda+šift+l da ga otvorite u pretraživaču"

#. translators: the message will be announced when a user converts a whatsapp number.
msgid ""
"The WhatsApp link has been generated and copied to the clipboard, press NVDA"
"+alt+shift+l to open it in browser."
msgstr ""
"WhatsApp link je generisan i kopiran u privremenu memoriju, pritisnite NVDA"
"+alt+šift+l da ga otvorite u pretraživaču."

#. translators: the message will be announced when there is nothing selected, or the clipboard is empty,
#. or if the selected or copied text not a supported service link nor phone number.
msgid ""
"Please select or copy a dropbox link, a google drive link or a oneDrive link "
"to convert, or a WhatsApp number to chat with."
msgstr ""
"Molimo kopirajte ili izaberite dropbox link, google drive link ili oneDrive "
"link za pretvaranje, ili WhatsApp broj sa kojim želite da ćaskate."

#. translators: appears in the NVDA input help
msgid "opens the converted link in browser."
msgstr "Otvara pretvoren link u pretraživaču."

#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on
#. to be shown on installation and add-on information found in Add-ons Manager.
msgid "Direct Link"
msgstr "Direktan link"

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid ""
" Converts dropbox, google drive or oneDrive links to direct links, and opens "
"Whatsapp chats with the given number."
msgstr ""
"Pretvara dropbox, google drive ili oneDrive linkove u direktne linkove, i "
"otvara WhatsApp ćaskanja sa pruženim brojem."
