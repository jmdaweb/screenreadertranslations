# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the zoomEnhancements package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: zoomEnhancements 1.1.1\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-08-17 09:00+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Title for the settings dialog
msgid "Zoom Enhancements settings"
msgstr ""

#. Translators: the lable of the combobox in the settings dialog
msgid "Alerts reporting mode"
msgstr ""

#. Translators: the text of the grouping in the settings dialog
msgid ""
"Choose which alerts should be reported (effective only when custom mode is "
"selected)"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Participant Has Joined/Left Meeting"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Participant Has Joined/Left Waiting Room"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Audio Muted by Host"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Video Stopped by Host"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Screen Sharing Started/Stopped by a Participant"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Recording Permission Granted/Revoked"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Public In-meeting Chat Received"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Private In-meeting Chat Received"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "In-meeting File Upload Completed"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Host Privilege Granted/Revoked"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Participant Has Raised/Lowered Hand (Host Only)"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "Remote Control Permission Granted/Revoked"
msgstr ""

#. Translators: a label of a checkbox in the settings dialog
msgid "IM Chat Received"
msgstr ""

#. A script category for the add -on
#. Translators: the name of the add-on category in input gestures
#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on to be shown on installation and add-on information.
msgid "Zoom Enhancements"
msgstr ""

#. Translators: a lable for alerts reporting mode
msgid "Report all alerts"
msgstr ""

#. Translators: a lable for alerts reporting mode
msgid "Beep for alerts"
msgstr ""

#. Translators: a lable for alerts reporting mode
msgid "Silence all alerts"
msgstr ""

#. Translators: a lable for alerts reporting mode
msgid "Custom"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting joined / left meeting alerts on / off"
msgstr ""

#. Translators: a label for on / off states for reporting a spicific alert
msgid "on"
msgstr ""

msgid "off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Participant Has Joined/Left Meeting is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting joined / left Waiting Room alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Participant Has Joined/Left Waiting Room is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting audio muted by host alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Audio Muted By Host is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting video stopped by host alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Video Stopped By Host is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid ""
"Toggles reporting screen sharing started / stopped by participant alerts "
"on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Screen Sharing Started/Stopped By Participant is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid ""
"Toggles reporting recording permission granted / revoked alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Recording Permission Granted/Revoked is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting public in-meeting chat received alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Public In-meeting Chat Received is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting private in-meeting chat received alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Private In-meeting Chat Received is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting in-meeting file upload completed alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting In-meeting File Upload Completed is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting host privilege granted / revoked alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Host Privilege Granted/Revoked is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting participant raised / lowered hand alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Participant Has Raised/Lowered Hand is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid ""
"Toggles reporting remote control permission granted /revoked alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting Remote Control Permission Granted/Revoked is %s"
msgstr ""

#. Translators: a description for a command to toggle reporting a spicific alert
msgid "Toggles reporting IM chat received alerts on / off"
msgstr ""

#. Translators: a message reported for the user when toggling reporting a spicific alert
#, python-format
msgid "Reporting IM Chat Received is %s"
msgstr ""

#. Translators: a description for a command to cycle between alerts reporting modes
msgid ""
"Toggles between reporting alerts as usual, beeping for the alert, silencing "
"alerts completely, or custom mode, where you can choose which alerts are "
"reported and which aren't."
msgstr ""

#. Translators: a description for a command to move the focus in / out of the remote controlled screen
msgid "Moves focus in / out of the remote controlled screen"
msgstr ""

#. Translators: a description for a command to show the add-on settings dialog
msgid "Shows Zoom enhancements settings dialog"
msgstr ""

#. Translators: a description for a command to show chat history dialog
msgid "Shows chat history dialog"
msgstr ""

#. Translaters: the title of the chat history dialog
msgid "Chat history"
msgstr ""

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid ""
"An add-on which aims to enhance the experience while using Zoom and NVDA"
msgstr ""
