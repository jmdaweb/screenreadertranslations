# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the pcKbBrl package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: pcKbBrl 2021.5\n"
"Report-Msgid-Bugs-To: nvda-translations@groups.io\n"
"POT-Creation-Date: 2021-10-08 16:05+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Describes a command.
msgid "Toggles braille input from the PC keyboard."
msgstr ""

#. Translators: Reported when braille input from the PC keyboard is disabled.
msgid "Braille input from PC keyboard disabled"
msgstr ""

#. Translators: Reported when braille input from the PC keyboard is enabled.
msgid "Braille input from PC keyboard enabled"
msgstr ""

#. Translators: Describes a command.
#, python-format
msgid "Shows the %s settings."
msgstr ""

#. Translators: label of a dialog.
msgid "&Type using one hand"
msgstr ""

#. Translators: label of a dialog.
msgid "&Speak dot when typing with one hand"
msgstr ""

#. Add-on summary, usually the user visible name of the addon.
#. Translators: Summary for this add-on to be shown on installation and add-on information.
msgid "PC Keyboard Braille Input"
msgstr ""

#. Add-on description
#. Translators: Long description to be shown for this add-on on add-on information from add-ons manager
msgid "Allows braille to be entered via the PC keyboard."
msgstr ""
